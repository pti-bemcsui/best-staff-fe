import { React, useContext } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Homepage from "./HomePage";
import BiroDepartemen from "./BiroDepartemen";
import NavbarAdmin from "../Component/Admin/NavbarAdmin";
import AdminFooter from "../Component/Admin/AdminFooter";
import YearAdd from "./AddBS";
import CrudAnggota from "./DataAnggota/CrudAnggota";
import EditAnggota from "./DataAnggota/EditAnggota";
import UpdateAnggota from "./DataAnggota/UpdateStaff";
import { Login } from "../Auth";

const Admin = () => {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/:domain/admin/DataAnggota/Crud/Update">
            <UpdateAnggota />
          </Route>
          <Route exact path="/:domain/admin/DataAnggota/Add">
            <EditAnggota></EditAnggota>
          </Route>
          <Route exact path="/:domain/admin/DataAnggota/Edit">
            <EditAnggota></EditAnggota>
          </Route>
          <Route exact path="/:domain/admin/DataAnggota/Crud">
            <CrudAnggota></CrudAnggota>
          </Route>
          <Route exact path="/:domain/admin/YearAdd">
            <YearAdd></YearAdd>
          </Route>
          <Route exact path="/:domain/admin/biro">
            <BiroDepartemen></BiroDepartemen>
          </Route>
          <Route exact path="/:domain/admin">
            <Homepage></Homepage>
          </Route>
        </Switch>
      </Router>
    </div>
  );
};
export default Admin;
