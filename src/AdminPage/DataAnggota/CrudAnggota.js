import { useState, useEffect } from "react";
import axios from "axios";
import NavbarAdmin from "../../Component/Admin/NavbarAdmin";
import AdminFooter from "../../Component/Admin/AdminFooter";
import {
  PaginationSection,
  PaginationContainer,
  NavbarSection,
  Popup,
  ButtonConfirm,
  UpdateParagraph1,
  UpdateParagraph2,
  UpdateImage,
  DeleteImage,
  KeteranganUD,
  KeteranganSection,
  KeteranganContainer,
  KeteranganTitle,
  KeteranganStrip,
  KeteranganUpdate,
  KeteranganDelete,
  TableSection,
  TableContainer,
  DropNamaBiro,
  NamaBiro,
  Vector,
  SearchImage,
  AddStaff,
  BiroSelectorContainer,
  SearchContainer,
  HeaderSection,
  Title,
  SelectorContainer,
} from "./style";
import Table from "react-bootstrap/Table";
import "bootstrap/dist/css/bootstrap.min.css";
import ListAnggota from "./ListAnggota";
import { Groupping, FilterBiro, FilterName } from "./Groupping";
import Pagination from "react-bootstrap/Pagination";
import Edit from "./EditAnggota";
import { Redirect, Link } from "react-router-dom";
import EditAnggota from "./EditAnggota";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import Spinner from "react-bootstrap/Spinner";
import { getCookie } from "../../helpers/cookies";

import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const MySwal = withReactContent(Swal);

const ListBiro = [
  "All",
  "Bismit",
  "PTI",
  "PSDM",
  "Kastrat",
  "Pengmas",
  "Adkesma",
  "Akpem",
  "Humas",
  "Media",
  "Keilmuan",
  "Depor",
  "Senbud",
];

function CrudAnggota() {
  const dataActive = parseInt(localStorage.getItem("Active"));
  const [GrouppedData, setGrouppedData] = useState([]);
  const [SelectedBirdep, setSelectedBirdep] = useState("Biro / Departemen");
  const [showDropBirdep, setShowDropBirdep] = useState(false);
  const [dataShow, setDataShow] = useState(GrouppedData[dataActive]);
  const [paginationItem, setPaginationItem] = useState([]);
  const [redirect, setRedirect] = useState(false);
  const [deleteAction, setDelete] = useState(false);
  const [update, setUpdate] = useState(false);
  const [show, setShow] = useState(true);
  const [isDelete, setIsDelete] = useState(false);
  const [ObjectDelete, setObjectDelete] = useState({});
  const [success, setSuccess] = useState(false);
  const [refresher, setRefresher] = useState(false);
  const token = localStorage.getItem("token") || getCookie("bs-token");
  useEffect(() => {
    let items = [];
    for (let number = 1; number <= GrouppedData.length; number++) {
      // console.log(number, " : ", number, dataActive, number === dataActive);
      items.push(
        <Pagination.Item key={number} active={number - 1 === dataActive}>
          {number}
        </Pagination.Item>
      );
    }
    setPaginationItem(items);
  }, [dataActive]);

  async function fetchData() {
    const fetchOption = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    axios
      .get(
        "https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/anggota-bem/",
        fetchOption
      )
      .then((res) => {
        const finalData = res.data.data;
        const readyData = Groupping(finalData);
        setGrouppedData(readyData);
        setDataShow(readyData);
      });
      // .catch((err) => console.log(err));
  }

  function DeleteAnggota(npm) {
    let payload = { npm: npm };
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    axios
      .delete("https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/anggota-bem/", {
        data: payload,
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        MySwal.fire({
          position: "center",
          icon: "success",
          title: "Success",
          text: "Anggota telah dihapus",
          showConfirmButton: false,
          timer: 3000,
        });
        setRefresher(!refresher);
      })
      .catch((err) =>
        MySwal.fire({
          position: "center",
          icon: "error",
          title: "Failed",
          text: "Gagal menghapus anggota",
          showConfirmButton: false,
          timer: 3000,
        })
      );
  }
  const WindowAddStaff = () => {
    var child = window.open(
      `https://ptibem.cs.ui.ac.id/beststaff/admin/user/bem/add/`,
      "",
      "toolbar=0,status=0,width=626,height=700"
    );
    var timer = setInterval(checkChild, 500);

    function checkChild() {
      if (child.closed) {
        setRefresher(!refresher);
        clearInterval(timer);
      }
    }
  };

  const WindowUpdateStaff = (npm) => {
    var child = window.open(
      `https://ptibem.cs.ui.ac.id/beststaff/admin/user/bem/${npm}/change/`,
      "",
      "toolbar=0,status=0,width=626,height=700"
    );
    var timer = setInterval(checkChild, 500);

    function checkChild() {
      if (child.closed) {
        setRefresher(!refresher);
        clearInterval(timer);
      }
    }
  };

  useEffect(() => {
    fetchData();
  }, [deleteAction, success, refresher]);

  return (
    <div>
      <div style={{ position: "relative", width: "100%", height: "100%" }}>
        <NavbarAdmin></NavbarAdmin>
      </div>

      {success ? (
        <div
          style={{
            width: "100%",
            height: "100%",
            position: "fixed",
            background: "rgba(0,0,0,0.25)",
            top: "0",
            left: "0",
            zIndex: "10",
          }}
        >
          <Popup>
            <img
              src="https://cdn.discordapp.com/attachments/755605623214964900/858746542177124362/Vector.png"
              style={{
                width: "100px",
                height: "100px",
                margin: "auto",
                marginTop: "36px",
              }}
            />
            <h3 style={{ textAlign: "center" }}>Success change data</h3>
            <ButtonConfirm onClick={() => setSuccess(!success)}>
              Close
            </ButtonConfirm>
          </Popup>
        </div>
      ) : null}

      <HeaderSection>
        <Title>DATA ANGGOTA BEM </Title>
        <SelectorContainer>
          <SearchContainer
            placeholder="Masukan Nama"
            onChange={(e) => {
              setDataShow(FilterName(GrouppedData, e.target.value));
            }}
          />
          <BiroSelectorContainer
            onClick={() => {
              setShowDropBirdep(!showDropBirdep);
            }}
          >
            <NamaBiro
              onClick={() => {
                setShowDropBirdep(!showDropBirdep);
              }}
            >
              {SelectedBirdep}
            </NamaBiro>
            <Vector onClick={() => setShowDropBirdep(!showDropBirdep)}></Vector>
            {showDropBirdep ? (
              <DropNamaBiro>
                {ListBiro.map((data) => {
                  return (
                    <div
                      style={{ zindex: "10", background: "" }}
                      onClick={() => {
                        setSelectedBirdep(data);
                      }}
                    >
                      <p
                        style={{
                          left: "15%",
                          position: "relative",
                          background: "",
                          width: "150px",
                          textAlign: "center",
                          top: "10px",
                        }}
                        onClick={() => {
                          setSelectedBirdep(data);
                          setShowDropBirdep(false);
                          setDataShow(FilterBiro(GrouppedData, data));
                        }}
                      >
                        {data}
                      </p>
                      <hr
                        style={{
                          width: "150px",
                          margin: "0px",
                          left: "15%",
                          position: "relative",
                        }}
                      ></hr>
                    </div>
                  );
                })}
              </DropNamaBiro>
            ) : null}
          </BiroSelectorContainer>
          <AddStaff>
            {" "}
            <Link
              to="/best-staff/admin/DataAnggota/Add"
              style={{ textDecoration: "none" }}
            >
              {" "}
              <p style={{ color: "white", position: "relative", top: "15px" }}>
                Add Staff
              </p>{" "}
            </Link>
          </AddStaff>
        </SelectorContainer>
      </HeaderSection>

      <TableSection>
        <br />
        <br />
        <br />

        <TableContainer>
          <Table striped bordered hover responsive="sm md lg">
            <thead style={{ background: "#F99622" }}>
              <tr style={{}}>
                <th style={{ color: "white", textAlign: "center" }}>No</th>
                <th style={{ color: "white", textAlign: "center" }}>Nama</th>
                <th style={{ color: "white", textAlign: "center" }}>
                  Username
                </th>
                <th style={{ color: "white", textAlign: "center" }}>NPM</th>
                <th style={{ color: "white", textAlign: "center" }}>
                  Asal Biro / Departemen
                </th>
                <th style={{ color: "white", textAlign: "center" }}>Update</th>
                <th style={{ color: "white", textAlign: "center" }}>Delete</th>
              </tr>
            </thead>

            <tbody>
              {dataShow ? (
                dataShow.map((data) => (
                  <tr>
                    <td style={{ textAlign: "center" }}>{data.id}</td>
                    <td style={{ textAlign: "center" }}>{data.nama}</td>
                    <td style={{ textAlign: "center" }}>{data.username}</td>
                    <td style={{ textAlign: "center" }}>{data.npm}</td>
                    <td style={{ textAlign: "center" }}>{data.birdept.kode}</td>
                    <td style={{ textAlign: "center" }}>
                      <Link
                        to={{
                          pathname: "/best-staff/admin/DataAnggota/Crud/Update",
                          state: { data },
                        }}
                      >
                        <UpdateImage style={{ left: "30%" }} />
                      </Link>
                    </td>
                    <td style={{ textAlign: "left" }}>
                      <DeleteImage
                        style={{ marginLeft: "0px" }}
                        onClick={() => {
                          MySwal.fire({
                            title: "Delete Anggota",
                            text: `Apakah yakin menghapus ${data.nama} dari anggota`,
                            icon: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#3085d6",
                            cancelButtonColor: "#d33",
                            confirmButtonText: "Yes, delete it!",
                          }).then((result) => {
                            if (result.isConfirmed) {
                              setIsDelete(true);
                              setObjectDelete(data);
                              DeleteAnggota(data.npm);
                            }
                          });
                        }}
                      ></DeleteImage>
                    </td>
                  </tr>
                ))
              ) : (
                <div style={{}}>
                  <Spinner animation="border" />
                </div>
              )}
            </tbody>
          </Table>
        </TableContainer>
        <br />
      </TableSection>

      <KeteranganSection>
        <KeteranganContainer>
          <KeteranganTitle>Keterangan</KeteranganTitle>
          <KeteranganStrip></KeteranganStrip>
          <KeteranganUD>
            <KeteranganUpdate>
              <UpdateImage style={{ top: "25%" }}></UpdateImage>
              <UpdateParagraph1>Update</UpdateParagraph1>
            </KeteranganUpdate>

            <KeteranganDelete>
              <DeleteImage style={{ top: "25%" }}></DeleteImage>
              <UpdateParagraph2>Delete</UpdateParagraph2>
            </KeteranganDelete>
          </KeteranganUD>
        </KeteranganContainer>
      </KeteranganSection>

      <AdminFooter></AdminFooter>
    </div>
  );
}
export default CrudAnggota;
