import React, { useState, useEffect } from "react";
import styled from "styled-components";
import JSONStatus from "./SimpenData/JSONStatus.json";
import BirdeptJSON from "./SimpenData/BirdeptJSON.json";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const MySwal = withReactContent(Swal);

const Page = styled.section`
  display: flex;
  width: 100%;
  height: 100vh;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  // border: 2px solid black;
  justify-content: center;
  align-items: flex-start;
`;

const Label = styled.label`
  display: flex;
`;

const Submit = styled.input`
  width: 20%;
`;

const TextInput = styled.input`
  margin-left: 10px;
`;

const Title = styled.h1`
  margin-bottom: 20px;
`;

const Select = styled.select`
  margin-left: 10px;
`;

const ImageInput = styled.input`
  margin-left: 10px;
`;

const AddStaff = (props) => {
  const [fetchData, setFetchData] = useState();
  const [inputData, setInputData] = useState();
  const [selectedFile, setSelectedFile] = useState(null);
  const [birdept, setBirdept] = useState();
  const [sabiBirdept, setSabiBirdept] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    const objBirdept = JSON.parse(birdept);

    var formdata = new FormData();
    formdata.append("name", inputData.nama);
    formdata.append("npm", inputData.npm);
    formdata.append("username", inputData.username);
    formdata.append("jurusan", inputData.jurusan);
    formdata.append("angkatan", inputData.angkatan);
    formdata.append("tahun_kepengurusan", inputData.tahun_kepengurusan);
    formdata.append(
      "birdept",
      `{\"kode\" : \"${objBirdept.kode}\", \"nama\" : \"${objBirdept.nama}\"}`
    );
    formdata.append("bidang", inputData.bidang);
    if (sabiBirdept !== "") {
      const objSabi = JSON.parse(sabiBirdept);
      formdata.append(
        "birdept_sabi",
        `{\"kode\" : \"${objSabi.kode}\", \"nama\" : \"${objSabi.nama}\"}`
      );
    }
    formdata.append("status", inputData.status);
    formdata.append("foto", selectedFile);

    // for (var pair of formdata.entries()) {
    //   console.log(pair[0] + ", " + pair[1]);
    // }


    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo4LCJ1c2VybmFtZSI6ImR6aWtyaS5xYWxhbTAxIiwiZXhwIjoxNjI5NDY5ODg1LCJlbWFpbCI6ImR6aWtyaS5xYWxhbTAxQHVpLmFjLmlkIn0.5wC62DI6tUs79JVzTnFCIC2HULk46ozTiDbyNUU-arE"
    );

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow",
    };

    fetch(
      "https://enigmatic-earth-76345.herokuapp.com/https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/anggota-bem/",
      requestOptions
    )
      .then((response) => {
        response.text();
      })
      .catch((error) => {
        alert("Your request denied");
      });
  };

  return (
    <Page>
      <Title>Add Staff</Title>
      <Form id="addForm" onSubmit={handleSubmit}>
        <Label>
          Name:
          <TextInput
            type="text"
            name="name"
            onChange={(e) =>
              setInputData({ ...inputData, nama: e.target.value })
            }
            required
          />
        </Label>
        <Label>
          NPM:
          <TextInput
            type="number"
            name="npm"
            onChange={(e) =>
              setInputData({ ...inputData, npm: e.target.value })
            }
            required
          />
        </Label>
        <Label>
          Username:
          <TextInput
            type="text"
            name="username"
            onChange={(e) =>
              setInputData({ ...inputData, username: e.target.value })
            }
            required
          />
        </Label>
        <Label>
          Jurusan:
          <TextInput
            type="text"
            name="jurusan"
            onChange={(e) =>
              setInputData({ ...inputData, jurusan: e.target.value })
            }
            required
          />
        </Label>
        <Label>
          Angkatan:
          <TextInput
            type="number"
            onChange={(e) =>
              setInputData({ ...inputData, angkatan: e.target.value })
            }
            required
            name="angkatan"
          />
        </Label>
        <Label>
          Tahun Kepengurusan:
          <TextInput
            type="number"
            name="tahun"
            onChange={(e) =>
              setInputData({ ...inputData, tahun_kepengurusan: e.target.value })
            }
            required
          />
        </Label>
        <Label>
          BirDept:
          <Select id="dropdown" onChange={(e) => setBirdept(e.target.value)}>
            <option value="" selected disabled hidden>
              Choose here
            </option>
            {BirdeptJSON.map((birodept) => {
              return (
                <option
                  value={`{"kode":"${birodept.kode}", "nama":"${birodept.nama}"}`}
                >
                  {birodept.kode}
                </option>
              );
            })}
          </Select>
        </Label>
        <Label>
          Bidang:
          <TextInput
            type="text"
            name="name"
            onChange={(e) =>
              setInputData({ ...inputData, bidang: e.target.value })
            }
            required
          />
        </Label>
        <Label>
          BirDept Sabi:
          <Select
            id="dropdown"
            onChange={(e) => setSabiBirdept(e.target.value)}
          >
            <option value="" selected>
              Tidak ada
            </option>
            {BirdeptJSON.map((birodept) => {
              return (
                <option
                  value={`{"kode":"${birodept.kode}", "nama":"${birodept.nama}"}`}
                >
                  {birodept.kode}
                </option>
              );
            })}
          </Select>
        </Label>
        <Label>
          Status:
          <Select
            id="dropdown"
            onChange={(e) =>
              setInputData({ ...inputData, status: e.target.value })
            }
          >
            <option value="" selected disabled hidden>
              Choose here
            </option>
            {JSONStatus.map((status) => {
              return <option value={status.key}>{status.text}</option>;
            })}
          </Select>
        </Label>
        <Label>
          Foto:
          <ImageInput
            type="file"
            onChange={(e) => setSelectedFile(e.target.files[0])}
          />
        </Label>
        <Label>
          isUdahTerpilih:
          <Select
            id="dropdown"
            onChange={(e) =>
              setInputData({ ...inputData, is_sudah_terpilih: e.target.value })
            }
          >
            <option value={false} selected>
              No
            </option>
            <option value={true}>Yes</option>
          </Select>
        </Label>
        <Submit type="submit" value="Submit" />
      </Form>
    </Page>
  );
};

export default AddStaff;
