import React, { useState, useEffect } from "react";
import styled from "styled-components";
import JSONStatus from "./SimpenData/JSONStatus.json";
import BirdeptJSON from "./SimpenData/BirdeptJSON.json";

const Page = styled.section`
  display: flex;
  width: 100%;
  height: 100vh;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  // border: 2px solid black;
  justify-content: center;
  align-items: flex-start;
`;

const Label = styled.label`
  display: flex;
`;

const Submit = styled.input`
  width: 20%;
`;

const TextInput = styled.input`
  margin-left: 10px;
`;

const Title = styled.h1`
  margin-bottom: 20px;
`;

const Select = styled.select`
  margin-left: 10px;
`;

const ImageInput = styled.input`
  margin-left: 10px;
`;

const StaffUpdate = () => {
  const [fetchData, setFetchData] = useState({
    npm: "1231241221",
    name: "test",
    username: "test11221",
    jurusan: "test",
    angkatan: "2021",
    tahun_kepengurusan: "2021",
    bidang: "Internal",
    status: 0,
    is_sudah_terpilih: false,
    nama: "test",
    foto: "",
    birdept: {
      kode: "PTI",
      nama: "Pengembangan Teknologi Informasi",
    },
    birdept_sabi: {
      kode: "PTI",
      nama: "Pengembangan Teknologi Informasi",
    },
  });

  const [nama, setNama] = useState(fetchData.name);
  const [npm, setNpm] = useState(fetchData.npm);
  const [username, setUserName] = useState(fetchData.username);
  const [jurusan, setJurusan] = useState(fetchData.jurusan);
  const [angkatan, setAngkatan] = useState(fetchData.angkatan);
  const [tahunKepengurusan, setTahunKepurusan] = useState(
    fetchData.tahun_kepengurusan
  );
  const [birdept, setBirdept] = useState(fetchData.birdept);
  const [sabiBirdept, setSabiBirdept] = useState(fetchData.birdept_sabi);
  const [status, setStatus] = useState(fetchData.status);
  const [bidang, setBidang] = useState(fetchData.bidang);
  const [selectedFile, setSelectedFile] = useState(fetchData.foto);
  const [terpilih, setTerpilih] = useState(fetchData.is_sudah_terpilih);

  const handleSubmit = (e) => {
    e.preventDefault();

    var formdata = new FormData();
    formdata.append("name", nama);
    formdata.append("npm", npm);
    formdata.append("username", username);
    formdata.append("jurusan", jurusan);
    formdata.append("angkatan", angkatan);
    formdata.append("tahun_kepengurusan", tahunKepengurusan);
    formdata.append(
      "birdept",
      `{\"kode\" : \"${birdept.kode}\", \"nama\" : \"${birdept.nama}\"}`
    );
    formdata.append("bidang", bidang);

    formdata.append("status", status);

    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo4LCJ1c2VybmFtZSI6ImR6aWtyaS5xYWxhbTAxIiwiZXhwIjoxNjI5NDY5ODg1LCJlbWFpbCI6ImR6aWtyaS5xYWxhbTAxQHVpLmFjLmlkIn0.5wC62DI6tUs79JVzTnFCIC2HULk46ozTiDbyNUU-arE"
    );

    var requestOptions = {
      method: "PUT",
      headers: myHeaders,
      body: formdata,
      redirect: "follow",
    };

    fetch(
      "https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/anggota-bem/",
      requestOptions
    )
      .then((response) => {
        response.text();
      })
      // .then((result) => console.log(result))
      .catch((error) => {
        alert("Your request denied");
      });
  };

  return (
    <Page>
      <Title>Staff Update</Title>
      <Form id="addForm" onSubmit={handleSubmit}>
        <Label>
          Name:
          <TextInput
            type="text"
            name="name"
            onChange={(e) => {
              setNama(e.target.value);
            }}
            defaultValue={nama}
            required
          />
        </Label>
        <Label>
          NPM:
          <TextInput
            type="number"
            name="npm"
            onChange={(e) => {
              setNpm(e.target.value);
            }}
            defaultValue={npm}
            required
          />
        </Label>
        <Label>
          Username:
          <TextInput
            type="text"
            name="username"
            onChange={(e) => {
              setUserName(e.target.value);
            }}
            defaultValue={username}
            required
          />
        </Label>
        <Label>
          Jurusan:
          <TextInput
            type="text"
            name="jurusan"
            onChange={(e) => {
              setJurusan(e.target.value);
            }}
            defaultValue={jurusan}
            required
          />
        </Label>
        <Label>
          Angkatan:
          <TextInput
            type="number"
            onChange={(e) => {
              setAngkatan(e.target.value);
            }}
            defaultValue={angkatan}
            required
            name="angkatan"
          />
        </Label>
        <Label>
          Tahun Kepengurusan:
          <TextInput
            type="number"
            name="tahun"
            onChange={(e) => {
              setTahunKepurusan(e.target.value);
            }}
            defaultValue={tahunKepengurusan}
            required
          />
        </Label>
        <Label>
          BirDept:
          <Select
            id="dropdown"
            onChange={(e) => {
              setBirdept(e.target.value);
            }}
          >
            <option value={fetchData.birdept} selected>
              {fetchData.birdept.kode}
            </option>
            {BirdeptJSON.map((birodept) => {
              return (
                <>
                  {fetchData.birdept.kode !== birodept.kode && (
                    <option
                      value={`{"kode":"${birodept.kode}", "nama":"${birodept.nama}"}`}
                    >
                      {birodept.kode}
                    </option>
                  )}
                </>
              );
            })}
          </Select>
        </Label>
        <Label>
          Bidang:
          <TextInput
            type="text"
            name="name"
            onChange={(e) => {
              setBidang(e.target.value);
            }}
            defaultValue={bidang}
            required
          />
        </Label>
        <Label>
          BirDept Sabi:
          <Select
            id="dropdown"
            onChange={(e) => {
              setSabiBirdept(e.target.value);
            }}
          >
            <option value={fetchData.birdept_sabi} selected>
              {fetchData.birdept_sabi.kode}
            </option>
            {BirdeptJSON.map((birodept) => {
              return (
                <>
                  {fetchData.birdept_sabi.kode !== birodept.kode && (
                    <option
                      value={`{"kode":"${birodept.kode}", "nama":"${birodept.nama}"}`}
                    >
                      {birodept.kode}
                    </option>
                  )}
                </>
              );
            })}
          </Select>
        </Label>
        <Label>
          Status:
          <Select
            id="dropdown"
            onChange={(e) => {
              setStatus(e.target.value);
            }}
            defaultValue={status}
          >
            <option value="" selected disabled hidden>
              Choose here
            </option>
            {JSONStatus.map((status) => {
              return <option value={status.key}>{status.text}</option>;
            })}
          </Select>
        </Label>
        <Label>
          Foto:
          <ImageInput
            type="file"
            onChange={(e) => setSelectedFile(e.target.files[0])}
            defaultValue={selectedFile}
          />
        </Label>
        <Label>
          isUdahTerpilih:
          <Select
            id="dropdown"
            onChange={(e) => {
              setTerpilih(e.target.value);
            }}
            defaultValue={terpilih}
          >
            <option value={false} selected>
              No
            </option>
            <option value={true}>Yes</option>
          </Select>
        </Label>
        <Submit type="submit" value="Submit" />
      </Form>
    </Page>
  );
};

export default StaffUpdate;
