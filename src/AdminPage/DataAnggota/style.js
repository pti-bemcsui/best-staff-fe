import styled from "styled-components";

export const ButtonConfirm = styled.button`
  display: flex;
  z-index: 1;

  margin: auto;
  padding: 12px 48px;
  background: #f99622;
  border: 0;
  border-radius: 12px;
  outline: none;

  font-size: 20px;
  color: white;
  cursor: pointer;
`;
export const Popup = styled.section`
  display: flex;
  flex-direction: column;
  width: 500px;
  height: 300px;
  z-index: 1;

  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  margin: 0;
  background: white;
  border-radius: 24px;
  filter: drop-shadow(0px 2px 2px rgba(0, 0, 0, 0.25));
`;

export const NavbarSection = styled.div`
  width: 100%;
  height: auto;
  background: red;
  position: relative;
`;

export const KeteranganSection = styled.div`
  ${"" /* width: 100%; */}
  margin: 0 8vw;
  height: 300px;
  top: 50px;
  ${"" /* background-color: blue;  */}
  position : relative;
`;

export const SubmitSection = styled.div`
  width: 100%;
  height: 400px;
  ${"" /* background-color: blue;  */}
  position : relative;
  top: 100px;
`;

export const SubmitButton = styled.button`
  height: 60px;
  width: 264px;
  left: 40%;
  top: 10%;
  border-radius: 20px;
  background: #f99622;
  border: none;
  font-size: 20px;
  font-weight: 500;
  color: #ffffff;
  position: relative;
  // box-shadow: 0px 0px 10px 2px rgba(0, 0, 0, 0.25);
  @media (max-width: 850px) {
    left: 35%;
  }
  @media (max-width: 620px) {
    left: 30%;
  }
  @media (max-width: 550px) {
    font-size: 16px;
    width: 200px;
    height: 50px;
  }
  @media (max-width: 450px) {
    left: 25%;
  }
  @media (max-width: 350px) {
    left: 20%;
  }
  &:hover {
    background: #0f3b5e;
    transition: 0.3s;
  }
`;

export const BlackVector = styled.div`
  background-image: url("https://www.pngkey.com/png/full/11-117105_down-arrow-png-image-background-arrow-down-icon.png");
  height: 12px;
  width: 20px;
  background-size: 100% 100%;
  position: relative;
  left: 94%;
  top: -120%;
  cursor: pointer;
  @media (max-width: 550px) {
    top: -150%;
  }
`;

export const DropChose = styled.div`
  position: absolute;
  width: 480px;
  height: auto;
  top: 120%;
  background: #ffffff;
  filter: drop-shadow(0px 2px 2px rgba(0, 0, 0, 0.25));
  border-radius: 15px;
  z-index: 5;
  @media (max-width: 1450px) {
    width: 440px;
  }
  @media (max-width: 1370px) {
    width: 400px;
  }
  @media (max-width: 1250px) {
    width: 360px;
  }
  @media (max-width: 1145px) {
    width: 320px;
  }
  @media (max-width: 1000px) {
    width: 280px;
  }
  @media (max-width: 900px) {
    width: 240px;
  }
  @media (max-width: 740px) {
    width: 200px;
  }
  @media (max-width: 715px) {
    width: 95%;
  }
`;

export const NamaPilihan = styled.p`
  position: relative;
  // top : 5%;
`;
export const Dpdown = styled.div`
  position: relative;
  padding-bottom: 5px;
  margin: auto;
  width: 100%;
  // top: 64px;
  top: 15px;
  text-align: center;
  z-index: 10;

  // background: white;
  // color: black;

  // filter: drop-shadow(0px 2px 2px rgba(0, 0, 0, 0.25));
  border-radius: 15px;
`;
// export const Dpdown = styled.p`
//     // left : 15%;
//     position : relative;
//     width : 480px;
//     text-align :center;
//     top: 10px;
//     @media(max-width: 1450px){
//         width: 440px;
//     }
//     @media(max-width: 1370px){
//         width: 400px;
//     }
//     @media(max-width: 1250px){
//         width: 360px;
//     }
//     @media(max-width: 1145px){
//         width: 320px;
//     }
//     @media(max-width: 1000px){
//         width: 280px;
//     }
//     @media(max-width: 900px){
//         width: 240px;
//     }
//     @media(max-width: 740px){
//         width: 200px;
//     }
//     @media(max-width: 715px){
//         width: 95%;
//     }
// `
export const ChoseInput = styled.div`
  height: 50px;
  width: 80%;
  top: 5%;
  position: relative;
  border-radius: 15px;
  background: #f1f1f1;
  text-align: left;
  padding: 10px 20px;
  border: 0px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  // @media(max-width: 800px){
  //     width: 90%;
  // }
  // @media(max-width: 715px){
  //     left: 5%;
  //     width: 80%;
  // }
  @media (max-width: 900px) {
    left: 0%;
    width: 60vw;
  }
  @media (max-width: 550px) {
    height: 40px;
  }
  @media (max-width: 384px) {
    left: 0%;
    width: 60vw;
  }
`;

export const TextInput = styled.input`
  height: 50px;
  width: 80%;
  top: 5%;
  position: relative;
  border-radius: 15px;
  background: #f1f1f1;
  text-align: left;
  padding: 10px 20px;
  border: 0px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  // @media(max-width: 800px){
  //     width: 90%;
  // }
  // @media(max-width: 715px){
  //     left: 5%;
  //     width: 97%;
  // }
  @media (max-width: 900px) {
    left: 0%;
    width: 60vw;
  }
  @media (max-width: 550px) {
    height: 40px;
  }
  @media (max-width: 384px) {
    left: 0%;
    width: 60vw;
  }
`;

export const ImageInput = styled.div`
  height: 60px;
  width: 80%;
  top: -10%;
  position: relative;
  border-radius: 15px;
  background: white;
  text-align: left;
  ${"" /* padding : 10px 15px; */}
  border : 0px;
  ${"" /* box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25); */}
  // @media(max-width: 800px){
    //     width: 90%;
    // }
    // @media(max-width: 715px){
    //     left: 5%;
    //     width: 80%;
    // }
    @media(max-width: 900px) {
    left: 5%;
    width: 60vw;
  }
  @media (max-width: 550px) {
    height: 40px;
  }
  @media (max-width: 384px) {
    left: 0%;
    width: 60vw;
  }
`;

export const LeftInputContainer = styled.div`
  width: 40%;
  // background : gray;
  font-weight: 600;
  position: relative;
  margin: 0 20px;
  @media (max-width: 900px) {
    margin: 0px;
    width: 90%;
  }
`;

export const RightInputContainer = styled.div`
  width: 60%;
  // background : gray;
  position: relative;
  // @media(max-width: 800px){
  //     width : 55%;
  // }
  // @media(max-width: 720px){
  //     width: 50%;
  // }
  // @media(max-width: 715px){
  //     width: 100%;
  // }
  @media (max-width: 715px) {
    width: 100%;
  }
`;
export const Haer = styled.hr`
  // left : 15%;
  position: relative;
  width: 480px;
  text-align: center;
  @media (max-width: 1450px) {
    width: 440px;
  }
  @media (max-width: 1370px) {
    width: 400px;
  }
  @media (max-width: 1250px) {
    width: 360px;
  }
  @media (max-width: 1145px) {
    width: 320px;
  }
  @media (max-width: 1000px) {
    width: 280px;
  }
  @media (max-width: 900px) {
    width: 240px;
  }
  @media (max-width: 740px) {
    width: 200px;
  }
  @media (max-width: 715px) {
    width: 95%;
  }
`;

export const IdentityContainer = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  height: 100px;
  font-size: 20px;
  flex-direction: row;
  left: 4.2vw;
  @media (max-width: 900px) {
    margin-left: 18px;
    flex-direction: column;
    height: 130px;
  }
  @media (max-width: 550px) {
    font-size: 14px;
    margin-left: 10px;
  }
`;

export const IdentityContainerImg = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  height: 20rem;
  flex-direction: row;
  left: 4.2vw;
  // background : pink;
  @media (max-width: 900px) {
    flex-direction: column;
  }
`;

export const InputContainer = styled.div`
  width: 85%;
  height: auto;
  // background : blue;
  position: relative;
  display: flex;
  flex-direction: column;
  left: 12%;
`;

export const InputSection = styled.section`
  width: 90%;
  height: auto;
  ${"" /* background : red; */}
  position : relative;
`;
//

export const PaginationSection = styled.div`
  ${"" /* background : red; */}
  width : 100%;
  height: 200px;
`;

export const PaginationContainer = styled.div`
  ${"" /* background : blue; */}
  width : 60%;
  height: 80px;
  position: relative;
  left: 30%;
  align-items: center;
  display: flex;
  left: 20%;
  @media (max-width: 384px) {
    left: 5%;
  }
`;

export const UpdateImage = styled.div`
  background-image: url("https://cdn.discordapp.com/attachments/755605623214964900/847672932670111815/Update_Button.png");
  height: 50px;
  width: 50px;
  background-size: 100% 100%;
  position: relative;
  flex: 1;
  @media (max-width: 550px) {
    height: 30px;
    width: 30px;
  }
`;

export const DeleteImage = styled.div`
  background-image: url("https://cdn.discordapp.com/attachments/755605623214964900/847672953705201664/Delete_Button.png");
  height: 50px;
  width: 50px;
  background-size: 100% 100%;
  position: relative;
  flex: 1;
  margin-left: 170px;
  @media (max-width: 550px) {
    height: 30px;
    width: 30px;
    margin-left: 115px;
  }
`;
export const UpdateParagraph1 = styled.p`
  font-size: 20px;
  margin: -15px 0 0 60px;
  @media (max-width: 550px) {
    font-size: 14px;
    margin: -2px 0 0 40px;
  }
`;
export const UpdateParagraph2 = styled.p`
  font-size: 20px;
  margin: -15px 0 0 225px;
  @media (max-width: 550px) {
    font-size: 14px;
    margin: -2px 0 0 155px;
  }
`;

export const KeteranganContainer = styled.div`
  width: 0px;
  height: 300px;
  ${"" /* background : blue; */}
  padding : 0 4vw;
  position: relative;
  left: 5%;
  display: flex;
  flex-direction: column;
  @media (max-width: 1248px) {
    ${"" /* width : 60%; */}
  }
  @media (max-width: 550px) {
    font-size: 14px;
  }
`;
export const KeteranganTitle = styled.p`
  font-family: Poppins;
  font-size: 20px;
  font-style: normal;
  font-weight: 600;
  line-height: 30px;
  letter-spacing: 0em;
  text-align: left;
  @media (max-width: 550px) {
    font-size: 18px;
  }
`;
export const KeteranganStrip = styled.div`
  background: #f99622;
  width: 62.5px;
  height: 0px;
  top: -12px;
  position: relative;
  border: 2px solid #f99622;
  left: 0px;
`;
export const KeteranganUD = styled.div`
  // width : 300px;
  height: 100px;
  ${"" /* background : navy; */}
  display : flex;
  flex-direction: row;
`;

export const KeteranganUpdate = styled.div`
  width: 50%;
  height: 100px;
  ${"" /* background : cyan; */}
`;

export const KeteranganDelete = styled.p`
  width: 50%;
  height: 100px;
  ${"" /* background : pink; */}
`;

export const TableSection = styled.section`
  position: relative;
  ${"" /* background : blue; */}
  width : 100%;
  height: auto;
`;
export const TableContainer = styled.div`
  position: relative;
  overflow-x: auto;
  padding: 0 11vw;
  ${"" /* background : red; */}
  width : 93%;
  height: auto;
  left: 5%;
  @media (max-width: 1200px) {
    width: 86vw;
  }
  @media (max-width: 1160px) {
    padding-top: 10vh;
  }
  @media (max-width: 705px) {
    padding-top: 20vh;
  }
  @media (max-width: 576px) {
    width: 93%;
    padding-top: 30vh;
    left: 5%;
    font-size: 14px;
  }
  @media (max-width: 550px) {
    padding-top: 10vh;
  }
  @media (max-width: 529px) {
    padding-top: 20vh;
  }
`;

export const HeaderSection = styled.div`
  position: relative;
  width: 80%;
  height: 250px;
  margin: 0px 0 0 8vw;
  @media (max-width: 377px) {
    top: -50px;
    height: 330px;
  }
`;
export const DropNamaBiro = styled.div`
  position: absolute;
  width: 220px;
  height: auto;
  top: 120%;
  background: #ffffff;
  filter: drop-shadow(0px 2px 2px rgba(0, 0, 0, 0.25));
  border-radius: 15px;
  z-index: 5;
`;

export const SelectorContainer = styled.div`
  width: 80%;
  height: 100px;
  position: relative;
  top: 45%;
  left: 10%;
`;

export const SearchContainer = styled.input`
  height: 50px;
  width: 250px;
  left: 163px;
  top: 412px;
  border-radius: 15px;
  background: #f1f1f1;
  text-align: left;
  padding: 10px 15px;
  border: 0px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`;
export const BiroSelectorContainer = styled.div`
  height: 55px;
  width: 220px;
  top: -50px;
  position: relative;
  border-radius: 15px;
  background: #0f3b5e;
  left: 30%;
  @media (max-width: 1300px) {
    left: 35%;
  }
  @media (max-width: 1160px) {
    top: 20px;
    left: 0%;
  }
  @media (max-width: 550px) {
    height: 45px;
    width: 70vw;
  }
`;

export const NamaBiro = styled.p`
  font-family: Poppins;
  font-size: 16px;
  font-style: normal;
  font-weight: 400;
  line-height: 30px;
  letter-spacing: 0em;
  text-align: center;
  color: white;
  position: relative;
  left: -20px;
  top: 20%;
  @media (max-width: 550px) {
    font-size: 13px;
  }
`;
export const Vector = styled.div`
  background-image: url("https://www.pngkit.com/png/full/50-508417_blue-arrow-down-png-download-white-arrow-down.png");
  height: 12px;
  width: 20px;
  background-size: 100% 100%;
  position: relative;
  cursor: pointer;
  left: 180px;
  top: -45%;
  @media (max-width: 550px) {
    top: -60%;
    left: 48vw;
  }
  @media (max-width: 430px) {
    left: 54vw;
  }
`;

export const AddStaff = styled.div`
  height: 56px;
  width: 174px;
  top: -110%;
  left: 92%;
  border-radius: 15px;
  background: #f99622;
  text-align: center;
  position: relative;
  text-align: center;
  cursor: pointer;
  @media (max-width: 992px) {
    left: 80%;
  }
  @media (max-width: 576px) {
    top: 35%;
    left: 0%;
  }
  @media (max-width: 550px) {
    height: 45px;
    width: 70vw;
    font-size: 14px;
  }
  &:hover {
    background: #0f3b5e;
    transition: 0.3s;
  }
`;

export const Title = styled.p`
  font-family: Poppins;
  font-size: 48px;
  font-style: normal;
  font-weight: 800;
  letter-spacing: 0em;
  text-align: left;
  margin-right: 8vw;
  color: #0f3b5e;
  left: 10%;
  top: 40%;
  position: relative;
  @media (max-width: 550px) {
    font-size: 36px;
  }
`;

export const SearchImage = styled.div`
  background-image: url("https://cdn.discordapp.com/attachments/755605623214964900/847472042650632232/Vector1.png");
  height: 20px;
  width: 20px;
  background-size: 100% 100%;
  position: absolute;
  flex: 1;
  top: 15%;
  left: 3%;
`;

export const ConfirmContainer = styled.div`
  height: 400px;
  width: 400px;
  border-radius: 50px;
  background: blue;
  border-radius: 50px;
  display: flex;
  flex-direction: column;
  position: relative;
  left: 23%;
  box-shadow: 0px 0px 10px 2px rgba(0, 0, 0, 0.25);
`;
export const SuccesImage = styled.div`
  height: 50%;
  width: 20%;
  position: absolute;
  top: 5%;
  left: 40%;
  background-size: 100% 100%;
  background-image: url("https://cdn.discordapp.com/attachments/755605623214964900/848387897337053203/teenyicons_tick-circle-solid.png");
  @media (max-width: 384px) {
    width: 50%;
    left: 25%;
  }
`;

export const FailedImage = styled.div`
  height: 50%;
  width: 30%;
  position: absolute;
  top: 5%;
  left: 35%;
  background-size: 100% 100%;
  @media (max-width: 384px) {
    width: 50%;
    left: 25%;
  }
  background-image: url("https://cdn.discordapp.com/attachments/755605623214964900/848387989348024330/Failed.png");
`;

export const ShowImage = styled.img`
  margin-left: auto;
  margin-right: auto;
  width: 15vw;
  height: 37vmin;
  border-radius: 10%;
  @media screen and (max-width: 900px) {
    width: 22vw;
    height: 37vmin;
  }
  @media screen and (max-width: 500px) {
    width: 30vw;
    height: 45vmin;
    margin-bottom: 10px;
  }
`;

export const imageValidate = (link) => {
  var res =
    "https://cdn.discordapp.com/attachments/755605623214964900/858178467349790731/Rectangle_59.png";
  var linkImage = link;
  if (typeof linkImage == "string") {
    if (linkImage.includes("http")) res = linkImage;
  } else {
    res = URL.createObjectURL(linkImage);
  }
  return res;
};
