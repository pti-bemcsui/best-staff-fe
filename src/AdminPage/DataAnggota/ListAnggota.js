const ListAnggota = [{ Nama: "Null", Biro: "Null", NPM: "-1" }];
ListAnggota.propTypes = {
  Nama: "string",
  Biro: "string",
  NPM: "string",
};

export default ListAnggota;
