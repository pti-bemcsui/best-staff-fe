export function Groupping(data) {
  var ans = [];
  for (let i = 0; i < data.length; i++) {
    data[i].id = i + 1;
    ans.push(data[i]);
  }
  return ans;
}

export function GrouppingByName(data) {
  var ans = [];
  for (let i = 0; i < data.length; i++) {
    ans.push(data[i].name);
  }
  return ans;
}

export function FilterName(data, search) {
  var ans = [];
  for (let i = 0; i < data.length; i++) {
    const domain = data[i].nama.toLowerCase();
    const range = search.toLowerCase();
    if (domain.includes(range)) ans.push(data[i]);
  }
  return ans;
}

export function FilterBiro(data, search) {
  var ans = [];
  if (search === "All") return data;
  for (let i = 0; i < data.length; i++) {
    try {
      const domain = data[i].birdept.kode.toLowerCase();
      const range = search.toLowerCase();
      if (domain.includes(range)) {
        ans.push(data[i]);
      }
    } catch (e) {
      // None to do
    }
  }
  return ans;
}
