import { useState, useEffect } from "react";
import axios from "axios";
import NavbarAdmin from "../../Component/Admin/NavbarAdmin";
import AdminFooter from "../../Component/Admin/AdminFooter";
import ImageUploader from "react-images-upload";
import { useLocation } from "react-router-dom";
import {
  SuccesImage,
  FailedImage,
  ConfirmContainer,
  ImageInput,
  KeteranganStrip,
  KeteranganUD,
  KeteranganUpdate,
  UpdateImage,
  KeteranganDelete,
  DeleteImage,
  KeteranganContainer,
  KeteranganTitle,
  KetranganSection,
  SubmitSection,
  SubmitButton,
  ChoseInput,
  NamaPilihan,
  BlackVector,
  DropChose,
  LeftInputContainer,
  RightInputContainer,
  TextInput,
  IdentityContainer,
  InputContainer,
  InputSection,
  Dpdown,
  Haer,
  HeaderSection,
  Title,
  KeteranganSection,
  ShowImage,
  imageValidate,
  IdentityContainerImg,
} from "./style";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { getCookie } from "../../helpers/cookies";

const MySwal = withReactContent(Swal);
const ListBiro = [
  "Bismit",
  "PTI",
  "PSDM",
  "Kastrat",
  "Pengmas",
  "Adkesma",
  "Akpem",
  "Humas",
  "Media",
  "Keilmuan",
  "Depor",
  "Senbud",
];
const BiroPair = {
  Bismit: "Bisnis Dan Kemitraan",
  PTI: "Pengembangan Teknologi Informasi",
  PSDM: "Pengembangan Sumber Daya Manusia",
  Kastrat: "Kajian Dan Aksi Strategis",
  Pengmas: "Pengabdian Masyarakat",
  Adkesma: "Advokasi Kesejahteraan Mahasiswa",
  Humas: "Hubungan Masyarakat",
  Media: "Media",
  Keilmuan: "Keilmuan",
  Depor: "Olahraga",
  Senbud: "Seni Budaya",
  "": "",
};
const ListJurusan = ["Sistem Informasi", "Ilmu Komputer"];
const ListStatus = ["Staff", "Deputi", "Kabiro", "Sabi", "Koorbid"];
const StatusDecode = {
  Staff: 0,
  Deputi: 1,
  Kabiro: 2,
  Sabi: 3,
  Koorbid: 4,
};
const StatusUndecode = {
  0: "Staff",
  1: "Deputi",
  2: "Kabiro",
  3: "Sabi",
  4: "Koorbid",
};
const ListBidang = [
  "Pengurus Inti",
  "Kontrol Internal",
  "Keuangan",
  "Internal",
  "Sosial Politik",
  "Kesejahteraan Mahasiswa",
  "Relasi",
  "Minat Bakat",
];
export default function UpdateAnggota() {
  let location = useLocation();
  var data = location.state.data;
  const [dropJurusan, setDropJurusan] = useState(false);
  const [dropAngkatan, setDropAngkata] = useState(false);
  const [dropStatus, setDropStatus] = useState(false);
  const [dropBidang, setDropBidang] = useState(false);
  const [dropBiro, setDropBiro] = useState(false);
  const [dropBiroLain, setDropBiroLain] = useState(false);

  const [nama, setNama] = useState(data.nama);
  const [username, setUsername] = useState(data.username);
  const [npm, setNpm] = useState(data.npm);
  const [jurusan, setJurusan] = useState(data.jurusan);
  const [angkatan, setAngkatan] = useState(data.angkatan);
  const [tahunKepengurusan, setTahunKepurusan] = useState(
    data.tahun_kepengurusan
  );
  const [status, setStatus] = useState(data.status);
  const [bidang, setBidang] = useState(data.bidang);
  const [biro, setBiro] = useState(data.birdept.kode);
  const [birolain, setBirolain] = useState(
    data.birdept_sabi.kode === undefined ? "" : data.birdept_sabi.kode
  );
  const [picture, setPicture] = useState(data.foto);
  return (
    <div>
      <div style={{ position: "relative", width: "100%", height: "100%" }}>
        <NavbarAdmin></NavbarAdmin>
      </div>
      <HeaderSection style={{ height: "300px" }}>
        <Title>DATA ANGGOTA BEM </Title>
      </HeaderSection>

      <InputSection>
        <InputContainer>
          {/* Nama */}
          <IdentityContainer>
            <LeftInputContainer>
              <p style={{ position: "relative", top: "15%" }}>Nama</p>
            </LeftInputContainer>
            <RightInputContainer>
              <TextInput
                type="text"
                onChange={(e) => {
                  setNama(e.target.value);
                }}
                placeholder={nama}
                onClick={() => {
                  dropBiroLain && setDropBiroLain(false);
                  dropJurusan && setDropJurusan(false);
                  dropStatus && setDropStatus(false);
                  dropBidang && setDropBidang(false);
                  dropBiro && setDropBiro(false);
                }}
              ></TextInput>
            </RightInputContainer>
          </IdentityContainer>

          {/* Username */}
          <IdentityContainer>
            <LeftInputContainer>
              <p style={{ position: "relative", top: "15%" }}>Username</p>
            </LeftInputContainer>
            <RightInputContainer>
              <TextInput
                type="text"
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
                placeholder={username}
                onClick={() => {
                  dropBiroLain && setDropBiroLain(false);
                  dropJurusan && setDropJurusan(false);
                  dropStatus && setDropStatus(false);
                  dropBidang && setDropBidang(false);
                  dropBiro && setDropBiro(false);
                }}
              ></TextInput>
            </RightInputContainer>
          </IdentityContainer>

          {/* NPM */}
          <IdentityContainer>
            <LeftInputContainer>
              <p style={{ position: "relative", top: "15%" }}>NPM</p>
            </LeftInputContainer>
            <RightInputContainer>
              <TextInput
                type="text"
                onChange={(e) => {
                  setNpm(e.target.value);
                }}
                placeholder={npm}
                onClick={() => {
                  dropBiroLain && setDropBiroLain(false);
                  dropJurusan && setDropJurusan(false);
                  dropStatus && setDropStatus(false);
                  dropBidang && setDropBidang(false);
                  dropBiro && setDropBiro(false);
                }}
              ></TextInput>
            </RightInputContainer>
          </IdentityContainer>

          {/* Jurusan */}
          <IdentityContainer>
            <LeftInputContainer>
              <p style={{ position: "relative", top: "15%" }}>Jurusan</p>
            </LeftInputContainer>
            <RightInputContainer>
              <ChoseInput
                onClick={() => {
                  setDropJurusan(!dropJurusan);
                  dropBiroLain && setDropBiroLain(false);
                  dropStatus && setDropStatus(false);
                  dropBidang && setDropBidang(false);
                  dropBiro && setDropBiro(false);
                }}
              >
                <NamaPilihan>{jurusan}</NamaPilihan>
                <BlackVector></BlackVector>
                {dropJurusan ? (
                  <DropChose
                    onMouseLeave={() => {
                      setDropJurusan(false);
                    }}
                  >
                    {ListJurusan.map((data) => {
                      return (
                        <div
                          style={{ zindex: "10", background: "" }}
                          onClick={() => {
                            setJurusan(data);
                          }}
                        >
                          <Dpdown>{data}</Dpdown>
                          <Haer></Haer>
                        </div>
                      );
                    })}
                  </DropChose>
                ) : null}
              </ChoseInput>
            </RightInputContainer>
          </IdentityContainer>

          {/* Angkatan */}
          <IdentityContainer>
            <LeftInputContainer>
              <p style={{ position: "relative", top: "15%" }}>Angkatan</p>
            </LeftInputContainer>
            <RightInputContainer>
              <TextInput
                onChange={(e) => {
                  setAngkatan(e.target.value);
                }}
                onClick={() => {
                  dropBiroLain && setDropBiroLain(false);
                  dropJurusan && setDropJurusan(false);
                  dropStatus && setDropStatus(false);
                  dropBidang && setDropBidang(false);
                  dropBiro && setDropBiro(false);
                }}
                placeholder={angkatan}
              ></TextInput>
            </RightInputContainer>
          </IdentityContainer>

          {/* Tahun Kepengurusan */}
          <IdentityContainer>
            <LeftInputContainer>
              <p style={{ position: "relative", top: "15%" }}>
                Tahun Kepengurusan
              </p>
            </LeftInputContainer>
            <RightInputContainer>
              <TextInput
                type="text"
                onChange={(e) => {
                  setTahunKepurusan(e.target.value);
                }}
                onClick={() => {
                  dropBiroLain && setDropBiroLain(false);
                  dropJurusan && setDropJurusan(false);
                  dropStatus && setDropStatus(false);
                  dropBidang && setDropBidang(false);
                  dropBiro && setDropBiro(false);
                }}
                placeholder={tahunKepengurusan}
              ></TextInput>
            </RightInputContainer>
          </IdentityContainer>

          {/* Status */}
          <IdentityContainer>
            <LeftInputContainer>
              <p style={{ position: "relative", top: "15%" }}>Status</p>
            </LeftInputContainer>
            <RightInputContainer>
              <ChoseInput
                onClick={() => {
                  setDropStatus(!dropStatus);
                  dropBiroLain && setDropBiroLain(false);
                  dropJurusan && setDropJurusan(false);
                  dropBidang && setDropBidang(false);
                  dropBiro && setDropBiro(false);
                }}
              >
                <NamaPilihan>{StatusUndecode[status]}</NamaPilihan>
                <BlackVector></BlackVector>
                {dropStatus ? (
                  <DropChose>
                    {ListStatus.map((dataStatus) => {
                      return (
                        <div
                          style={{ zindex: "10", background: "" }}
                          onClick={() => {
                            if (
                              StatusDecode[dataStatus] !== 3 &&
                              StatusDecode[dataStatus] !== 4
                            ) {
                              if (status === 3 || status === 4) {
                                setBirolain("");
                              }
                            }
                            if (status !== 3 && status !== 4) {
                              if (
                                (data.status === 3 &&
                                  StatusDecode[dataStatus] === 3) ||
                                (data.status === 4 &&
                                  StatusDecode[dataStatus] === 4)
                              ) {
                                setBirolain(data.birdept_sabi.kode);
                              }
                            }
                            setStatus(StatusDecode[dataStatus]);
                          }}
                        >
                          <Dpdown>{dataStatus}</Dpdown>
                          <Haer></Haer>
                        </div>
                      );
                    })}
                  </DropChose>
                ) : null}
              </ChoseInput>
            </RightInputContainer>
          </IdentityContainer>

          {/* Bidang */}
          <IdentityContainer>
            <LeftInputContainer>
              <p style={{ position: "relative", top: "15%" }}>Bidang</p>
            </LeftInputContainer>
            <RightInputContainer>
              <ChoseInput
                onClick={() => {
                  setDropBidang(!dropBidang);
                  dropBiroLain && setDropBiroLain(false);
                  dropJurusan && setDropJurusan(false);
                  dropStatus && setDropStatus(false);
                  dropBiro && setDropBiro(false);
                }}
              >
                <NamaPilihan>{bidang}</NamaPilihan>
                <BlackVector></BlackVector>
                {dropBidang ? (
                  <DropChose
                    onMouseLeave={() => {
                      setDropJurusan(false);
                    }}
                  >
                    {ListBidang.map((data) => {
                      return (
                        <div
                          style={{ zindex: "10", background: "" }}
                          onClick={() => {
                            setBidang(data);
                          }}
                        >
                          <Dpdown>{data}</Dpdown>
                          <Haer></Haer>
                        </div>
                      );
                    })}
                  </DropChose>
                ) : null}
              </ChoseInput>
            </RightInputContainer>
          </IdentityContainer>

          {/* Biro */}
          <IdentityContainer>
            <LeftInputContainer>
              <p style={{ position: "relative", top: "15%" }}>
                Biro / Departemen
              </p>
            </LeftInputContainer>
            <RightInputContainer>
              <ChoseInput
                onClick={() => {
                  setDropBiro(!dropBiro);
                  dropBiroLain && setDropBiroLain(false);
                  dropJurusan && setDropJurusan(false);
                  dropStatus && setDropStatus(false);
                  dropBidang && setDropBidang(false);
                }}
              >
                <NamaPilihan>{biro}</NamaPilihan>
                <BlackVector></BlackVector>
                {dropBiro ? (
                  <DropChose
                    onMouseLeave={() => {
                      setDropJurusan(false);
                    }}
                  >
                    {ListBiro.map((data) => {
                      return (
                        <div
                          style={{ zindex: "10", background: "" }}
                          onClick={() => {
                            setBiro(data);
                          }}
                        >
                          <Dpdown>{data}</Dpdown>
                          <Haer></Haer>
                        </div>
                      );
                    })}
                  </DropChose>
                ) : null}
              </ChoseInput>
            </RightInputContainer>
          </IdentityContainer>

          {/* Biro Lain */}
          {(status === 3 || status == 4) && (
            <IdentityContainer>
              <LeftInputContainer>
                <p style={{ position: "relative", top: "15%" }}>
                  Biro / Departemen Lain
                </p>
              </LeftInputContainer>
              <RightInputContainer>
                <ChoseInput
                  onClick={() => {
                    setDropBiroLain(!dropBiroLain);
                    dropJurusan && setDropJurusan(false);
                    dropStatus && setDropStatus(false);
                    dropBidang && setDropBidang(false);
                    dropBiro && setDropBiro(false);
                  }}
                >
                  <NamaPilihan>
                    {birolain === "" ? "Please select" : birolain}
                  </NamaPilihan>
                  <BlackVector></BlackVector>
                  {dropBiroLain ? (
                    <DropChose
                      onMouseLeave={() => {
                        setDropJurusan(false);
                      }}
                    >
                      {ListBiro.map((data) => {
                        return (
                          <div
                            style={{ zindex: "10", background: "" }}
                            onClick={() => {
                              setBirolain(data);
                            }}
                          >
                            <Dpdown>{data}</Dpdown>
                            <Haer></Haer>
                          </div>
                        );
                      })}
                    </DropChose>
                  ) : null}
                </ChoseInput>
              </RightInputContainer>
            </IdentityContainer>
          )}

          {/* Picture */}
          <IdentityContainerImg>
            <LeftInputContainer>
              <p style={{ position: "relative", top: "15%" }}>Foto</p>
            </LeftInputContainer>
            <RightInputContainer>
              <ShowImage src={imageValidate(picture)}></ShowImage>
              <input
                style={{ position: "relative", top: "10%", width: "100%" }}
                type="file"
                name="file"
                onChange={(e) => {
                  const selectedFile = e.target.files[0];
                  setPicture(selectedFile);
                }}
                onClick={() => {
                  dropBiroLain && setDropBiroLain(false);
                  dropJurusan && setDropJurusan(false);
                  dropStatus && setDropStatus(false);
                  dropBidang && setDropBidang(false);
                  dropBiro && setDropBiro(false);
                }}
              />
            </RightInputContainer>
          </IdentityContainerImg>
        </InputContainer>
      </InputSection>

      <SubmitSection>
        <SubmitButton
          onClick={() => {
            var checkNama = nama === "null" || nama === "";
            var checkUsername = username === "";
            var checkJurusan = jurusan === "null" || jurusan === "";
            var checkNpm = npm === "null" || npm === "";
            var checkAngkatan = angkatan === "null" || angkatan === "";
            var checkTahunKepengurusan =
              tahunKepengurusan === "null" || tahunKepengurusan === "";
            var checkStatus = status === -1;
            var checkBidang = bidang === "null" || bidang === "";
            var checkBiro = biro === "null" || biro === "";
            var checkBiroLain = () => {
              if (status === "Sabi" || status === "Koorbid")
                if (birolain === "null" || birolain === "") return true;
              return false;
            };
            var checkPicture = picture === null;
            var isFulfilled =
              checkNama ||
              checkUsername ||
              checkJurusan ||
              checkNpm ||
              checkAngkatan ||
              checkTahunKepengurusan ||
              checkStatus ||
              checkBidang ||
              checkBiro ||
              checkPicture ||
              checkBiroLain();
            isFulfilled = !isFulfilled;

            if (isFulfilled) {
              MySwal.fire({
                position: "center",
                icon: "info",
                title: "Request Process",
                showConfirmButton: false,
                timer: 3000,
              });
              let payload = {
                npm: npm,
                username: nama,
                jurusan: jurusan,
                angkatan: angkatan,
                tahun_kepengurusan: tahunKepengurusan,
                bidang: bidang,
                status: status,
                is_sudah_terpilih: false,
                foto: picture,
                birdept: {
                  kode: biro,
                  nama: BiroPair[biro],
                },
                birdept_sabi: {
                  kode: birolain,
                  nama: BiroPair[birolain],
                },
              };
              const FormPayload = new FormData();
              FormPayload.append("npm", npm);
              FormPayload.append("name", nama);
              FormPayload.append("username", username);
              FormPayload.append("jurusan", jurusan);
              FormPayload.append("angkatan", angkatan);
              FormPayload.append("tahun_kepengurusan", tahunKepengurusan);
              FormPayload.append("status", status);
              FormPayload.append("bidang", bidang);
              FormPayload.append(
                "birdept",
                `{"kode":"${biro}","nama":"${BiroPair[biro]}"}`
              );
              FormPayload.append(
                "birdept_sabi",
                `{"kode":"${birolain}","nama":"${BiroPair[biro]}"}`
              );
              FormPayload.append("foto", picture);

              let token =
                localStorage.getItem("token") || getCookie("bs-token");
              const config = {
                method: "put",
                url: "https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/anggota-bem/",
                headers: {
                  Authorization: `Bearer ${token}`,
                  "Content-Type": "application/form-data",
                },
                data: FormPayload,
              };

              axios(config)
                .then((res) => {
                  MySwal.fire({
                    position: "center",
                    icon: "success",
                    title: "Data Save",
                    showConfirmButton: false,
                    timer: 1000,
                  }).then(() => {
                    window.location.assign(
                      "/best-staff/admin/DataAnggota/Crud"
                    );
                  });
                })
                .catch((err) => {
                  MySwal.fire({
                    position: "center",
                    icon: "error",
                    title: "Data not Save !",
                    showConfirmButton: false,
                    timer: 1000,
                  });
                });
            } else {
              alert("Failed to Add / Update! Please complete all section.");
            }
          }}
        >
          Submit
        </SubmitButton>
      </SubmitSection>

      <AdminFooter></AdminFooter>
    </div>
  );
}
