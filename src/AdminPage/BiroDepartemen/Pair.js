const Pair = {
    'PTI' : 'Biro PTI',
    'Bismit' : 'Biro Bismit',
    'PSDM' : 'Biro PSDM',
    'Kastrat' : 'Departemen Kastrat',
    'Pengmas' : 'Departemen Pengmas',
    'Adkesma' : 'Departemen Adkesma',
    'Akpem' : 'Departemen Akpem',
    'Humas' : 'Biro Humas',
    'Media' : 'Biro Media',
    'Keilmuan' : 'Departemen Keilmuan',
    'Depor' : 'Departemen Depor',
    'Senbud' : 'Departemen Senbud'
}

export default Pair