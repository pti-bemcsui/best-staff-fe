import styled from "styled-components";

export const BiroFilter = styled.div`
  display: flex;
  margin: auto;
  height: 54px;
  width: 230px;
  min-height: 42px;
  justify-content: space-between;

  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  background: #ffffff;
  border-radius: 15px;
  cursor: pointer;
  position: relative;
  top: 60%;
  left: -27vw;
  @media (max-width: 1350px) {
    left: -25vw;
  }
  @media (max-width: 1140px) {
    left: -22vw;
  }
  @media (max-width: 920px) {
    left: -20vw;
  }
  @media (max-width: 550px) {
    margin-top: -20px;
    font-size: 14px;
    left: -18vw;
    height: 45px;
    width: 220px;
  }
  @media (max-width: 450px) {
    left: -12vw;
  }
  @media (max-width: 370px) {
    left: -9vw;
  }
  @media (max-width: 340px) {
    left: -6vw;
  }
`;

export const FilterButton = styled.div`
  display: flex;
  margin: auto;
  height: 54px;
  width: 230px;
  min-height: 42px;
  justify-content: space-between;

  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  background: #ffffff;
  border-radius: 15px;
  cursor: pointer;
  position: relative;
  top: 58%;
  left: -9%;
  @media (max-width: 1350px) {
    left: -5%;
  }
  @media (max-width: 1200px) {
    left: -3%;
  }
  @media (max-width: 1140px) {
    left: 1%;
  }
  @media (max-width: 1040px) {
    left: 4%;
  }
  @media (max-width: 920px) {
    left: 8%;
  }
  @media (max-width: 860px) {
    left: 10%;
  }
  @media (max-width: 820px) {
    left: -20%;
    top: 75%;
  }
  @media (max-width: 550px) {
    margin-top: -20px;
    font-size: 14px;
    left: -18vw;
    height: 45px;
    width: 220px;
  }
  @media (max-width: 450px) {
    left: -12vw;
  }
  @media (max-width: 370px) {
    left: -9vw;
  }
  @media (max-width: 340px) {
    left: -6vw;
  }
`;

export const Dropdown = styled.div`
  position: absolute;
  padding-bottom: 28px;
  margin: auto;
  width: 100%;
  top: 64px;
  z-index: 10;

  background: white;
  color: black;

  filter: drop-shadow(0px 2px 2px rgba(0, 0, 0, 0.25));
  border-radius: 15px;
`;

export const DData = styled.div`
  padding-top: 12px;
  padding-bottom: 5px;
  border-radius: 4px;

  font-weight: normal;
  color: black;
  text-align: center;

  cursor: pointer;
  &:hover {
    background: #e5e5e5;
  }
`;
export const Selection = styled.section`
  position: relative;
  width: 100%;
  height: 200px;
  @media (max-width: 820px) {
    display: flex;
    flex-direction: column;
    margin-bottom: 100px;
  }
`;
export const StaffData = styled.section`
  width: 100%;
  height: auto;
  background: #ffffff;
`;
export const Description = styled.section`
  width: 100%;
  height: 400px;
  margin-bottom: 100px;
  ${"" /* background : blue; */}
  @media (max-width:584px) {
    margin-bottom: 200px;
  }
  @media (max-width: 360px) {
    margin-bottom: 400px;
  }
`;

export const TableContainer = styled.div`
  width: 75%;
  height: auto;
  ${"" /* background : blue; */}
  position : relative;
  left: 15%;
  top: 80px;
  @media (max-width: 700px) {
    left: 10%;
    width: 80%;
  }
  @media (max-width: 550px) {
    top: 40px;
    font-size: 14px;
  }
`;

export const Text = styled.p`
  height: 78px;
  ${"" /* width: 667px; */}
  left: 15vw;
  top: 20%;
  border-radius: 0px;
  font-family: Poppins;
  font-size: 48px;
  font-style: normal;
  font-weight: 800;
  line-height: 72px;
  letter-spacing: 0em;
  text-align: left;
  margin: 0px;
  position: absolute;
  color: #0f3b5e;
  @media (max-width: 550px) {
    font-size: 36px;
    left: 10%;
  }
`;

export const MonthSelect = styled.div`
  height: 54px;
  width: 295px;
  left: 20%;
  top: 60%;
  border-radius: 15px;
  position: absolute;
  background: #ffffff;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
`;

export const Vector = styled.div`
  background-image: url("https://www.pngkey.com/png/full/11-117105_down-arrow-png-image-background-arrow-down-icon.png");
  height: 12px;
  width: 20px;
  background-size: 100% 100%;
  position: absolute;
  margin-left: 10px;
  margin-top: 5px;
  z-index: 1;
`;
export const Header = styled.div`
  background: #f99622;
  height: 69px;
  position: relative;
  text-align: center;
`;

export const HeaderTitle = styled.p`
  font-size: 22px;
  font-style: normal;
  font-weight: 600;
  line-height: 33px;
  letter-spacing: 0em;
  text-align: center;
  color: #ffffff;
  position: relative;
  top: -5px;
`;

export const Body = styled.div`
  background: #e6e6e6;
  display: flex;
  position: relative;
  height: auto;
  text-align: center;
  flex-direction: column;
`;
export const DataText = styled.p`
  font-family: Poppins;
  font-size: 22px;
  font-style: normal;
  font-weight: 400;
  letter-spacing: 0em;
  text-align: ce;
  color: black;
`;

export const DescriptionKeterangan = styled.div`
  width: 80%;
  height: 300px;
  background: #ffffff;
  position: relative;
  top: 35%;
  left: 15%;
  @media (max-width: 550px) {
    top: 25%;
    left: 12%;
  }
  @media (max-width: 360px) {
    width: 60%;
  }
  @media (max-width: 550px) {
    font-size: 14px;
  }
`;

export const FooterContainer = styled.section`
  height: 100px;
  background: red;
  width: 100%;
  top: 30%;
  position: relative;
`;

export const DropMonthContainer = styled.div`
  position: absolute;
  width: 230px;
  height: 350px;
  left: 20%;
  top: 36%;
  background: #ffffff;
  box-shadow: 5px 5px 4px 8px rgba(0, 0, 0, 0.25);
  border-radius: 15px;
  z-index: 5;
`;
