import NavbarAdmin from "../../Component/Admin/NavbarAdmin";
import AdminFooter from "../../Component/Admin/AdminFooter";
import styled from "styled-components";
import { useState, useEffect } from "react";
import Table from "react-bootstrap/Table";
import Arrow from "./Arrow.png";
import Button from 'react-bootstrap/Button'
import "bootstrap/dist/css/bootstrap.min.css";
import {
  FilterButton,
  DData,
  Dropdown,
  BiroFilter,
  Selection,
  StaffData,
  Description,
  TableContainer,
  Text,
  MonthSelect,
  Vector,
  Header,
  HeaderTitle,
  Body,
  DataText,
  DescriptionKeterangan,
  FooterContainer,
  DropMonthContainer,
} from "./styles";
import Data from "./Data";
import axios from "axios";
import Alert from "react-bootstrap/Alert";
import { useLocation } from "react-router-dom";
import { getCookie } from "../../helpers/cookies";
import PairBiro from './Pair'

//---------Sweet Alert ----------------
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const MySwal = withReactContent(Swal);
//--------Sweet Alert-------------------

function BiroDepartemen() {
  const DefaultProps = useLocation().state;
  const [month, setMonth] = useState("loading");
  const [currentMonth,setCurrentMonth] = useState("")
  const [previousMonth, setPreviousMonth] = useState([]);
  const NamaBulan = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Augustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];
  const DaftarBiro = [
    "Bismit",
    "PTI",
    "PSDM",
    "Kastrat",
    "Pengmas",
    "Adkesma",
    "Akpem",
    "Humas",
    "Media",
    "Keilmuan",
    "Depor",
    "Senbud",
  ];
  const [showMonth, setShowMonth] = useState(false);
  const [dataAnggota, setDataAnggota] = useState([]);
  const [dataBulan, setDataBulan] = useState([]);
  const [filterToggle, setFilterToggle] = useState(false);
  const [biroSelected, setBiroSelected] = useState(
    DefaultProps ? DefaultProps : "PTI"
  );
  const [show, setShow] = useState(false);

 function fetchMonth() {
    const token = localStorage.getItem("token")
    const fetchOption = {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    axios
        .get("https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/", fetchOption)
        .then((res) => {
          console.log(res.data.data.bulan)
          const dat = res.data.data.bulan
          setCurrentMonth(dat)
        })
        .catch((err) => {
          // console.log('ERR',err)
        });
  }

  function firstFetch() {
    const token = window.localStorage.getItem("token");
    const fetchOption = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    axios
      .get("https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/month/", fetchOption)
      .then((res) => {
        setDataBulan(listedBulan(res.data.data));
      })
      .catch((err) => console.log(err));

    axios
      .get("https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/", fetchOption)
      .then((res) => {
        setMonth(res.data.data.bulan.nama);
      })
      .catch((err) => console.log(err));
  }

  function fetchData() {
    const token = localStorage.getItem("token") || getCookie("bs-token");
    const fetchOption = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    axios
      .get(
        `https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/${biroSelected}/?month=${month}`,
        fetchOption
      )
      .then((res) => {
        const data = res.data.data;
        setDataAnggota(data);
      })
      .catch((err) => setShow(true));
  }

  const listedBulan = (bulan) => {
    let bulanVerified = [];
    for (let i = 0; i < bulan.length; i++) {
      bulanVerified[i] = bulan[i].nama;
    }
    return bulanVerified;
  };

  const AddBestStaff = (data)=>{
    const token = window.localStorage.getItem("token")
    const payload = {
      id: currentMonth.id,
      npm: data.npm,
      nama: data.name,
    };
    const fetchOption = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    Swal.fire({
      title: 'Anda yakin ?',
      text: "Anggota akan dijadikan Best Staff",
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, tambahkan dia!'
    }).then((result) => {
      if (result.isConfirmed) {
        axios
      .post(
        "https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/best-staff/monthly/",
        payload,
        fetchOption
      )
      .then((res) => {
        MySwal.fire("Success", `${payload.nama} has been added`, "success");
      })
      .catch((err) => {
        MySwal.fire("Failed", `Gagal menambahkan Best Staff`, "error");
      });
      }
    })
  }

  useEffect(() => {
    firstFetch();
    fetchMonth();
  }, []);

  useEffect(() => {
    fetchData();
  }, [month, biroSelected]);

  const FormattingNumber = (Data)=>{
    Data = Data.toString();
    const Segment = Data.split('.')
    if(Segment.length > 1){
      const Fraction = Segment[1].split('');
      if(Fraction.length > 1){
        return Segment[0] + '.' + Fraction[0]  + Fraction[1];
      }
      
      return Segment[0] + '.' + Fraction[0];
    }else{
      return Data
    }
  }

  function ShowData(data) {
    try {
      const statusVote = data.is_vote ? "Yes" : "No";
      const average = FormattingNumber(data.avg);
      const Q1 = FormattingNumber(data.result[0].q1);
      const Q2 = FormattingNumber(data.result[1].q2);
      const Q3 = FormattingNumber(data.result[2].q3);
      const Q4 = FormattingNumber(data.result[3].q4);
      const Q5 = FormattingNumber(data.result[4].q5);
      return (
        <tr>
          <th style={{ textAlign: "left" }}>{data.name}</th>
          <th>{Q1}</th>
          <th>{Q2}</th>
          <th>{Q3}</th>
          <th>{Q4}</th>
          <th>{Q5}</th>
          <th>{average}</th>
          <th>{statusVote}</th>
          <th><Button variant="outline-dark" onClick={()=>{AddBestStaff(data)}}>ADD</Button></th>
        </tr>
      );
    } catch (e) {
      return null;
    }
  }
  return (
    <div>
      {show ? (
        <Alert variant="danger" onClose={() => setShow(false)} dismissible>
          <Alert.Heading>Eror 500</Alert.Heading>
          <p>Maaf atas ketidaknyamanannya, terdapat kesalahan dalam database</p>
        </Alert>
      ) : null}
      <div
        style={{
          position: "relative",
          width: "100%",
          height: "100%",
          background: "red",
        }}
      >
        <NavbarAdmin></NavbarAdmin>
      </div>
      <Selection>
        <Text>{PairBiro[biroSelected]}</Text>
        
        <BiroFilter
          onClick={() => {
            setFilterToggle(setShowMonth(!showMonth));
          }}
          style={{ top: "85%" }}
        >
          <span style={{ margin: "auto 20px" }}>{month}</span>
          <Vector
            style={{
              top: "30%",
              position: "absolute",
              right: "10%",
            }}
          ></Vector>
          {showMonth ? (
            <Dropdown>
              {dataBulan.map((nama) => (
                <>
                  <DData
                    data-value={nama}
                    onClick={() => {
                      setMonth(nama);
                    }}
                  >
                    {nama}
                  </DData>
                  <hr
                    style={{
                      width: "160px",
                      margin: "0px",
                      left: "15%",
                      position: "relative",
                    }}
                  ></hr>
                  {/* <Border/> */}
                </>
              ))}
            </Dropdown>
          ) : null}
        </BiroFilter>

        <FilterButton
          onClick={() => {
            setFilterToggle(!filterToggle);
          }}
        >
          <span style={{ margin: "auto 20px" }}>{biroSelected}</span>
          <Vector
            style={{
              top: "30%",
              position: "absolute",
              right: "10%",
            }}
          ></Vector>
          {filterToggle ? (
            <Dropdown>
              {DaftarBiro.map((nama) => (
                <>
                  <DData
                    data-value={nama}
                    onClick={() => {
                      setBiroSelected(nama);
                    }}
                  >
                    {nama}
                  </DData>
                  <hr
                    style={{
                      width: "160px",
                      margin: "0px",
                      left: "15%",
                      position: "relative",
                    }}
                  ></hr>
                  {/* <Border/> */}
                </>
              ))}
            </Dropdown>
          ) : null}
        </FilterButton>
      </Selection>
      <StaffData>
        <TableContainer>
          <Table striped bordered hover responsive="sm md lg">
            <thead style={{ background: "#F99622", textAlign: "center" }}>
              <tr>
                <th style={{ color: "white" }}>Nama</th>
                <th style={{ color: "white" }}>Q1</th>
                <th style={{ color: "white" }}>Q2</th>
                <th style={{ color: "white" }}>Q3</th>
                <th style={{ color: "white" }}>Q4</th>
                <th style={{ color: "white" }}>Q5</th>
                <th style={{ color: "white" }}>Average</th>
                <th style={{ color: "white" }}>Status Vote</th>
                <th style={{ color: "white" }}>Add BS</th>
              </tr>
            </thead>
            <tbody style={{ textAlign: "center", background: "#E6E7E8" }}>
              {dataAnggota.map((data) => ShowData(data))}
            </tbody>
          </Table>
        </TableContainer>
      </StaffData>

      <Description>
        <DescriptionKeterangan>
          <h4
            style={{
              fontWeight: "700",
              fontSize: "25px",
            }}
          >
            Keterangan
          </h4>
          <div
            style={{
              position: "relative",
              width: "70px",
              backgroundColor: "#F99622",
              border: "3px solid #F99622",
              left: "3px",
              top: "5px",
              background: "#F99622",
            }}
          ></div>
          <ul
            style={{
              position: "relative",
              top: "10px",
            }}
          >
            <li>Q1 = Rata-rata Skor pernyataan 1</li>
            <li>Q2 = Rata-rata Skor pernyataan 2</li>
            <li>Q3 = Rata-rata Skor pernyataan 3</li>
            <li>Q4 = Rata-rata Skor pernyataan 4</li>
            <li>Q5 = Rata-rata Skor pernyataan 5</li>
            <li>
              <strong>Status Vote</strong>
            </li>
            <ul>
              <p>Yes = Sudah vote</p>
              <p>No = Belum vote</p>
            </ul>
          </ul>
        </DescriptionKeterangan>
      </Description>
      <FooterContainer>
        <AdminFooter />
      </FooterContainer>
    </div>
  );
}
export default BiroDepartemen;
