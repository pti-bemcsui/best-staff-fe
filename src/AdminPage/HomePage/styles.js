import styled from "styled-components";

export const BiroPair = {
  "BISNIS DAN KEMITRAAN": "Bismit",
  "PENGEMBANGAN TEKNOLOGI INFORMASI": "PTI",
  "PENGEMBANGAN SUMBER DAYA MANUSIA": "PSDM",
  "KAJIAN DAN AKSI STRATEGIS": "Kastrat",
  "PENGABDIAN MASYARAKAT": "Pengmas",
  "ADVOKASI KESEJAHTERAAN MAHASISWA": "Akadesma",
  "HUBUNGAN MASYARAKAT": "Humas",
  MEDIA: "Media",
  KEILMUAN: "Keilmuan",
  Depor: "Olahraga",
  "SENI BUDAYA": "Senbud",
  "": "",
};

export const NoName = styled.h1`
  position: relative;
  text-align: center;
`;

export const DateRangePopup = styled.div`
  position: relative;
  top: 0%;
  left: 24%;
  background: red;
  width: 0px;
`;

export const CurrentMonthName = styled.p`
  color: #adadac;
  left: 15%;
  position: relative;
  top: 25%;
  width: 0px;
`;

export const MonthSelectTitle = styled.p`
  // position: absolute;
  // height: 54px;
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  // line-height: 45px;
  text-align: center;
  padding: 0;
  margin: 0;
  color: #0f3b5e;
  // top : 15%;
  // left : 34%;
  margin-top: 50px;
  @media (max-width: 854px) {
    left: 13%;
    top: 25%;
  }
`;

export const EditSelectTitle = styled.p`
  // position: absolute;
  // height: 54px;
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  letter-spacing: -0.3px;
  text-align: center;
  color: #0f3b5e;
  padding: 0;
  margin: 0;
  // left : 30%;
  // top : 15%;
  margin-top: 50px;
  @media (max-width: 384px) {
    left: 14%;
    top: 30%;
  }
`;

export const Line = styled.div`
  position: relative;
  width: 152px;
  height: 0px;
  border: 5px solid #ff7c1f;
  left: 42%;
  top: -30px;
  margin: 0px;
  @media (max-width: 360px) {
    left: 25%;
  }
`;

export const ManualContainer = styled.div`
  position: absolute;
  top: 70%;
  left: 38%;
  margin: 0 auto;
  @media (max-width: 854px) {
    position: relative;
    top: 50px;
    // left : 40%;
    left: 0%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center;
  }
  // @media(max-width:854px){
  //     top : 100%;
  //     left : 33%;
  // }
`;

export const DropMonthContainer = styled.div`
  position: relative;
  width: 100%;
  height: auto;
  top: 40%;
  left: 0%;
  background: #ffffff;
  padding-top: 5%;
  ${
    "" /* display : flex;
    flex-direction: column; */
  }
  box-shadow: 0px 3px 4px rgba(0, 0, 0, 0.25);
  border-radius: 15px;
  z-index: 5;
`;

export const MonthChange = styled.div`
  height: 54px;
  width: 100%;
  border-radius: 15px;
  position: relative;
  background: #ffffff;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
`;

export const MonthInput = styled.input`
  position: relative;
  width: 60%;
  top: 20%;
  left: 10%;
`;

export const ButtonChangeMonth = styled.button`
  position: relative;
  border: 1px solid gold;
  background-color: #ffffff;
  border-radius: 10%;
  left: 13%;
  top: 0%;
  width: auto;
  height: 50px;
  &:hover {
    background-color: gold;
  }
`;

export const MIDescription = styled.p`
  position: relative;
  top: 30%;
  text-align: left;
  left: 8%;
  font-size: 14px;
  margin: 10px 10px;
`;

export const ButtonPop = styled.button`
  display: flex;
  z-index: 1;

  margin: auto;
  padding: 12px 48px;
  background: #f99622;
  border: 0;
  border-radius: 12px;
  outline: none;

  font-size: 20px;
  color: white;
  cursor: pointer;
`;
export const Popup = styled.section`
  display: flex;
  flex-direction: column;
  width: 500px;
  height: auto;
  z-index: 1;
  padding: 2% 3%;
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  margin: 0;
  background: white;
  border-radius: 24px;
  filter: drop-shadow(0px 2px 2px rgba(0, 0, 0, 0.25));
`;

export const PublishButton = styled.button`
  height: 54px;
  width: 234px;
  border-radius: 15px;
  background: #0f3b5e;
  color: #ffffff;
  text-align: center;
  font-weight: 500;
  border: 0px;
  // left : 40%;
  // top : 35%;
  position: relative;
  &:hover {
    color: #ebebeb;
    transition: 0.3s;
  }
  &:active {
    background: red;
  }
`;

export const MonthlyTitle = styled.p`
  font-family: Poppins;
  font-size: 40px;
  font-weight: 800;
  color: #0f3b5e;
  margin-bottom: 80px;
`;

export const JurusanTitle = styled.div`
  width: 100%;
  background: white;
  height: 100px;
  top: -20px;
  position: relative;
`;
export const BsContainerTitle = styled.p`
  font-family: Poppins;
  font-size: 30px;
  font-style: normal;
  font-weight: 800;
  line-height: 45px;
  letter-spacing: 0em;
  text-align: center;
  position: absolute;
`;
export const ChartContainerTitle = styled.p`
  font-family: Poppins;
  font-size: 30px;
  font-style: normal;
  font-weight: 700;
  line-height: 45px;
  letter-spacing: 0em;
  text-align: center;
  position: absolute;
  color: #0f3b5e;
  top: -15%;
  left: 30%;
  @media (max-width: 1176px) {
    left: 5%;
    font-size: 36px;
  }
`;

export const ChartContainerPercentage = styled.p`
  font-family: Poppins;
  font-size: 20px;
  font-style: normal;
  font-weight: 600;
  line-height: 40px;
  letter-spacing: 0em;
  text-align: center;
  position: absolute;
  color: white;
  bottom: 32%;
  left: 42%;
  @media (max-width: 1176px) {
    position: relative;
    left: 0%;
    bottom: 55%;
  }
`;

export const InputContainer = styled.div`
  width: 70%;
  position: relative;
  // @media(max-width:645px){

  // }
`;
export const PlusBTNContainer = styled.div`
  width: 0%;
  position: relative;
  cursor: pointer;
  @media (max-width: 1176px) {
    left: 20%;
  }
  @media (max-width: 550px) {
    left: 30%;
    top: 7%;
    margin: 0px auto;
  }
`;
export const ScrollLinearContainer = styled.div`
    position: relative;
    display : flex;
    width : 100%;
    height : 40px;
    margin-bottom : 20px;x
    margin-bottom : 20px;
    @media screen and (max-width:550px){
        flex-direction: column;
        height : 60px;
    }
`;

export const ScrollBSContainer = styled.div`
  width: 80%;
  height: 70%;
  ${"" /* background : red; */}
  left : 20%;
  top: 15%;
  position: relative;
  overflow: auto;
  @media (max-width: 1176px) {
    left: 8vw;
  }
  @media screen and (max-width: 550px) {
    left: 6vw;
    width: 90%;
  }
`;

export const StartDate = styled.input`
  position: relative;
  height: 54px;
  top: 15px;
  background: #ffffff;
  border: 1px solid #d9d9d9;
  boxsizing: border-box;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  border-radius: 15px;
  padding: 0% 2%;
  height: 50px;
  width: 150px;
  left: 23%;
  color: #adadac;
  @media (max-width: 1000px) {
    left: 18%;
  }
  @media (max-width: 854px) {
    left: 28%;
  }
  @media (max-width: 802px) {
    left: 25%;
  }
  @media (max-width: 740px) {
    left: 22%;
  }
  @media (max-width: 740px) {
    left: 20%;
  }
  @media (max-width: 550px) {
    left: 15%;
  }
  @media (max-width: 410px) {
    left: 10%;
  }
  @media (max-width: 384px) {
    left: 8%;
  }
  @media (max-width: 355px) {
    left: 3%;
  }
  @media (max-width: 330px) {
    height: 45px;
    width: 120px;
    left: 8%;
  }
  @media (max-width: 300px) {
    left: 6%;
  }
`;

export const EndDate = styled.input`
  position: relative;
  height: 54px;
  background: #ffffff;
  border: 1px solid #d9d9d9;
  boxsizing: border-box;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  border-radius: 15px;
  padding: 0% 2%;
  height: 50px;
  width: 150px;
  left: 35%;
  top: 15px;
  color: #adadac;
  @media (max-width: 1000px) {
    left: 25%;
  }
  @media (max-width: 854px) {
    left: 35%;
  }
  @media (max-width: 740px) {
    left: 38%;
  }
  @media (max-width: 740px) {
    left: 35%;
  }
  @media (max-width: 640px) {
    left: 32%;
  }
  @media (max-width: 580px) {
    left: 30%;
  }
  @media (max-width: 480px) {
    left: 20%;
  }
  @media (max-width: 384px) {
    left: 13%;
  }
  @media (max-width: 345px) {
    left: 5%;
  }
  @media (max-width: 330px) {
    height: 45px;
    width: 120px;
    left: 16%;
  }
  @media (max-width: 290px) {
    left: 10%;
  }
`;

export const DataShowSection = styled.div`
  width: 100%;
  height: 900px;
  ${"" /* background : red; */}
  @media(max-width:1176px) {
    height: 1100px;
  }
  @media (max-width: 360px) {
    margin-top: 10%;
  }
`;

export const BiroCard = styled.div`
  height: 223px;
  width: 250.55px;
  left: 963.87353515625px;
  top: 2466px;
  border-radius: 40px;
  background: blue;
  margin-left: 130px;
  margin-top: 90px;
  background: #ffffff;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
`;

export const CardContainer = styled.div`
  height: 220px;
  width: 250.5487823486328px;
  left: 963.87353515625px;
  top: 2466px;
  border-radius: 40px;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  top: 0px;
  // margin-left : 7%;
  // margin-top : 6%;
  background: white;
`;

export const Image = styled.div`
  height: 130px;
  width: 110px;
  position: absolute;
  top: 110px;
  left: 195px;
  background-size: 100% 100%;
  background-image: url(${(props) => props.src});
`;

export const SelectTitle = styled.p`
  position: absolute;
  height: 54px;
  font-family: Poppins;
  font-style: normal;
  font-weight: 500;
  font-size: 30px;
  line-height: 45px;
  text-align: center;
  color: #000000;
`;

export const SelectorBoxStart = {
  position: "absolute",
  height: "54px",
  top: "40%",
  background: "#FFFFFF",
  border: "1px solid #D9D9D9",
  boxSizing: "border-box",
  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.25)",
  borderRadius: "15px",
  padding: "1% 2%",
  height: "50px",
  width: "150px",
  left: "15%",
};

export const SelectorBoxEnd = {
  position: "absolute",
  height: "54px",
  top: "40%",
  background: "#FFFFFF",
  border: "1px solid #D9D9D9",
  boxSizing: "border-box",
  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.25)",
  borderRadius: "15px",
  padding: "1% 2%",
  height: "50px",
  width: "150px",
  left: "60%",
};

export const Text = styled.p`
  color: #adadac;
  position: absolute;
  z-index: 1;
`;

export const ChartBox = styled.div`
  position: relative;
  width: 600px;
  height: 450px;
  left: 0%;
  top: 10%;
  background: #0f3b5e;
  border-radius: 0px 250px 250px 0px;
  padding-bottom: 30px;
  padding-top: 30px;
  // border: 1px solid green;
  z-index: 0;
  @media (max-width: 1176px) {
    width: 100%;
    border-radius: 0px;
    top: 16%;
  }
`;
export const BestStaffBox = styled.div`
  position: relative;
  float: right;
  width: 630px;
  height: 450px;
  top: 5%;
  background: #0f3b5e;
  border-radius: 250px 0px 0px 250px;
  z-index: 1;

  @media (max-width: 1176px) {
    margin-left: 0px;
    left: 0%;
    border-radius: 0px;
    width: 100%;
    top: 30%;
  }
`;

export const BestStaffBackground = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
  // position: relative;
  ${"" /* width: 1400px; */}
  // height : 1000px;
    padding: 100px;
  width: 100%;
  background: #f99d32;
  @media (max-width: 1176px) {
    margin-top: 100px;
  }
`;
export const PersonImage = styled.div`
  height: 280px;
  width: 100%;
  left: 0%;
  top: 0%;
  background-size: 100% 100%;
  margin-top: -7%;
  position: relative;
  background-image: url(${(props) => props.src});
  border: 1px solid black;
  box-shadow: 10px 0px 20px rgba(0, 0, 0, 0.25);
  @media (max-width: 384px) {
    height: 250px;
  }
`;

export const BirDep = styled.section`
  display: flex;
  flex-wrap: wrap;
  padding: 0vh 10vw;
  margin: 5rem 0 10rem 0;
  gap: 5rem;
  justify-content: center;
  align-items: flex-start;
  // width : 92%;
  background: #ffffff;
`;

export const PlusButton = styled.div`
  height: 40px;
  width: 40px;
  background-size: 100% 100%;
  position: relative;
  background-image: url("https://cdn.discordapp.com/attachments/755605623214964900/841691701843722250/Vector.png");
  @media (max-width: 384px) {
    left: -25%;
    margin-top: 10%;
  }
`;

export const InputStyle = styled.input`
  type: text;
  background: #efe6ec;
  height: 50px;
  width: 300px;
  left: 10%;
  top: 0px;
  border-radius: 30px;
  padding: 3% 3%;
  font-size: 13px;
  position: absolute;
  @media (max-width: 1176px) {
    width: 60vw;
    font-size: 16px;
  }
  @media (max-width: 550px) {
    left: 0px;
    width: 65vw;
    // width: 100%;
    font-size: 13px;
  }
`;

export const ImageContainer = styled.div`
  width: 300px;
  z-index: 3;
  ${"" /* background : red; */}
  position : relative;
  // left : 38%;
  // top : 30%;
  display: flex;
  flex-direction: column;
  margin-bottom: 40px;
  @media (max-width: 384px) {
    // left : 13%;
    width: 250px;
  }
  // @media(max-width:360px){
  //     left : 10%;
  // }
`;

export const BiroJurusanContainer = styled.div`
  width: 100%;
`;

export const TextContainer = styled.div`
  width: 300px;
  height: 80px;
  background: #fafafa;
  top: -60px;
  position: relative;
  text-align: center;
  box-shadow: 10px 0px 20px rgba(0, 0, 0, 0.25);
`;

export const BirdepTitle = styled.div`
  margin: 0;
  color: black;
  font-size: 22px;
  font-weight: 500;
  z-index: 5;
  width: 100%;
  background-color: #ffffff;
  ${"" /* left : 35%; */}
  position : relative;
  text-align: center;
  word-wrap: break-word;
`;
export const BiroHeadline = styled.p`
  position: relative;
  height: 78px;
  ${"" /* top : 1%; */}
  left : 35%;
  font-family: Poppins;
  font-style: normal;
  font-weight: 800;
  font-size: 40px;
  z-index: 2;
  color: #0f3b5e;
  margin-top: 0px;
  @media (max-width: 638px) {
    font-size: 35px;
  }
  @media (max-width: 578px) {
    left: 20%;
  }
  @media (max-width: 384px) {
    left: 5px;
    top: 0px;
  }
`;

export const DateSection = styled.div`
  width: 100%;
  height: 250px;
  ${"" /* background : red; */}
  position : relative;
  display: flex;
  flex-direction: row;
  @media (max-width: 854px) {
    flex-direction: column;
    height: 500px;
  }
`;

export const DateSectionLeft = styled.div`
  float: left;
  width: 50%;
  height: 300px;
  margin: 0 auto;
  ${"" /* background : pink; */}
  position : relative;
  @media (max-width: 854px) {
    width: 100%;
    height: 600px;
  }
`;
export const DateSectionRight = styled.div`
  float: left;
  width: 50%;
  height: 300px;
  margin: 0 auto;
  ${"" /* background : blue; */}
  position : relative;
  @media (max-width: 854px) {
    width: 100%;
    height: 500px;
  }
`;
export const MonthSelect = styled.div`
  height: 54px;
  width: 295px;
  // left: 30%;
  top: 5%;
  margin: 0 auto;
  border-radius: 15px;
  position: relative;
  background: #ffffff;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  @media (max-width: 330px) {
    height: 45px;
    width: 260px;
  }
`;

export const Vector = styled.div`
  background-image: url("https://cdn.discordapp.com/attachments/755605623214964900/838065786676707348/Vector.png");
  height: 10px;
  width: 20px;
  background-size: 100% 100%;
  position: absolute;
  margin-left: 10px;
  margin-top: 5px;
  z-index: 1;
`;
