import { useState, useEffect } from "react";
import NavbarAdmin from "../../Component/Admin/NavbarAdmin";
import AdminFooter from "../../Component/Admin/AdminFooter";
import {
  PublishButton,
  ButtonPop,
  Popup,
  MonthInput,
  ButtonChangeMonth,
  MIDescription,
  MonthChange,
  ManualContainer,
  DateRangePopup,
  MonthlyTitle,
  JurusanTitle,
  ChartContainerTitle,
  ChartContainerPercentage,
  BsContainerTitle,
  InputContainer,
  Line,
  PlusBTNContainer,
  ScrollLinearContainer,
  ScrollBSContainer,
  StartDate,
  EndDate,
  BestStaffBox,
  DataShowSection,
  DateSection,
  DateSectionRight,
  DateSectionLeft,
  MonthSelect,
  CurrentMonthName,
  NoName,
  BiroCard,
  CardContainer,
  Image,
  SelectTitle,
  ChartBox,
  MonthSelectTitle,
  EditSelectTitle,
  BestStaffBackground,
  PersonImage,
  BirDep,
  PlusButton,
  InputStyle,
  ImageContainer,
  TextContainer,
  BiroHeadline,
  BirdepTitle,
  DropMonthContainer,
} from "./styles";
import { Redirect, Link } from "react-router-dom";
import { Doughnut } from "react-chartjs-2";
import axios from "axios";
import Failed from "./Failed.png";
import Success from "./Success.png";
import Button from "react-bootstrap/Button";
import { getCookie } from "../../helpers/cookies";

// Date css
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import { DateRange } from "react-date-range";

import { ListBiroDepartemen } from "./BiroDepartemen";

//---------Sweet Alert ----------------
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const MySwal = withReactContent(Swal);
//--------Sweet Alert-------------------

function HomePage() {
  localStorage.setItem("DataUsed", "Now");
  localStorage.setItem("Month", "None");
  const [month, setMonth] = useState("Select Month");
  const NamaBulan = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];
  const [showMonth, setShowMonth] = useState(false);
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [topTen, setTopTen] = useState();
  const [totalVote, setTotalVote] = useState(0);
  const [totalAnggota, setTotalAnggota] = useState(150); // Current TOTAL Member in BEM UI FASILKOM
  const [nonVote, setNonVote] = useState(0);
  const [best_staff, setBest_Staff] = useState({});
  const [isAdmin, setIsAdmin] = useState(true);
  const [currentDate, setCurrentDate] = useState({});
  const [changeDate, setChangeDate] = useState(false);
  const [datePayload, setDatePayload] = useState([]);
  const [CreateMonth, setCreateMonth] = useState(false);
  const [TopCandidate, setTopCandidate] = useState({});
  const [AddCandidate, setAddCandidate] = useState(false);
  const [MonthValid, setMonthValid] = useState(true);
  const [success, isSuccess] = useState(false);
  const [failed, isFailed] = useState(false);
  const [refresher, setRefresher] = useState(false);
  const [backToPrevious, setBackToPrevious] = useState(false);
  const [calendarOpen, setCalendarOpen] = useState(false);
  const token = localStorage.getItem("token") || getCookie("bs-token");
  const [selectMonth, setSelectedMonth] = useState("None");
  const [previousMonth, setPreviousMonth] = useState([]);
  const [state, setState] = useState([
    {
      startDate: null,
      endDate: null,
      key: "selection",
    },
  ]);
  const [popupMonth, setPopupMonth] = useState(false);

  const FormattingNumber = (Data)=>{
    Data = Data.toString();
    const Segment = Data.split('.')
    if(Segment.length > 1){
      const Fraction = Segment[1].split('');
      if(Fraction.length > 1){
        return Segment[0] + '.' + Fraction[0]  + Fraction[1];
      }
      
      return Segment[0] + '.' + Fraction[0];
    }else{
      return Data
    }
  }

  const isBulanVerified = (data) => {
    if (NamaBulan.includes(data)) return true;
    return false;
  };

  const isAlreadyExist = (nameMonth) => {
    if (previousMonth.length > 0) {
      for (let i = 0; i < previousMonth.length; i++) {
        if (previousMonth[i].nama === nameMonth) return true;
      }
    }

    return false;
  };

  const DebugAnggota = () => {
    window.location.assign("/best-staff/admin/DataAnggota/Crud");
  };

  async function fetchData() {
    const fetchOption = {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    if (localStorage.getItem("DataUsed") === "Now") {
      axios
        .get("https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/", fetchOption)
        .then((res) => {
          const dat = res.data;
          const data = dat.data;
          setTopTen(topTenData(data.top_ten));
          setBest_Staff(data.best_staff);
          setCurrentDate(data.bulan);
          localStorage.setItem("Month", data.bulan);
          const HasVote = data.total_vote;
          const NotVote = totalAnggota - HasVote;
          setTotalVote(HasVote);
          setNonVote(NotVote);
        })
        .catch((err) => {
          MySwal.fire({
            title: "Data error",
            text: "Ada anggota yang tidak mempunyai foto, apakah ingin mengubahnya ? ",
            icon: "error",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ubah data angggota",
          }).then((result) => {
            if (result.isConfirmed) {
              DebugAnggota();
            } else {
              setRefresher(!refresher);
            }
          });
        });
    }
  }

  const GetPrevious = () => {
    const fetchOption = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios
      .get("https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/month/", fetchOption)
      .then((res) => {
        const data = res.data.data;
        setPreviousMonth(data);
      });
      // .catch((err) => {
      //   console.log("Previous not Found : ", err);
      // });
  };

  const imageValidate = (link) => {
    var res =
      "https://cdn.discordapp.com/attachments/755605623214964900/859573873053138980/NoImage.png";
    var linkImage = link;
    if (linkImage) if (linkImage.includes("http")) res = linkImage;
    return res;
  };

  const totalVotePercentage = (
    (totalVote * 100) /
    (totalVote + nonVote)
  ).toFixed(2);

  const topTenData = (Data) => {
    var n = 12 - Data.length;
    var result = [];
    for (let i = 0; i < Data.length; i++) {
      result.push(Data[i]);
    }
    for (let i = 0; i < n; i++) {
      result.push({ name: "Tidak Ada", nilai: 0 });
    }
    return result;
  };

  const UpdateDate = (Start, End) => {
    const fetchOption = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    const payload = {
      id: currentDate.id,
      start_date: Start,
      end_date: End,
    };
    axios
      .put(
        "https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/voting-date/",
        payload,
        fetchOption
      )
      .then((res) => {
        MySwal.fire("Success", `Date has been update`, "success");
        setRefresher(!refresher);
      })
      .catch((err) => {
        setStartDate(null);
        setEndDate(null);
        MySwal.fire("Failed", `Date not update`, "error");
      });
  };

  const UpdateMonth = () => {
    const fetchOption = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    MySwal.fire({
      title: "Masukan nama bulan",
      input: "text",
      icon: "info",
      inputValue: "",
      showCancelButton: true,
      inputValidator: (value) => {
        if (!isBulanVerified(value)) {
          return "Nama bulan harus dalam bahasa Indonesia KBBI ! & Huruf depan berupa kapital";
        } else if (isAlreadyExist(value)) {
          return "Month is already exist";
        } else {
          const payload = {
            bulan: value,
          };
          axios
            .post(
              "https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/month/",
              payload,
              fetchOption
            )
            .then((res) => {
              setRefresher(!refresher);
              MySwal.fire({
                icon: "success",
                title: "Suksess !",
                text: "Bulan telah diganti",
              });
            })
            .catch((err) => {
              setMonth("");
              setRefresher(!refresher);
              MySwal.fire({
                icon: "eror",
                title: "Gagal !",
                text: "Bulan telah diganti",
              });
            });
        }
      },
    });
  };

  const UpdtatePreviousMonth = () => {
    const fetchOption = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    MySwal.fire({
      title: "Change Data",
      text: `Admin akan menampilkan bulan ${selectMonth.nama}`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .get(
            `https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/?bulan=${selectMonth.id}`,
            fetchOption
          )
          .then((res) => {
            const dat = res.data;
            const data = dat.data;
            setTopTen(topTenData(data.top_ten));
            setBest_Staff(data.best_staff);
            setCurrentDate(data.bulan);
            const HasVote = data.total_vote;
            const NotVote = totalAnggota - HasVote;
            setTotalVote(HasVote);
            setNonVote(NotVote);

            MySwal.fire("Changed", "Data Show has been changed", "success");
          });
          // .catch((err) => {
          //   console.log(err);
          // });
      }
    });
  };

  const AddTopStaff = () => {
    const fetchOption = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    const payload = {
      id: currentDate.id,
      npm: TopCandidate.npm,
      nama: TopCandidate.name,
    };
    axios
      .post(
        "https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/best-staff/monthly/",
        payload,
        fetchOption
      )
      .then((res) => {
        setTopCandidate("");
        MySwal.fire("Success", `${payload.nama} has been added`, "success");
      })
      .catch((err) => {
        setTopCandidate("");
        MySwal.fire("Failed", `Failed add Best Staff`, "error");
      });
  };

  const Publish = () => {
    MySwal.fire({
      title: "Are you sure?",
      text: "Data akan final dan tidak bisa diubah!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#F99622",
      cancelButtonColor: "#F99622",
      confirmButtonText: "Ya",
    }).then((result) => {
      if (result.isConfirmed) {
        const fetchOption = {
          method: "POST",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };

        fetch(
          "https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/publish/",
          fetchOption
        )
          .then((res) => {
            if (res.status === 201) {
              MySwal.fire({
                icon: "success",
                title: "Sucess",
                text: "Data have been published",
              });
            } else {
              MySwal.fire({
                icon: "error",
                title: "Oops...",
                text: "You have publish it",
              });
            }
          })
          .catch((err) => {
            isFailed(false);
          });
      }
    });
  };

  const ManualEdit = () => {
    MySwal.fire({
      icon: "warning",
      title: "Redirect to server page",
      text: `Format Date : YYYY-MM-DD ||  id Month : ${currentDate.id}`,
    }).then(() => {
      var child = window.open(
        "https://ptibem.cs.ui.ac.id/beststaff/admin/adminpage/month/",
        "",
        "toolbar=0,status=0,width=626,height=436"
      );
      var timer = setInterval(checkChild, 500);

      function checkChild() {
        if (child.closed) {
          setRefresher(!refresher);
          clearInterval(timer);
        }
      }
    });
  };

  useEffect(() => {
    fetchData();
    GetPrevious();
  }, [refresher]);
  useEffect(() => {
    if (selectMonth === "None") {
    } else {
      UpdtatePreviousMonth();
    }
  }, [selectMonth]);
  useEffect(() => {}, [nonVote]);
  useEffect(() => {
    if (state[0].startDate && state[0].endDate) {
      let SD_Day = state[0].startDate.getDate();
      let SD_Month = state[0].startDate.getMonth() + 1;
      let SD_Year = state[0].startDate.getYear() + "";
      SD_Year = "20" + SD_Year.substring(1, 3);
      let SD = `${SD_Day}-${SD_Month}-${SD_Year}`;

      let ED_Day = state[0].endDate.getDate();
      let ED_Month = state[0].endDate.getMonth() + 1;
      let ED_Year = state[0].endDate.getYear() + "";
      ED_Year = "20" + ED_Year.substring(1, 3);
      let ED = `${ED_Day}-${ED_Month}-${ED_Year}`;

      if (!(SD === ED)) {
        Swal.fire({
          title: "Are you sure?",
          text: `Date will change from ${SD} to ${ED}`,
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Yes, change it!",
        }).then((result) => {
          if (result.isConfirmed) {
            UpdateDate(SD, ED);
          } else {
            setStartDate(null);
            setEndDate(null);
          }
        });
        setDatePayload([SD, ED]);
        setPopupMonth(false);
      }
    }
  }, [state]);
  const data = {
    labels: ["Yes", "No"],
    datasets: [
      {
        label: "Vote Distribution",
        backgroundColor: ["#FF7C1F", "#EFE6EC"],
        borderColor: "rgba(255,99,132,1)",
        borderWidth: 1,
        hoverBackgroundColor: "rgba(255,99,132,0.4)",
        hoverBorderColor: "rgba(255,99,132,1)",
        data: [totalVote, nonVote],
      },
    ],
  };

  const monthValidate = () => {
    var found = false;
    for (let i = 0; i < NamaBulan.length; i++) {
      if (month === NamaBulan[i]) {
        found = true;
      }
    }
    if (found) {
      setCreateMonth(true);
    } else {
      setMonthValid(false);
    }
  };

  return (
    <div>
      {/* Popup */}

      {success ? (
        <div
          style={{
            width: "100%",
            height: "100%",
            position: "fixed",
            background: "rgba(0,0,0,0.25)",
            top: "0",
            left: "0",
            zIndex: "10",
          }}
        >
          <Popup>
            <img
              src={Success}
              style={{
                width: "100px",
                height: "100px",
                margin: "auto",
                marginTop: "36px",
              }}
            />
            <h3 style={{ textAlign: "center" }}>Publish Successs</h3>
            <ButtonPop onClick={() => isSuccess(false)}>Close</ButtonPop>
          </Popup>
        </div>
      ) : null}

      {MonthValid ? null : (
        <div
          style={{
            width: "100%",
            height: "100%",
            position: "fixed",
            background: "rgba(0,0,0,0.25)",
            top: "0",
            left: "0",
            zIndex: "10",
          }}
        >
          <Popup>
            <img
              src={Failed}
              style={{
                width: "100px",
                height: "100px",
                margin: "auto",
                marginTop: "36px",
              }}
            />
            <h3 style={{ textAlign: "center" }}>
              Masukan <strong>Bulan</strong> dengan benar!
            </h3>
            <ButtonPop onClick={() => setMonthValid(!MonthValid)}>
              Close
            </ButtonPop>
          </Popup>
        </div>
      )}

      {changeDate ? (
        <div
          style={{
            width: "100%",
            height: "100%",
            position: "fixed",
            background: "rgba(0,0,0,0.25)",
            top: "0",
            left: "0",
            zIndex: "10",
          }}
        >
          <Popup>
            <br />
            <h3 style={{ textAlign: "center" }}>Ganti Date</h3>
            <p style={{ textAlign: "center" }}>
              Apakah yakin ingin mengubah data
            </p>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <ButtonPop
                onClick={() => {
                  setChangeDate(false);
                  UpdateDate();
                }}
              >
                Ya
              </ButtonPop>
              <ButtonPop
                onClick={() => {
                  setChangeDate(false);
                  setStartDate(null);
                  setEndDate(null);
                }}
              >
                Tidak
              </ButtonPop>
            </div>
          </Popup>
        </div>
      ) : null}

      {AddCandidate ? (
        <div
          style={{
            width: "100%",
            height: "100%",
            position: "fixed",
            background: "rgba(0,0,0,0.25)",
            top: "0",
            left: "0",
            zIndex: "10",
          }}
        >
          <Popup>
            <br />
            <h3 style={{ textAlign: "center" }}>Tambah Month Best Staff</h3>
            <p style={{ textAlign: "center" }}>
              Apakah ingin menambahkan <strong>{`${TopCandidate.name}`}</strong>
              <br /> sebagai Best Month bulan ini
            </p>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <ButtonPop
                onClick={() => {
                  setAddCandidate(false);
                  AddTopStaff();
                }}
              >
                Ya
              </ButtonPop>
              <ButtonPop
                onClick={() => {
                  setAddCandidate(false);
                  setTopCandidate("");
                }}
              >
                Tidak
              </ButtonPop>
            </div>
          </Popup>
        </div>
      ) : null}

      {/* Main Program */}
      <div style={{ position: "relative", width: "100%", height: "100%" }}>
        <NavbarAdmin></NavbarAdmin>
      </div>
      <DateSection>
        <DateSectionLeft>
          <MonthSelectTitle>Current Month</MonthSelectTitle>

          <MonthSelect onClick={() => setShowMonth(!showMonth)}>
            <CurrentMonthName>{currentDate.nama}</CurrentMonthName>
            {showMonth ? (
              <DropMonthContainer
                onMouseLeave={() => {
                  setShowMonth(false);
                }}
              >
                {previousMonth.map((data) => (
                  <div style={{ zindex: "10" }}>
                    <button
                      style={{
                        left: "18%",
                        position: "relative",
                        background: "white",
                        width: "180px",
                        textAlign: "center",
                        border: "none",
                      }}
                      onClick={() => {
                        setSelectedMonth(data);
                      }}
                    >
                      {data.nama}
                    </button>
                    <hr style={{ width: "200px" }}></hr>
                  </div>
                ))}

                <div
                  style={{
                    height: "50px",
                    justifyContent: "center",
                    marginLeft: "29%",
                  }}
                >
                  <Button
                    variant="outline-primary"
                    onClick={() => {
                      UpdateMonth();
                    }}
                  >
                    Add Month
                  </Button>{" "}
                </div>
              </DropMonthContainer>
            ) : null}
          </MonthSelect>
        </DateSectionLeft>

        <DateSectionRight>
          <EditSelectTitle>Edit Voting Date</EditSelectTitle>

          {popupMonth ? (
            <DateRangePopup>
              <DateRange
                editableDateInputs={true}
                onChange={(item) => {
                  setState([item.selection]);
                }}
                moveRangeOnFirstSelection={false}
                ranges={state}
              />
            </DateRangePopup>
          ) : (
            <div>
              <StartDate
                onClick={() => {
                  setPopupMonth(!popupMonth);
                }}
                value={currentDate.start_date}
              ></StartDate>
              <EndDate
                onClick={() => {
                  setPopupMonth(!popupMonth);
                }}
                value={currentDate.end_date}
              ></EndDate>
              {/* <ManualContainer>
                        <Button style={{postion : 'absolute'}} variant="outline-warning" onClick = {ManualEdit}>Change Manual</Button>
                    </ManualContainer> */}
            </div>
          )}
        </DateSectionRight>
      </DateSection>

      <DataShowSection>
        <ChartBox>
          <ChartContainerTitle>Total Vote</ChartContainerTitle>
          <Doughnut
            data={data}
            width={730}
            height={500}
            options={{
              maintainAspectRatio: false,
              plugins: {
                legend: {
                  labels: {
                    // This more specific font property overrides the global property
                    font: {
                      size: 25,
                      color: "white",
                    },
                  },
                },
              },
            }}
          />
          <ChartContainerPercentage>
            Yes : {totalVotePercentage}% <br />
            No : {100 - totalVotePercentage}%
          </ChartContainerPercentage>
        </ChartBox>

        <BestStaffBox>
          <ChartContainerTitle>TOP 12 Best Staff</ChartContainerTitle>
          <ScrollBSContainer>
            {topTen
              ? topTen.map((data) => (
                  <ScrollLinearContainer>
                    <InputContainer>
                      <InputStyle
                        value={
                          data ? `${data.name}(${FormattingNumber(data.nilai)})-${data.birdep}` : "Belum ada"
                        }
                        onChange={(e) => {
                          setTopCandidate(e.target.value);
                        }}
                      ></InputStyle>
                    </InputContainer>
                    <PlusBTNContainer
                      onClick={() => {
                        setAddCandidate(true);
                        setTopCandidate(data);
                      }}
                    >
                      <PlusButton></PlusButton>
                    </PlusBTNContainer>
                  </ScrollLinearContainer>
                ))
              : null}
          </ScrollBSContainer>
        </BestStaffBox>
      </DataShowSection>

      <BestStaffBackground>
        <MonthlyTitle>Monthly Best Staff</MonthlyTitle>
        {best_staff.nama ? (
          <>
            {" "}
            <ImageContainer>
              <PersonImage src={imageValidate(best_staff.foto)}></PersonImage>
              <p
                style={{
                  position: "relative",
                  maxWidth: "300px",
                  height: "auto",
                  left: "0",
                  color: "white",
                  fontSize: "25px",
                  fontWeight: "1000",
                  background: "#0F3B5E",
                  textAlign: "center",
                  alignFont: "center",
                  wordWrap: "break-word",
                }}
              >
                {best_staff.nama ? best_staff.nama : "Tidak ada"}
              </p>
              <JurusanTitle style={{ textAlign: "center" }}>
                <p
                  style={{
                    display: "inline-block",
                    verticalAlign: "middle",
                    position: "relative",
                    top: "20px",
                  }}
                >
                  {best_staff.kode_birdept ? best_staff.kode_birdept : "None"}
                  <br />
                  {best_staff.jurusan} {best_staff.angkatan}
                </p>
              </JurusanTitle>
            </ImageContainer>
            <PublishButton
              onClick={() => {
                Publish();
              }}
            >
              Publish
            </PublishButton>{" "}
          </>
        ) : (
          <NoName>Tidak ada Best Staff</NoName>
        )}
      </BestStaffBackground>

      <br />

      {/* Noot Good Practice  */}
      {/* Can Use Loop */}
      <BirDep>
        {ListBiroDepartemen.map((data) => (
          <CardContainer>
            <Link
              to={{
                pathname: "/best-staff/admin/biro",
                state: `${data.title}`,
              }}
            >
              <Image
                src={data.picture}
                style={{
                  top: "10%",
                  left: "28%",
                  position: "relative",
                }}
              />
              <TextContainer
                style={{
                  boxShadow: "none",
                  width: "97%",
                  height: "50px",
                  top: "8%",
                  left: "3px",
                  backgroundColor: "white",
                }}
              >
                <BirdepTitle>{data.title}</BirdepTitle>
              </TextContainer>
            </Link>
          </CardContainer>
        ))}
      </BirDep>

      <AdminFooter></AdminFooter>
    </div>
  );
}
export default HomePage;
