export const ListBiroDepartemen = [
  {
    title: "PTI",
    picture:
      "https://cdn.discordapp.com/attachments/755605623214964900/866084914700746762/PTI_1.png",
  },
  {
    title: "Pengmas",
    picture:
      "https://cdn.discordapp.com/attachments/755605623214964900/866084976645636096/Pengmas_1.png",
  },
  {
    title: "PSDM",
    picture:
      "https://cdn.discordapp.com/attachments/755605623214964900/866085096381743134/Kontrol_Internal_1.png",
  },
  {
    title: "Depor",
    picture:
      "https://cdn.discordapp.com/attachments/755605623214964900/866085174118973440/Departemen-Olahraga_1.png",
  },
  {
    title: "Senbud",
    picture:
      "https://cdn.discordapp.com/attachments/755605623214964900/866085267328991252/Departemen-Seni-dan-Budaya_1.png",
  },
  {
    title: "Keilmuan",
    picture:
      "https://cdn.discordapp.com/attachments/755605623214964900/866085330675695616/Departemen-Keilmuan_1.png",
  },
  {
    title: "Media",
    picture:
      "https://cdn.discordapp.com/attachments/755605623214964900/866085401504514068/Biro-Media_1.png",
  },
  {
    title: "Kastrat",
    picture:
      "https://cdn.discordapp.com/attachments/755605623214964900/866085727188287518/Kastrat_1.png",
  },
  {
    title: "Bismit",
    picture:
      "https://cdn.discordapp.com/attachments/755605623214964900/866085781453406238/Bismit_1.png",
  },
  {
    title: "Humas",
    picture:
      "https://cdn.discordapp.com/attachments/755605623214964900/866085862810320976/Biro-Hubungan-Masyarakat_1.png",
  },
  {
    title: "Akpem",
    picture:
      "https://cdn.discordapp.com/attachments/755605623214964900/866085947266826270/Akpem_1.png",
  },
  {
    title: "Adkesma",
    picture:
      "https://cdn.discordapp.com/attachments/755605623214964900/866086010843824148/Adkesma_1.png",
  },
];
