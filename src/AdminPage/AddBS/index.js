import { useEffect, useState } from "react";
import NavbarAdmin from "../../Component/Admin/NavbarAdmin";
import AdminFooter from "../../Component/Admin/AdminFooter";
import Popup from "reactjs-popup";
import Select from "react-select";
import axios from "axios";
import { GrouppingByName } from "./Groupping";
import Alert from "react-bootstrap/Alert";
import {
  ImageLeft,
  IdentityRight,
  ImagePopup,
  biggerFont,
  DeleteButton,
  Button,
  PopupConfirm,
  customstyle,
  ModalWrapper,
  Title,
  StaffNameContainer,
  MessageContainer,
  AddButton,
  BestStaffAdd,
  Vector,
  BestStaffContainer,
  Image,
  BiroNameContainer,
  CardContainer,
  BirdepTitle,
  PreviousNameContainer,
  PreviousBestStaff,
  StaffInfoPopup,
  BestStaffInfo,
} from "./styles";
import { getCookie } from "../../helpers/cookies";
import ModalHeader from "react-bootstrap/ModalHeader";
import Modal from "react-bootstrap/Modal";
import ModalBody from "react-bootstrap/ModalBody";

export default function AddBestStaff() {
  const [stafToAdd, setStaffToAdd] = useState("");
  const [AnggotaBem, setAnggotaBem] = useState([]);
  const [NamaAnggota, setNamaAnggota] = useState([]);
  const [KesanPesan, setKesanPesan] = useState("");
  const [YearlyBestStaff, setYearlyBestStaff] = useState([]);
  const [AddYearly, setAddYearly] = useState(false);
  const [success, setSuccess] = useState(false);
  const [failed, setFailed] = useState(false);
  const [show, setShow] = useState(false);
  const [dataShow, setDataShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const onFocus = ({ focused, isDisabled }) => {
    const msg = `${focused.label}`;
    setStaffToAdd(msg);
  };

  function ObjectFind(nama) {
    var obj = null;
    AnggotaBem.map((data) => {
      if (data.nama === nama) {
        obj = data;
      }
    });
    return obj;
  }

  function PostData() {
    const ObjectToAdd = ObjectFind(stafToAdd);
    try {
      var payload = {
        npm: ObjectToAdd.npm,
        name: ObjectToAdd.nama,
        kesan_pesan: KesanPesan,
      };
    } catch (e) {
      var payload = {
        npm: "",
        name: "",
        kesan_pesan: "",
      };
    }

    const token = localStorage.getItem("token") || getCookie("bs-token");
    const fetchOption = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    axios
      .post(
        "https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/best-staff/yearly/",
        payload,
        fetchOption
      )
      .then((res) => {
        setSuccess(true);
      })
      .catch((err) => {
        setFailed(true);
      });
  }

  function DeleteYearBestStaff(data) {
    const token = localStorage.getItem("token");
    const fetchOption = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    const URL = `https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/best-staff/yearly/delete/${data.npm}`;
    axios
      .delete(URL, fetchOption)
      .then((res) => {
        setSuccess(true);
      })
      .catch((err) => setFailed(true));
  }

  function fetchData() {
    const token = localStorage.getItem("token");
    const fetchOption = {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    axios
      .get(
        "https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/anggota-bem/",
        fetchOption
      )
      .then((res) => {
        const finalData = res.data.data;
        setNamaAnggota(GrouppingByName(finalData));
        setAnggotaBem(finalData);
      })
      .catch((err) => {});
  }

  function ListYearBestStaff() {
    const token = localStorage.getItem("token");
    const fetchOption = {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    axios
      .get(
        "https://ptibem.cs.ui.ac.id/beststaff/api/v1/admin/best-staff/yearly/2021",
        fetchOption
      )
      .then((res) => {
        const finalData = res.data.data;
        setYearlyBestStaff(finalData);
      })
      .catch((err) => {});
  }

  useEffect(() => {
    fetchData();
    ListYearBestStaff();
  }, [success, failed]);

  return (
    <div>
      {AddYearly ? (
        <div
          style={{
            width: "100%",
            height: "100%",
            position: "fixed",
            background: "rgba(0,0,0,0.25)",
            top: "0",
            left: "0",
            zIndex: "10",
          }}
        >
          <PopupConfirm>
            <br />
            <h3 style={{ textAlign: "center" }}>Ganti Date</h3>
            <p style={{ textAlign: "center" }}>
              {`Apakah yakin ingin menambahkan ${stafToAdd}`}
            </p>
            <Button
              onClick={() => {
                setAddYearly(false);
              }}
            >
              Ya
            </Button>
            <Button
              onClick={() => {
                setAddYearly(false);
              }}
            >
              Tidak
            </Button>
          </PopupConfirm>
        </div>
      ) : null}

      {success ? (
        <div
          style={{
            width: "100%",
            height: "100%",
            position: "fixed",
            background: "rgba(0,0,0,0.25)",
            top: "0",
            left: "0",
            zIndex: "10",
          }}
        >
          <PopupConfirm>
            <img
              src="https://cdn.discordapp.com/attachments/755605623214964900/858746542177124362/Vector.png"
              style={{
                width: "100px",
                height: "100px",
                margin: "auto",
                marginTop: "36px",
              }}
            />
            <h3 style={{ textAlign: "center" }}>Success change data</h3>
            <Button onClick={() => setSuccess(!success)}>Close</Button>
          </PopupConfirm>
        </div>
      ) : null}

      {failed ? (
        <div
          style={{
            width: "100%",
            height: "100%",
            position: "fixed",
            background: "rgba(0,0,0,0.25)",
            top: "0",
            left: "0",
            zIndex: "10",
          }}
        >
          <PopupConfirm>
            <img
              src="https://cdn.discordapp.com/attachments/755605623214964900/859283436014141470/Vector1.png"
              style={{
                width: "100px",
                height: "100px",
                margin: "auto",
                marginTop: "36px",
              }}
            />
            <h3 style={{ textAlign: "center" }}>Failed change data</h3>
            <Button onClick={() => setFailed(!failed)}>Close</Button>
          </PopupConfirm>
        </div>
      ) : null}

      <div style={{ position: "relative", width: "100%", height: "100%" }}>
        <NavbarAdmin></NavbarAdmin>
      </div>

      <BestStaffAdd>
        <Title>ADD BEST STAFF YEARLY</Title>
        <StaffNameContainer>
          <Select
            ariaLiveMessages={{
              onFocus,
            }}
            options={NamaAnggota}
            placeholder="Nama Lengkap"
            styles={customstyle}
          />
        </StaffNameContainer>
        <MessageContainer
          placeholder="Kesan Pesan"
          onChange={(e) => {
            setKesanPesan(e.target.value);
          }}
        ></MessageContainer>
        <AddButton onClick={PostData}>Add</AddButton>
      </BestStaffAdd>

      <BestStaffContainer>
        {YearlyBestStaff.map((data, key) => {
          var styles = {};
          if (key % 2 == 1) {
            styles = {
              background: "#0F3B5E",
            };
          }

          return (
            <CardContainer
              onClick={() => {
                handleShow();
                setDataShow(data);
              }}
            >
              <BiroNameContainer style={styles}>
                <BirdepTitle>{data.birdept}</BirdepTitle>
              </BiroNameContainer>
              <Image src={imageValidate(data.img)}></Image>
              <PreviousNameContainer>
                <PreviousBestStaff>{data.name}</PreviousBestStaff>
              </PreviousNameContainer>
            </CardContainer>
          );
          // return(

          //     <Popup trigger={

          //     } modal>{close => (
          //        <StaffInfoPopup>
          //         <BestStaffInfo>
          //             <ImageLeft>
          //             <ImagePopup src={imageValidate(data.img)}/>
          //             </ImageLeft>
          //             <IdentityRight>
          //                 <br/><br/>
          //                 <h5>Nama</h5>
          //                 <p>{data.name}</p>
          //                 <h5>Jurusan</h5>
          //                 <p>{data.jurusan}</p>
          //                 <h5>Biro / Departemen</h5>
          //                 <p>{data.birdept}</p>
          //                 <h5>Kesan/Pesan</h5>
          //                 <p style={{wordWrap: 'break-word'}}>{data.kesan_pesan}</p>
          //             </IdentityRight>
          //             <DeleteButton onClick={()=>{
          //         DeleteYearBestStaff(data)
          //         close()
          //     }}>Delete</DeleteButton>
          //         </BestStaffInfo>

          //        </StaffInfoPopup>
          //     )}

          //     </Popup>

          // )
        })}
        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>{dataShow.birdept}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <ModalWrapper>
              <div className="left">
                <img src={imageValidate(dataShow.img)} />
              </div>

              <div className="right">
                <h5>Nama</h5>
                <p>{dataShow.name}</p>
                <h5>Jurusan</h5>
                <p>
                  {dataShow.jurusan} {dataShow.angkatan}
                </p>
                <h5>Biro / Departemen</h5>
                <p>{dataShow.birdept}</p>
                <h5>Kesan/Pesan</h5>
                <p style={{ wordWrap: "break-word" }}>{dataShow.kesan_pesan}</p>
              </div>
            </ModalWrapper>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="primary"
              onClick={() => {
                handleClose();
                DeleteYearBestStaff(dataShow);
              }}
            >
              Delete
            </Button>
          </Modal.Footer>
        </Modal>
      </BestStaffContainer>
      <AdminFooter></AdminFooter>
    </div>
  );
}

const imageValidate = (link) => {
  var res =
    "https://cdn.discordapp.com/attachments/755605623214964900/858178467349790731/Rectangle_59.png";
  var linkImage = link;
  if (linkImage) if (linkImage.includes("http")) res = linkImage;
  return res;
};
