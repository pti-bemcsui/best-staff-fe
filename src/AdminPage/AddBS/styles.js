import styled from "styled-components";

export const ModalWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  .left {
    display: flex;
    img {
      width: 250px;
      height: 250px;
      margin: 50px auto;
      border-radius: 5px;
    }
  }

  .right {
    padding: 0% 15%;
  }
`;

export const Button = styled.button`
  display: flex;
  z-index: 1;

  margin: auto;
  padding: 12px 48px;
  background: #f99622;
  border: 0;
  border-radius: 12px;
  outline: none;

  font-size: 20px;
  color: white;
  cursor: pointer;
`;
export const PopupConfirm = styled.section`
  display: flex;
  flex-direction: column;
  width: 500px;
  height: 300px;
  z-index: 1;

  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  margin: 0;
  background: white;
  border-radius: 24px;
  filter: drop-shadow(0px 2px 2px rgba(0, 0, 0, 0.25));
`;

export const Title = styled.p`
  position: absolute;
  left: 250px;
  top: 100px;
  border-radius: nullpx;
  font-family: Poppins;
  font-size: 48px;
  font-style: normal;
  font-weight: 1000;
  line-height: 60px;
  letter-spacing: 0em;
  text-align: left;
  color: #0f3b5e;
  @media (max-width: 1300px) {
    left: 10vw;
  }
  @media (max-width: 767px) {
    left: 20vw;
    right: 10vw;
  }
  @media (max-width: 650px) {
    left: 10vw;
    right: 10vw;
  }
  @media (max-width: 550px) {
    left: 10%;
    font-size: 36px;
    line-height: 60px;
    top: 10%;
  }
`;
export const StaffNameContainer = styled.div`
  height: 56px;
  width: 565px;
  top: 260px;
  border-radius: 15px;
  position: absolute;
  @media (max-width: 767px) {
    left: 20vw;
    width: 430px;
  }
  @media (max-width: 650px) {
    left: 10vw;
    width: 500px;
  }
  @media (max-width: 590px) {
    width: 460px;
  }
  @media (max-width: 550px) {
    font-size: 14px;
    width: 420px;
  }
  @media (max-width: 500px) {
    width: 380px;
  }
  @media (max-width: 450px) {
    width: 340px;
  }
  @media (max-width: 400px) {
    width: 83vw;
  }
`;

export const MessageContainer = styled.textarea`
  height: 195px;
  width: 565px;
  // left: 41%;
  top: 360px;
  border-radius: 15px;
  border-style: none;
  background: #f1f1f1;
  position: absolute;
  padding: 10px 20px;
  font-size: 18px;
  font-style: normal;
  font-weight: 400;
  line-height: 27px;
  letter-spacing: 0em;
  text-align: left;
  font-family: Poppins;
  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
  @media (max-width: 767px) {
    left: 20vw;
    width: 430px;
  }
  @media (max-width: 650px) {
    left: 10vw;
    width: 500px;
  }
  @media (max-width: 590px) {
    width: 460px;
  }
  @media (max-width: 550px) {
    font-size: 14px;
    width: 420px;
  }
  @media (max-width: 500px) {
    width: 380px;
  }
  @media (max-width: 450px) {
    width: 340px;
  }
  @media (max-width: 400px) {
    width: 83vw;
  }
`;

export const AddButton = styled.button`
  height: 55px;
  width: 160px;
  // left: 47%;
  top: 600px;
  border-radius: 15px;
  background: #f99622;
  border-radius: 15px;
  position: absolute;
  font-size: 20px;
  color: #ffffff;
  border: 0px;
  &:hover {
    background: #0f3b5e;
    transition: 0.3s;
  }
  @media (max-width: 550px) {
    font-size: 14px;
    height: 40px;
    width: 100px;
  }
`;

export const BestStaffAdd = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 700px;
  position: relative;
  // @media (max-width:487px){
  //     height: 1000px;
  // }
  @media (max-width: 384px) {
    height: 630px;
  }
`;

export const BestStaffContainer = styled.section`
  width: 100%;
  height: auto;
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 15%;
  margin-top: 10%;
  @media (max-width: 384px) {
    margin-top: 50%;
  }
`;

export const Image = styled.div`
  height: 330px;
  width: 230px;
  position: relative;
  background-size: 100% 100%;
  background-image: url(${(props) => props.src});
  z-index: 1;
  left: 70px;
  top: -29px;
  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
`;

export const ImagePopup = styled.div`
  height: 330px;
  width: 230px;
  position: relative;
  background-size: 100% 100%;
  background-image: url(${(props) => props.src});
  z-index: 1;
  top: 5%;
  border-radius: 20px;
  box-shadow: 0px 0px 60px rgba(0, 0, 0, 0.25);
  left: 10%;
  @media (max-width: 1200px) {
    height: 200px;
    width: 150px;
    left: 30%;
  }
  @media (max-width: 384px) {
    height: 200px;
    width: 150px;
    border-radius: 40px;
    left: 0%;
    top: 10%;
  }
`;

export const BiroNameContainer = styled.div`
  height: 51.125px;
  width: 216px;
  left: 0px;
  top: 0px;
  border-radius: 0px;
  background: #f99622;
  position: absolute;
  z-index: 2;
  position: relative;
  text-align: center;
`;

export const CardContainer = styled.div`
  width: 300px;
  height: 381px;
  top: 0px;
  margin-left: 8%;
  margin-top: 6%;
  left: 2%;
  position: relative;
  cursor: pointer;
  @media (max-width: 384px) {
    margin-bottom: 30%;
  }
  @media (max-width: 360px) {
    margin-left: 2%;
  }
`;
export const BirdepTitle = styled.p`
  margin: 0;
  color: #ffffff;
  font-size: 28px;
  font-weight: 500;
  z-index: 5;
  ${"" /* left : 35%; */}
  position : relative;
  @media (max-width: 384px) {
    font-size: 25px;
  }
`;

export const PreviousNameContainer = styled.div`
  height: auto;
  width: 230px;
  border-radius: 0px;
  background: #f8f8f8;
  position: relative;
  top: -29px;
  box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.25);
  text-align: center;
  left: 70px;
`;

export const PreviousBestStaff = styled.p`
  font-family: Poppins;
  font-size: 22px;
  font-style: normal;
  font-weight: 600;
  line-height: 33px;
  letter-spacing: 0em;
  text-align: center;
  color: #0f3b5e;
  margin: 0;
  z-index: 4;
  position: relative;
`;

export const ButtonPopup = styled.button``;

export const StaffInfoPopup = styled.div`
  width: 100%;
  height: auto;
  border-radius: 300px;
  @media (max-width: 384px) {
    ${"" /* width : 400px; */}
    left : 200px;
  }
`;
export const BestStaffInfo = styled.div`
  ${"" /* background : blue; */}
  width : 100%;
  height: 650px;
  @media (max-width: 1206px) {
    flex-direction: column;
  }
  @media (max-width: 384px) {
    height: 350px;
  }
  display: flex;
  flex-direction: row;
`;
export const ImageLeft = styled.div`
  position: relative;
  ${"" /* background : red; */}
  width : 45%;
  @media (max-width: 1206px) {
    width: 100%;
  }
  @media (max-width: 384px) {
    display: none;
  }
`;

export const IdentityRight = styled.div`
  width: 55%;
  height: 100%;
  position: relative;
  ${"" /* background : pink; */}
  @media(max-width:1206px) {
    width: 100%;
    padding: 0px 30px;
  }
`;

export const bigFont = styled.h1`
  font-size: 30px;
`;

export const DeleteButton = styled.button`
  height: 62px;
  width: 179px;
  left: 35%;
  position: relative;
  bottom: 2%;
  border-radius: 15px;
  background: #f4511e;
  border-radius: 15px;
  position: absolute;
  font-size: 24px;
  color: #ffffff;
  border: 0px;
  &:hover {
    background: blue;
  }
  @media (max-width: 384px) {
    display: none;
  }
`;
export const customstyle = {
  // placeholder: (defaultStyles) => {
  //     return {
  //         ...defaultStyles,
  //         color: '#ffffff',
  //     }
  // },
  // indicatorSeparator: () => {},
  //   dropdownIndicator: defaultStyles => ({
  //     ...defaultStyles,
  //     color: '#FFFFFF',
  // }),
  control: (css) => ({
    ...css,
    background: "#FFFFFF",
    //   color: '#FFFFFF',
    padding: "5px 5px",
    border: "1px solid #D9D9D9",
    boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.25)",
    height: "50px",
    marginTop: "20px",
    borderRadius: "15px",
    borderStyle: "none",
    boxShadow: "none",
    textAlignLast: "center",
  }),
};
