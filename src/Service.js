import { useEffect, useState } from "react";
import ReactLoading from "react-loading";
import styled from "styled-components";
import { UserContext } from "./UserContext";
import { Redirect, Link } from "react-router-dom";
import axios from "axios";
import { setCookie, getCookie } from "./helpers/cookies";
const LoadingContainer = styled.div`
  position: absolute;
  left: 47%;
  top: 45%;
`;
const ImageNextPage = styled.div`
  height: 350px;
  width: 300px;
  position: relative;
  top: 0px;
  left: 0px;
  background-size: 100% 100%;
  @media (max-width: 384px) {
    height: 200px;
    width: 250px;
  }
  background-image: url("https://bem.cs.ui.ac.id/wp-content/uploads/2019/03/logo-bem.png");
`;
const Next = styled.div`
  position: absolute;
  display: flex;
  height: auto;
  width: auto;
  left: 38%;
  top: 10%;
  flex-direction: column;
  @media (max-width: 384px) {
    left: 20%;
  }
  -webkit-animation: fadeIn linear 7s;
  -moz-animation: fadeIn linear 1s;
  -o-animation: fadeIn linear 1s;
  -ms-animation: fadeIn linear 1s;
  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @-moz-keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @-webkit-keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @-o-keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @-ms-keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
`;
const NextText = styled.div`
  text-decoration: none;
  margin-top: 40px;
  position: relative;
  left: 11%;
`;
const KeteranganNext = styled.p`
  color: #d6d6d6;
  left: 30%;
  position: relative;
`;

const singkatan = (fullName) => {
  // Algo :
  // Get first dan second name and get first character after
  // example :
  // Bisma Khomeini = Bisma K
  // Dzikir Qalam Haturogan  = Dzkiri QH
  var ans = "";
  var arr = fullName.split(" ");
  if (arr.length === 1) return fullName;
  ans += arr[0] + " " + arr[1] + " ";
  for (let i = 2; i < arr.length; i++) {
    ans += arr[i].charAt(0);
  }
  return ans;
};

export default function Service() {
  const [isAdmin, setIsAdmin] = useState(false);
  const [isClient, setIsClient] = useState(false);
  const [isFetch, setIsFetch] = useState(false);
  useEffect(() => {
    const param = window.location.pathname;
    console.log(window.location.pathname);
    const paramSplit = param.split("/");
    var tokenLocation = 0;
    console.log(tokenLocation);
    for (let i = 0; i < paramSplit.length; i++) {
      if (paramSplit[i] === "login-sivitas") tokenLocation = i + 1;
    }
    console.log(tokenLocation);
    const token = paramSplit[tokenLocation];
    const user = localStorage.getItem("user");
    localStorage.setItem("token", token);
    localStorage.setItem("isVisited", true);

    setCookie("bs-token", token, 100);
    setCookie("isVisited", true, 100);

    setTimeout(() => {
      window.location.assign("/best-staff/");
    }, 2000);
  }, []);

  useEffect(() => {
    const token = localStorage.getItem("token");
    const fetchOption = {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    axios
      .get(
        "https://ptibem.cs.ui.ac.id/beststaff/api/v1/auth/profile/user",
        fetchOption
      )
      .then((data) => {
        const hasil = data.data.data;
        console.log(hasil);
        localStorage.setItem("user", singkatan(hasil.full_name));
        localStorage.setItem("AdminAccess", hasil.is_staff);

        setCookie("user", singkatan(hasil.full_name), 100);
        setCookie("AdminAcess", hasil.is_staff);

        setIsFetch(true);
      })
      .catch((err) => {
        console.log("Something happened");
      });
  });

  const RoutingService = () => {
    return (
      <LoadingContainer>
        <ReactLoading
          type={"bars"}
          color={"#F8F8F2"}
          height={100}
          width={100}
        />
      </LoadingContainer>
    );
  };

  return <div className="Service">{RoutingService()}</div>;
}
