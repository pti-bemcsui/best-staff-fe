import styled from "styled-components";
import { Link, NavLink } from "react-router-dom";
import { Navbar, Nav, Container, NavDropdown } from "react-bootstrap";
import { Logout } from "../../Auth";
const Image = styled.div`
  width: 200px;
  height: 60px;
  background-size: 100% 100%;
  margin-left: 10%;
  border: 0px;
  ${"" /* margin-top : 5%; */}
  cursor : pointer;
  background-image: url("https://cdn.discordapp.com/attachments/755605623214964900/838234119015235584/PTI.png");
`;
const ProfileImage = styled.div`
  width: 100px;
  height: 100px;
  background-size: 100% 100%;
  margin: 18px;
  margin-left: 10%;
  border: 0px;
  border: 1px black solid;
  ${"" /* margin-top : 5%; */}
  background-image : url('https://images.pexels.com/photos/1007066/pexels-photo-1007066.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500')
`;

const NavbarAdmin = () => {
  return (
    <Navbar expand="lg md sm" variant="light" bg="light">
      <Container>
        <Navbar.Brand>
          <Image
            onClick={() => {
              window.location.replace("/best-staff");
            }}
          ></Image>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav ml-auto" />
        <Navbar.Collapse id="basic-navbar-nav ml-auto">
          <Nav className="justify-content-end" style={{ width: "100%" }}>
            <Nav.Link href="/best-staff/admin" style={{ color: "black" }}>
              Dashboard
            </Nav.Link>
            <Nav.Link
              href="/best-staff/admin/YearAdd"
              style={{ color: "black" }}
            >
              Add Best Staff
            </Nav.Link>
            <Nav.Link href="/best-staff/admin/biro" style={{ color: "black" }}>
              Biro/Departemen
            </Nav.Link>
            <Nav.Link
              href="/best-staff/admin/DataAnggota/Crud"
              style={{ color: "black" }}
            >
              Data Staff
            </Nav.Link>
            <NavDropdown title={`Admin`} id="navbarScrollingDropdown">
              <NavDropdown.Item href="/best-staff/">
                Client HomePage
              </NavDropdown.Item>
              <NavDropdown.Item href="#action3" onClick={Logout}>
                Logout
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NavbarAdmin;
