import styled from "styled-components";
const Bar = styled.div`
  width: 100%;
  height: 100px;
  background-color: #0f3b5e;
  position: relative;
`;

const TextOne = styled.div`
  position: absolute;
  color: white;
  top: 35px;
  left: 12%;
  @media (max-width: 472px) {
    top: 30px;
    left: 5vw;
    right: 5vw;
    text-align: center;
    font-size: 14px;
  }
`;

const TextTwo = styled.div`
  position: absolute;
  color: white;
  top: 35px;
  right: 12%;
  @media (max-width: 472px) {
    top: 60px;
    left: 5vw;
    right: 5vw;
    text-align: center;
    font-size: 14px;
  }
`;

const AdminFooter = (props) => {
  const top = props.top;
  return (
    <div>
      <Bar style={{ top: top }}>
        <TextOne>
          Made with <span style={{ color: "red" }}>❤</span> by PTI for PSDM
        </TextOne>
        <TextTwo>Copyright© 2021</TextTwo>
      </Bar>
    </div>
  );
};
export default AdminFooter;
