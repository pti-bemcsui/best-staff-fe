import styled from "styled-components";
import { motion } from "framer-motion";
import { HamburgerIcon, CloseIcon, ChevronDownIcon, ChevronUpIcon } from "@chakra-ui/icons";
import { Logout } from "../../Auth";
import logout from "../images/logout.svg";
import { useState, useEffect } from "react";
import bem from "../images/bemLogo.svg";
import Styled from "./NavStyle";

const Image = styled.div`
  padding: 12px 0px;
  cursor: pointer;
}
`;

const Right = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  justify-content: flex-end;
`;

const NavbarAdminLogin = (props) => {
  const [clicked, SetClicked] = useState(false)

  return (
    <Styled
      style={{
        display: "flex",
        flexDirection: "flex-end",
        background: "#F4F1E9",
        boxShadow: " 0px 5px 10px rgba(0, 0, 0, 0.2)",
      }}
    >
      
      <div className="userbutton mobs" onClick={() => SetClicked(!clicked)} >
          {!clicked ? <HamburgerIcon boxSize={6}/> : <ChevronUpIcon boxSize={6} />}
      </div>
      <Image>
        <img src={bem} onClick={() => {window.location.replace("/best-staff");}} ></img>
      </Image>
      <Right>
        <div className="link desks">
          <a
            href="/best-staff/voting"
            style={{
              color: "black",
              fontFamily: "Commissioner",
              fontWeight: "700",
              marginRight: "24px",
              display: "flex",
            }}
          >
            Voting
          </a>
          <div className="userbutton desks" onClick={() => SetClicked(!clicked)} >
            <div className="username">{`${props.user}`}</div>
            {!clicked ? <ChevronDownIcon boxSize={6} /> : <ChevronUpIcon boxSize={6} />}
          </div>
          {clicked ? 
            <div className="dropdown desks">
              <a href="/best-staff/dashboard">Rapor saya</a>
              <a onClick={Logout} style={{color: "#D30026"}}>Logout</a>
            </div> 
          : null}
        </div>
        {clicked ? 
            <div className="dropdown mobs">
              <a href="/best-staff/home">Home</a>
              <a href="/best-staff/voting">Voting</a>
              <a href="/best-staff/dashboard">Rapor saya</a>
            </div> 
          : null}
        <div className="userbutton mobs">
          <img
            src={logout}
            onClick={Logout}
          />
        </div>
        {/*<NavDropdown id="navbarScrollingDropdown">*/}
        {/*<NavDropdown.Item href="/best-staff/admin">
            Admin
        </NavDropdown.Item>*/}
          {/*<NavDropdown.Item href="#action3" onClick={Logout}>
            Logout
          </NavDropdown.Item>
        </NavDropdown>*/}
      </Right>
    </Styled>
  );
};

export default NavbarAdminLogin;
