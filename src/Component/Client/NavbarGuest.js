import styled from "styled-components";
import { Link } from "react-router-dom";
import { Navbar, Nav, Container, NavDropdown } from "react-bootstrap";
import { HamburgerIcon, CloseIcon } from "@chakra-ui/icons";
import { Login } from "../../Auth";
import { useState, useEffect } from "react";
import { Button } from "@chakra-ui/react";
import bem from "../images/bemLogo.svg";
import login from "../images/login.svg";
import loginNav from "../images/loginNav.svg";
import Styled from "./NavStyle.js";
import "./style.css";

const Image = styled.div`
  width: 290px;
  height: 59px;
  background-size: 100% 100%;
  border: 0px;
  background-image: url("${bem}");
`;

const ButtonLogin = styled(Button)`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 12px 20px;
  gap: 8px;
  width: 110px !important;
  height: 44px !important;
  background: #121263;
  border-radius: 8px;
  color: #ffffff;
  font-family: "Commissioner";
  font-weight: 700;
`;

const NavbarGuest = (props) => {
  return (
    <Navbar
      expand="lg md sm"
      className="mainNavbar"
      style={{
        background: "#F4F1E9",
        boxShadow: " 0px 5px 10px rgba(0, 0, 0, 0.2)",
      }}
    >
      <Container>
        <div className="empty"></div>
        <a href={`${process.env.PUBLIC_URL}`}>
          <Image />
        </a>
        <img src={loginNav} onClick={Login} className="imgLog"></img>
        <div className="login">
          <Nav className="justify-content-end" style={{ width: "100%" }}>
            <ButtonLogin
              variant="outline-warning"
              className="ml-4"
              onClick={Login}
            >
              Login<img src={login}></img>
            </ButtonLogin>{" "}
          </Nav>
        </div>
      </Container>
    </Navbar>
  );
};

export default NavbarGuest;
