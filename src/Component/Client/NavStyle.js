import styled from "styled-components";

const Styled = styled.div`
    -webkit-user-select: none; /* Safari */        
    -moz-user-select: none; /* Firefox */
    -ms-user-select: none; /* IE10+/Edge */
    user-select: none; /* Standard */

    width: 100%;
    height: 80px;
    background-color: #F4F1E9;
    box-shadow: 0px 4px 6px -1px rgba(0, 0, 0, 0.1),
    0px 2px 4px -1px rgba(0, 0, 0, 0.06);
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 48px 0 16px;
    z-index: 999;

    @media screen and (max-width: 768px) {
        height: 80px;
        padding: 0 7%;
    }

    .menu-icon {
        display: none;
    }
    
    .link {
        flex-direction: row;
        align-items: center;
      
        justify-content: flex-end;
    }

    .userbutton {
        cursor: pointer;
        gap: 4px;
        padding: 12px 0px;
    }

    .username {
        color: rgba(18, 18, 99, 1);
        font-weight: 700;
    }

    .dropdown {
        position: absolute;
        z-index: 999;
        right: 14px;
        top: 80px;
        width: 240px;
        background: #F4F1E9;
        border-radius: 0px 0px 8px 8px;
        display: flex;
        flex-direction: column;
        filter: drop-shadow(0px 5px 10px rgba(0, 0, 0, 0.2));

        a {
            cursor: pointer;
            text-align: center;
            padding: 12px 24px;
            color: #181823;
            font-weight: 700;
        }

        a:hover {
            background: #FCF8EE;
            border-radius: 0px 0px 8px 8px;
            color: black;
        }

        @media screen and (max-width: 768px) {
            right: 0px;
            width: 100%;
        }
    }

    .desks {
        display: flex;

        @media screen and (max-width: 768px) {
            display: none;
        }
    }

    .mobs {
        display: none;

        @media screen and (max-width: 768px) {
            display: flex;
        }
    }
`;

export default Styled;
