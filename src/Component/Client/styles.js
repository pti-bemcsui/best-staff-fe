import styled from "styled-components";

const Styles = styled.div`
  width: 100%;
  height: 80px;
  background-color: #121263;
  color: #fff;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 2%;
  margin-bottom: 0px;
  position: absolute;
  z-index: 1;
  bottom: -70px;
  border-radius: 12px 12px 0px 0px;
  @media screen and (max-width: 780px) {
    flex-direction: row;
    padding: 20px 7%;
    height: 100px;
  }

  @media screen and (max-width: 300px) {
    padding: 25px 7%;
  }

  p {
    font-family: "Inter";
    font-weight: 700;
    font-size: 14px;

    @media screen and (max-width: 780px) {
      font-size: 12px;
    }

    @media screen and (max-width: 300px) {
      font-size: 10px;
    }
  }

  .copyright {
    width: 260px;
    height: 20px;
    margin: 12px;

    @media screen and (max-width: 300px) {
      width: 100px;
    }
    @media screen and (max-width: 780px) and (min-width: 550px) {
      width: 170px;
    }
    @media screen and (max-width: 550px) {
      width: 125px;
    }
  }

  .ImageContainer {
    display: flex;
    flex-direction: row;
    width: 260px;
    justify-content: space-between;

    img {
      width: 20px;
      height: 20px;
      margin: 4%;

      @media screen and (max-width: 550px) {
        height: 16px;
        width: 16px;
      }
    }

    @media screen and (max-width: 1000px) {
      width: 260px;
      margin: 12px;
    }
    @media screen and (max-width: 300px) {
      width: 100px;
    }
    @media screen and (max-width: 780px) and (min-width: 550px) {
      width: 170px;
    }
    @media screen and (max-width: 550px) {
      width: 125px;
    }
  }
`;

export default Styles;
