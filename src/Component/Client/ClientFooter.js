import React from "react";
import Styles from "./styles";

const ClientFooter = () => {
  return (
    <Styles>
      <img
        className="copyright"
        src={process.env.PUBLIC_URL + "/static/assets/footer/copyright.svg"}
        alt="copyright"
      />
      {console.log(process.env.PUBLIC_URL)}
      <div className="ImageContainer">
        <img
          alt="line logo"
          src={process.env.PUBLIC_URL + "/static/assets/footer/linelogo.svg"}
          onClick={() => {
            window.location.href =
              "https://linevoom.line.me/user/_dTuaIG7gAO4PrGBOjaqKHrldjjG9ti-nImejj8g?utm_medium=windows&utm_source=desktop&utm_campaign=OA_Profile";
          }}
        />
        <img
          alt="ig logo"
          style={{ cursor: "pointer" }}
          src={
            process.env.PUBLIC_URL + "/static/assets/footer/instagramlogo.svg"
          }
          onClick={() => {
            window.location.href = "https://www.instagram.com/bemfasilkomui/";
          }}
        />
        <img
          alt="twt logo"
          style={{ cursor: "pointer" }}
          src={process.env.PUBLIC_URL + "/static/assets/footer/twitterlogo.svg"}
          onClick={() => {
            window.location.href = "https://twitter.com/BEMFasilkomUI";
          }}
        />
        <img
          alt="fb logo"
          src={
            process.env.PUBLIC_URL + "/static/assets/footer/facebooklogo.svg"
          }
          onClick={() => {
            window.location.href = "https://www.facebook.com/BEMFasilkomUI/";
          }}
        />
        <img
          alt="medium logo"
          src={process.env.PUBLIC_URL + "/static/assets/footer/mediumlogo.svg"}
          onClick={() => {
            window.location.href = "https://medium.com/@KastratBEMFasilkomUI";
          }}
        />
        <img
          alt="issue logo"
          src={process.env.PUBLIC_URL + "/static/assets/footer/issuelogo.svg"}
          onClick={() => {
            window.location.href = "https://issuu.com/bemfasilkomui";
          }}
        />
      </div>
    </Styles>
  );
};
export default ClientFooter;
