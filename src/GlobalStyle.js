import styled from "styled-components";

const Global = styled.div`
  background: #f4f1e9;
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
  font-family: "Commissioner", sans-serif;
`;

export default Global;