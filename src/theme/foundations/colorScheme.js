const colorScheme = {
    bem_blue: {
        50: "rgba(68, 68, 153, 0.4)",
        100: "rgba(68, 68, 153, 0.4)",
        400: "#121263",
        500: "#121263",
        600: "#121263",
        700: "radial-gradient(23.69% 50% at 50% 50%, #121263 0%, #444499 100%)",
    },
    bem_red: {
        400: "#A32833",
        500: "#A32833",
        600: "#A32833",
        700: "radial-gradient(50% 50% at 50% 50%, #620012 0%, #D30026 100%)",
    },
    bem_statesRed: {
        50: "#FF8DA2",
        100: "#FF8DA2",
        600: "#D30026",
    },
    bem_statesYellow: {
        50: "#FCE2A1",
        100: "#FCE2A1",
        400: "rgba(229, 165, 0, 1)",
        500: "rgba(229, 165, 0, 1)",
        600: "rgba(229, 165, 0, 1)",
        700: "radial-gradient(50% 50% at 50% 50%, #A67700 0%, #E5A500 100%)",
    }
}

export default colorScheme;