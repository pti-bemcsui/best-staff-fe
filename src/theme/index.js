import { extendTheme, Select } from '@chakra-ui/react'

// Foundational style overrides
import colorScheme from './foundations/colorScheme.js'

// Component style overrides
import Button from './components/button'
import tabs from './components/tabs'
import table from './components/table.js'
import input from './components/input'
import textArea from './components/textArea'
import select from './components/select'

const overrides = {
  colors: colorScheme,

  components: {
    Button: Button,
    Tabs: tabs,
    Table: table,
    Input: input,
    Textarea: textArea,
    Select: select
  },
}

export default extendTheme(overrides)