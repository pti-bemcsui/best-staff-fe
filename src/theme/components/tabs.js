const tabs = {
    variants: {
        'bem-line': {
            tab: {
                color: "rgba(18, 18, 99, 1)",
                fontWeight: "semibold",
                borderRadius: "8px",
                _focus: {
                    boxShadow: "none",
                },
                _selected: {
                    borderRadius: "0px",
                    borderBottom: "1px solid",
                    borderColor: "#121263"
                },
                _disabled: {
                    color: "rgba(144, 144, 144, 1)",
                }
            }
        },
        'bem-bar': (props) => ({
            tab: {
                minWidth: "max-content",
                padding: "9px 15px",
                border: props.useBorder ? "2px solid rgba(18, 18, 99, 1)" : "0px" ,
                color: "rgba(18, 18, 99, 1)",
                bg: "#F4F1E9",
                [props.orientation === 'vertical'  ? "mb" : null]: props.customMargin ? props.customMargin : "10px",
                [props.orientation === 'vertical'  ? null : "m"]: props.customMargin ? "0 px " + props.customMargin : "8px 10px",
                fontWeight: "semibold",
                borderRadius: "8px",
                filter: props.useShadow ? 'drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25))' : null,
                _focus: {
                    boxShadow: "none",
                },
                _hover: {
                    bg: props.isDisabled ? 'rgba(68, 68, 153, 0.4)' : null,
                },
                _selected: {
                    bg: "rgba(18, 18, 99, 1)",
                    color: "white",
                },
                _disabled: {
                    filter: props.useShadow ? 'drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25))' : null,
                    color: "rgba(144, 144, 144, 1)",
                    border: "2px solid rgba(144, 144, 144, 1)",
                }
            }
        })
    },
    // 6. We can overwrite defaultProps
    defaultProps: {
      size: 'lg',
      colorScheme: 'bem_blue',
    },
}

export default tabs;