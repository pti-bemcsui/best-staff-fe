const textArea = {
    variants: {
        "outline" : {
            border: "1px solid",
            borderColor: "#121263",
            _focus: {
                boxShadow: "none",
            },
        }
    }
}

export default textArea;