const table = {
    variants: {
        'bem-simple': (props) => ({
            bg: "blue",
            color: "white",
            border: "1px solid #181823",
            thead: {
                th: {
                    bg: props.useTopBg ? "#121263" : null,
                    color: props.useTopBg ? "white" : null,
                    fontSize: "16px",
                }
            },
            th: {
                border: "1px solid #181823",
            },
            td: {
                border: "1px solid #181823",
            }, 
            tfoot: {
                fontSize: "12px",
                bg: "rgba(229, 165, 0, 1)",
            },
        })
    },
    defaultProps: {
        colorScheme: "bem_blue",
    }
}

export default table;