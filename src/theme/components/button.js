const Button = {
    variants: {
      'solid': {
        _hover: {
          boxShadow: '0px 4px 8px rgba(18, 18, 99, 0.6)'
        },
        _disabled: {
          bg: "#E5E5E5",
          color: "#909090",
        },
        _focus: {
          boxShadow: "none",
      },
      },
      'outline': {
          _focus: {
            boxShadow: "none",
        },
      },
      'realisasi': {
        bg: "rgba(229, 165, 0, 1)",
        color: 'bem_blue.500',
        _active: {
          bg: "radial-gradient(50% 50% at 50% 50%, #A67700 0%, #E5A500 100%)"
        },
        _hover: {
          boxShadow: "0px 4px 12px rgba(166, 119, 0, 0.8)"
        },
        _focus: {
          boxShadow: "none",
        },
      },
      _focus: {
        boxShadow: "none",
    },
    },
    // 6. We can overwrite defaultProps
    defaultProps: {
      size: 'lg',
      colorScheme: 'bem_blue',
    },
}

export default Button;