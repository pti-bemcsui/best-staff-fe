const input = {
    variants: {
        "outline" : {
            field: {
                border: "1px solid",
                borderColor: "#121263",
                "> option, > optgroup": {
                    bg: "white",
                    borderRadius: "md",
                },
                _focus: {
                    boxShadow: "none",
                },
                _hover: {
                    borderColor: "#121263",
                },
                _disabled: {
                    border: "1px solid",
                    borderColor: "#B9B3B3",
                    color: "#B9B3B3",
                    opacity: "1",
                },
            },
        }
    }
}

export default input;