import { deleteAllCookies, setCookie } from "./helpers/cookies";

export const Login = () => {
  const LOGIN = "https://ptibem.cs.ui.ac.id/beststaff/api/v1/auth/login/sivitas";
  const ADMIN = "https://ptibem.cs.ui.ac.id/beststaff/admin/"
  var child = window.open(
    LOGIN,
    "Authentication",
    "toolbar=0,status=0,width=626,height=436"
  );
  var timer = setInterval(checkChild, 500);

  function checkChild() {
    if (child.closed) {
      window.location.assign(LOGIN);
      clearInterval(timer);
    }
  }

  setTimeout(() => {
    child.close();
    window.location.assign(LOGIN);
    clearInterval(timer);
  }, 500);


};

export const Logout = () => {
  localStorage.clear();
  deleteAllCookies();
  const toClear = ["bs-token", "AdminAccess", "user"];
  toClear.map((data) => {
    setCookie(data, null, 365);
  });
  var wdn = window.open(
    "https://ptibem.cs.ui.ac.id/beststaff/api/v1/auth/logout/",
    "_self",
    "resizable,scrollbars,status"
  );
};
