import Client from "./ClientPage";
import Admin from "./AdminPage";
import Service from "./Service";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { ChakraProvider } from '@chakra-ui/react'
import Theme from "./theme/index";
import Global from "./GlobalStyle";

const theme = Theme;

function App() {
  return (
    <div className="App">
      <ChakraProvider theme={theme}>
        <Global>
          <Router>
            <Switch>
              <Route exact path="/login-sivitas/:token">
                <Service></Service>
              </Route>
              <Route exact path="/:domain/login-sivitas/:token">
                <Service></Service>
              </Route>
              <Route path="/:domain/admin">
                <Admin></Admin>
              </Route>
              <Route path="/:domain/">
                <Client></Client>
              </Route>
            </Switch>
          </Router>
        </Global>
      </ChakraProvider>
    </div>
  );
}

export default App;
