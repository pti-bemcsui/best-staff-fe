import Homepage from "./Homepage";
import Dashboard from "./Dashboard";
import Vote from "./Vote";
import Voting from "./Voting";
import AllBestStaff from "./AllBestStaff";
import BestStaff from "./BestStaff";
import BestStaffYearly from "./BestStaffYearly";
import ClientFooter from "../Component/Client/ClientFooter";
import NavbarClientLogin from "../Component/Client/NavbarClientLogin";
import NavbarAdminLogin from "../Component/Client/NavbarAdminLogin";
import NavbarGuest from "../Component/Client/NavbarGuest";
import React from "react";
import { getCookie, setCookie } from "../helpers/cookies";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Logout } from "../Auth";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import VoteResult from "./VoteResult";
const MySwal = withReactContent(Swal);

const CheckAuth = () => {
  const user = localStorage.getItem("user") || getCookie("user");
  const Acess = localStorage.getItem("AdminAccess") || getCookie("AdminAccess");
  const token = localStorage.getItem("token") || getCookie("bs-token");
  const isVisit = localStorage.getItem("isVisited") || getCookie("isVisited");

  if (token) {
    if (!user) {
      MySwal.fire(
        "Auth Error",
        "Some problem in yout data, refreshing your page",
        "error"
      ).then(() => Logout());
    }
  }

  if (user) {
    if (!token) {
      MySwal.fire(
        "Auth Error",
        "Some problem in yout data, refreshing your page",
        "error"
      ).then(() => Logout());
    }
  }

  if (isVisit) {
    if (!token || !user) {
      MySwal.fire(
        "Auth Error",
        "Some problem in yout data, refreshing your page",
        "error"
      ).then(() => Logout());
    }
  }
};

const Client = () => {
  const user = localStorage.getItem("user");
  const Acess = localStorage.getItem("AdminAccess");
  const token = localStorage.getItem("token");

  CheckAuth();
  function whatNavbar() {
    if (token && user && Acess) {
      if (Acess === "true") {
        return <NavbarAdminLogin user={`${user}`} />;
      } else {
        return <NavbarClientLogin user={`${user}`} />;
      }
    } else {
      return <NavbarGuest />;
    }
  }

  return (
    <div>
      <Router>
        {whatNavbar()}
        <Switch>
          <Route path="/:domain/bulanan">
            <BestStaff type="bulanan"></BestStaff>
          </Route>
          <Route path="/:domain/tahunan">
            <BestStaffYearly type="tahunan"></BestStaffYearly>
          </Route>
          <Route path="/:domain/voteresult">
            <VoteResult></VoteResult>
          </Route>
          <Route path="/:domain/allbeststaff">
            <AllBestStaff></AllBestStaff>
          </Route>
          <Route path="/:domain/dashboard">
            <Dashboard></Dashboard>
          </Route>
          <Route path="/:domain/vote">
            <Vote></Vote>
          </Route>
          <Route path="/:domain/voting">
            <Voting></Voting>
          </Route>
          <Route path="/:domain">
            <Homepage></Homepage>
          </Route>
        </Switch>
        <ClientFooter />
      </Router>
    </div>
  );
};

export default Client;
