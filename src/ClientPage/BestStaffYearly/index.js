import styled from "styled-components";
import ClientFooter from "../../Component/Client/ClientFooter";
import Popup from "reactjs-popup";
import axios from "axios";
import { useState, useEffect } from "react";
import "reactjs-popup/dist/index.css";
import { NoProperty} from "../Homepage";
import { Title } from "../Voting";
import { Line2 } from "../BestStaff/index";
import WavyBalls from "../../utils/waveballs";

const Description = styled.div`
  margin: 120px 235px 30px 235px;
  height: 160px;

  @media screen and (max-width: 1400px) {
    margin: 120px 175px 30px 175px;
  }
  @media screen and (max-width: 1300px) {
    margin: 120px 125px 30px 125px;
  }
  @media screen and (max-width: 992px) {
    margin: 120px 175px 30px 175px;
  }
  @media screen and (max-width: 900px) {
    margin: 120px 125px 30px 125px;
  }
  @media screen and (max-width: 800px) {
    margin: 120px 75px 30px 75px;
  }
  @media screen and (max-width: 768px) {
    margin: 120px 150px 30px 150px;
  }
  @media screen and (max-width: 700px) {
    margin: 120px 100px 30px 100px;
    height: 140px;
  }
  @media screen and (max-width: 550px) {
    margin: 80px 50px 30px 50px;
    height: 100px;
  }
  @media screen and (max-width: 307px) {
    height: 180px;
  }
`;
// const Line = styled.div`
//   width: 120px;
//   border: 4px solid #f99622;
//   position: relative;
//   margin: 20px 0;
//   @media screen and (max-width: 763px) {
//     display: none;
//   }
// `;

// const Title = styled.p`
//   height: 30px;
//   border-radius: 0px;
//   font-family: Poppins;
//   font-size: 48px;
//   font-style: normal;
//   font-weight: 800;
//   letter-spacing: 0em;
//   text-align: left;
//   color: #0f3b5e;
//   position: relative;
//   top: -30px;
//   text-decoration: none;

//   @media screen and (max-width: 550px) {
//     font-size: 36px;
//     top: 0px;
//   }
//   @media screen and (max-width: 310px) {
//     margin-bottom: 40px;
//   }
// `;

const StaffList = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 0vh 12vw;
  margin: 5rem 5rem 10rem 5rem;
  gap: 8rem 5rem;
  justify-content: center;
  align-items: flex-start;

  @media screen and (max-width: 1300px) {
    padding: 0vh 6vw;
  }
  @media screen and (max-width: 500px) {
    gap: 5rem;
  }
`;

const ImageContainer = styled.div`
  display: flex;
  max-width: 275px;
  flex-direction: column;
  height: 350px;
  z-index: 3;
  position: relative;
  top: 0%;
  flex: 2;
  cursor: pointer;

  @media screen and (max-width: 384px) {
    height: 300px;
    left: 5%;
  }
`;

const HeadContainer = styled.div`
  background: #f99622;
  color: #ffffff;
  position: relative;
  text-align: center;
  text-transform: uppercase;
  font-size: 22px;
  font-weight: 500;
  flex: 0;
  left: -30px;
  top: -20px;
  width: 70%;
  height: 14%;
  border-radius: 10px;
  padding: 3px 0;

  @media screen and (max-width: 500px) {
    width: 120px;
    height: 33px;
    font-size: 18px;
    margin-left: -30px;
    top: -12px;
    left: -10px;
  }
`;

const HeadContainer2 = styled.div`
  margin-left: -50px;
  top: -19px;
  width: 200px;
  height: 50px;
  background: #0f3b5e;
  color: #ffffff;
  padding: 3px 0;
  position: relative;
  text-align: center;
  text-transform: uppercase;
  font-size: 25px;
  font-weight: 500;
  box-shadow: 10px 0px 20px rgba(0, 0, 0, 0.25);

  @media screen and (max-width: 500px) {
    width: 120px;
    height: 33px;
    font-size: 18px;
    margin-left: -30px;
    top: -12px;
  }
`;
const PersonImage = styled.div`
  height: 330px;
  width: 260px;
  left: 0%;
  top: 0%;
  background-size: 100% 100%;
  margin-top: -7%;
  position: relative;
  flex: 0px;
  background-image: url(${(props) => props.src});
  box-shadow: 10px 0px 20px rgba(0, 0, 0, 0.25);

  @media screen and (max-width: 500px) {
    width: 240px;
    height: 230px;
  }
  @media screen and (max-width: 384px) {
    width: 200px;
  }
`;
const TextContainer = styled.div`
  width: 260px;
  height: auto;
  background: #fafafa;
  padding: 5px 10px;
  margin-top: 0px;
  position: relative;
  text-align: left;
  box-shadow: 10px 0px 20px rgba(0, 0, 0, 0.25);

  @media screen and (max-width: 500px) {
    width: 240px;
  }
  @media screen and (max-width: 384px) {
    width: 200px;
  }
`;
const BirdepTitle = styled.p`
  margin: 5px 0;
  color: #0f3b5e;
  font-size: 20px;
  font-weight: 600;
  text-align: center;
  text-transform: uppercase;
  z-index: 5;
  position: relative;

  @media screen and (max-width: 500px) {
    font-size: 16px;
  }
  @media screen and (max-width: 384px) {
    font-size: 13px;
  }
`;

const Ppcbidang = styled.div`
  top: -35px;
  padding: 3px;
  width: 300px;
  height: 60px;
  margin-left: auto;
  margin-right: auto;
  display: block;
  background: #f99622;
  color: #ffffff;
  position: relative;
  text-align: center;
  font-size: 32px;
  font-weight: 600;
  border-radius: 15px;

  @media screen and (max-width: 900px) {
    top: -40px;
    padding: 5px;
    width: 250px;
    height: 50px;
    font-size: 24px;
    border-radius: 10px;
  }
  @media screen and (max-width: 500px) {
    top: -30px;
    width: 200px;
    height: 40px;
    font-size: 20px;
  }
`;

const Ppcard = styled.div`
  display: block;
  margin-left: auto;
  margin-right: auto;
  height: 400px;
  width: 100%;
  padding: 0px 25px;

  @media screen and (max-width: 1260px) {
    padding: 0px;
  }
`;

const Ppccolumn1 = styled.div`
  float: left;
  width: 45%;

  @media screen and (max-width: 1260px) {
    width: 45%;
  }
  @media screen and (max-width: 900px) {
    width: 100%;
  }
`;
const Ppccolumn2 = styled.div`
  float: left;
  width: 55%;
  margin-top: 0px;

  @media screen and (max-width: 1260px) {
    width: 55%;
  }
  @media screen and (max-width: 900px) {
    width: 100%;
    text-align: center;
  }
`;

const Ppcimage = styled.img`
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 15vw;
  height: 37vmin;

  @media screen and (max-width: 1040px) {
    width: 22vw;
    height: 37vmin;
  }
  @media screen and (max-width: 900px) {
    width: 27vw;
    height: 37vmin;
  }
  @media screen and (max-width: 500px) {
    width: 40vw;
    height: 45vmin;
    margin-bottom: 10px;
  }
`;

const Ppctitle = styled.p`
  font-weight: 700;
  font-size: 18px;
  margin-bottom: 0px;
  @media screen and (max-width: 900px) {
    font-size: 16px;
  }
  @media screen and (max-width: 500px) {
    font-size: 13px;
  }
`;
const Ppcword = styled.p`
  margin-bottom: 30px;
  font-size: 18px;
  @media screen and (max-width: 900px) {
    font-size: 15px;
  }
  @media screen and (max-width: 500px) {
    font-size: 13px;
  }
`;

const imageValidate = (link) => {
  var res =
    "https://cdn.discordapp.com/attachments/755605623214964900/858178467349790731/Rectangle_59.png";
  var linkImage = link;
  if (linkImage) if (linkImage.includes("http")) res = linkImage;
  return res;
};

const BestStaffYearly = (props) => {
  const [bestStaff, setBest_Staff] = useState(undefined);
  const [show, setShow] = useState(false);

  function fetchData() {
    const Year_Now = new Date().getFullYear();
    axios
      .get(`https://ptibem.cs.ui.ac.id/beststaff/api/v1/homepage/${Year_Now}`)
      .then((res) => {
        const result = res.data.data;
        setBest_Staff(result);
        setShow(true);
      });
      // .catch((err) => console.log(err));
  }

  useEffect(() => {
    fetchData();
  }, []);

  if (!show) {
    return (
      <div style={{ minHeight: "100vh" }}>
        <Description>
          <Title>BEST STAFF OF 2021</Title>
          <Line2/>
        </Description>
        <WavyBalls />
      </div>
    );
  }

  return (
    <div style={{ minHeight: "" }}>
      <Description>
        <Title>BEST STAFF OF 2021</Title>
        <Line2/>
      </Description>
      <StaffList>
        {bestStaff === undefined || bestStaff.length === 0 ? (
          <NoProperty style={{ marginTop: "0" }}>
            Best staff belum dipilih :(
          </NoProperty>
        ) : (
          bestStaff
            .sort((a, b) => (a > b ? 1 : -1))
            .map((data) =>
              data.id % 2 == 0 ? (
                <Popup
                  trigger={
                    <ImageContainer>
                      <PersonImage src={imageValidate(data.img)}>
                        <HeadContainer>{data.birdept}</HeadContainer>
                      </PersonImage>
                      <TextContainer>
                        <BirdepTitle>{data.nama}</BirdepTitle>
                      </TextContainer>
                    </ImageContainer>
                  }
                  modal
                >
                  <Ppcard>
                    <Ppcbidang>BIRO {data.birdept}</Ppcbidang>
                    <Ppccolumn1>
                      <Ppcimage src={imageValidate(data.img)}></Ppcimage>
                    </Ppccolumn1>
                    <Ppccolumn2>
                      <Ppctitle>Nama</Ppctitle>
                      <Ppcword>{data.nama}</Ppcword>
                      <Ppctitle>Jurusan / Angkatan</Ppctitle>
                      <Ppcword>{`${data.jurusan} / ${data.angkatan}`}</Ppcword>
                      <Ppctitle>Kesan / pesan</Ppctitle>
                      <Ppcword>{data.kesan_pesan}</Ppcword>
                    </Ppccolumn2>
                  </Ppcard>
                </Popup>
              ) : (
                <Popup
                  trigger={
                    <ImageContainer>
                      <PersonImage src={imageValidate(data.img)}>
                        <HeadContainer2>{data.birdept}</HeadContainer2>
                      </PersonImage>
                      <TextContainer>
                        <BirdepTitle>{data.nama}</BirdepTitle>
                      </TextContainer>
                    </ImageContainer>
                  }
                  modal
                >
                  <Ppcard>
                    <Ppcbidang>BIRO {data.birdept}</Ppcbidang>
                    <Ppccolumn1>
                      <Ppcimage src={imageValidate(data.img)}></Ppcimage>
                    </Ppccolumn1>
                    <Ppccolumn2>
                      <Ppctitle>Nama</Ppctitle>
                      <Ppcword>{data.nama}</Ppcword>
                      <Ppctitle>Jurusan / Angkatan</Ppctitle>
                      <Ppcword>{`${data.jurusan} / ${data.angkatan}`}</Ppcword>
                      <Ppctitle>Kesan / pesan</Ppctitle>
                      <Ppcword>{data.kesan_pesan}</Ppcword>
                    </Ppccolumn2>
                  </Ppcard>
                </Popup>
              )
            )
        )}
      </StaffList>
      <ClientFooter></ClientFooter>
    </div>
  );
};
export default BestStaffYearly;
