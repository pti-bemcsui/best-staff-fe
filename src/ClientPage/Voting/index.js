import { useState, useEffect } from "react";
import styled from "styled-components";
import { useHistory, useLocation } from "react-router-dom";
import Picture from "./VotingYuk.png";
import HeaderPict from "./PictForHeader.png"
import { VotingWrapper, } from "../Homepage";
import { getCookie } from "../../helpers/cookies";
import VotingModal from "./VotingModal";
import { useDisclosure } from '@chakra-ui/react'
import { findAllByTestId } from "@testing-library/react";

export const Title = styled.h1`
  margin: 1.5em 0;
  text-align: left;
  text-transform: uppercase;
  font-weight:bold;
  color:#0F3B5E;
  font-size: 3em;
  @media screen and (max-width: 768px) {
    font-size: 1.75em;
  }
`;

const Title2 = styled.h2`
  font-size: 1.5em;
  font-weight: bold;
  color: black;
  text-align:left;
  margin: 0 0 0.5em 0;

  @media screen and (max-width: 768px) {
    font-size: 1.25em;
  }

`;

export const Square = styled.div`
  padding: 3px;
  background: #f99622;
  margin-top: 4px;
  width: 144px;

  @media screen and (max-width: 768px) {
    width: 115px;
    padding: 2px;
  }
`;

const Image = styled.img`
  display: block;
  width: 40%;  
`;

const Image2 = styled.img`
  display: block;
  width: 100%;
  object-fit:cover;
  object-positon:center;  
  margin: 3em 0;
`;

const RContainer = styled.div`
  display: flex;
  text-align: left;
  flex-direction: column;
`;

const NumberedList = styled.li`
  display: list-item;
  list-style-position: inside;
  font-size: 18px;
  margin: 0.6em 0;
  font-weight: normal;
  color: black;

  @media screen and (max-width: 768px) {
    font-size: 16px;
  }
`;

const LetterList = styled.li`
  display: list-item;
  list-style-position: inside;
  font-size: 18px;
  margin: 0.6em 0;
  font-weight: normal;
  color: black;

  @media screen and (max-width: 768px) {
    font-size: 16px;
  }
`;

const BContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content:center;
  margin-bottom: 10vh;

  @media screen and (max-width: 768px) {
    flex-direction: column;
    align-items:center;
  }
`;

const Button = styled.button`
  margin: 2em;
  background: #121263;
  border-radius: 12px;
  outline: none;
  border: 0;
  font-size: 20px;
  color: white;
  cursor: pointer;
  height: 100%;
  padding: 0.75em 1.5em;

  @media screen and (max-width: 768px) {
    margin-bottom: 24px;
  }
`;

const OrderedList = styled.ol`
  list-style-type: decimal;  
`;

const OrderedList2 = styled.ol`
  margin-left: 2em;
  list-style-type: lower-alpha;
  @media screen and (max-width: 768px) {
    margin-top: 10px;
  }
`;

const Voting = (props) => {
  const [data, setData] = useState({ vote: [] });
  const [message, setMessage] = useState("Ada error yang terjadi")
  const { isOpen, onClose, onOpen } = useDisclosure({ defaultIsOpen: true })
  const [showModal, setShowModal] = useState(false)
  const [votingOpen, setVotingOpen] = useState(true)
  const [error, setError] = useState(true)

  const history = useHistory()
  const location = useLocation()

  const token = localStorage.getItem("token") || getCookie("bs-token");
  const request = {
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  const handleError = (response) => {
    if (response.status === 500) {
      setMessage("Server Error");
      setError(true)
      throw Error(response.status);
    } else if (response.status === 403) {
      setMessage("Kamu bukan anggota BEM")
      setError(true)
      throw Error(response.status);
    } else if (response.status === 400) {
      setMessage("Maaf, voting belum dibuka")
      setError(true)
      setVotingOpen(false)
      throw Error(response.status);
    } else if (response.status !== 200) throw Error(response.status);
    return response;
  };

  // All useEffect
  useEffect(() => {
    if(localStorage.getItem("token")){

      if (location.state === "ada") {
        setMessage("Terima kasih! Respon Anda sudah kami terima")
        setError(false)
        setShowModal(true)
      }

      fetch("https://ptibem.cs.ui.ac.id/beststaff/api/v1/vote/paruh", request)
      .then(handleError)
      .then((response) => response.json())
      .then((x) => {
        console.log(x)
        setData(x.data);
      })
      .catch((error) => {
        setError(true)
        setShowModal(true)
      });
    }else{
      setError(true)
      setMessage("Kamu harus login terlebih dahulu!")
      setShowModal(true)
        // <VotingModal type="error" title="Akses ditolak" text="Kamu harus login terlebih dahulu" />
    }
    
  }, []);

  const handleConfirmationError = (response) => {
    if (response.status === 400) {
      setMessage("Bulan atau tahun atau birdept tidak ditemukan");
      setError(true)
      showModal(true)
      throw Error(response.status);
    } else if (response.status === 403) {
      setMessage("Maaf, Anda tidak dapat mengisi voting lagi");
      setError(true)
      showModal(true)
      throw Error(response.status);
    } else if (response.status === 406) {
      showModal(true)
      setError(true)
      setMessage("Kamu bukan anggota dari biro/departemen ini");
      throw Error(response.status);
    } else if (response.status !== 200) throw Error(response.status);
    return response;
  };

  const handleCheckURL = (URL) => {
    if (localStorage.getItem("token")) {
      fetch(`https://ptibem.cs.ui.ac.id/beststaff${URL}/`, request)
        .then(handleConfirmationError)
        .then((response) => response.json())
        .then((x) => {
          history.push({ pathname: `/best-staff/vote${URL}`, state: URL })
        })
        .catch((error) => {
          onOpen()
          setError(true)
          setShowModal(true)
        })
    }
  }

  const toggleShowModal = () => {
    setShowModal(false)
  }

  return (
    <>
      {showModal === true && (
        <VotingModal
          type="error"
          errorStatus={error}
          toggleShowModal={toggleShowModal}
          title="Akses ditolak"
          text={message}
          isOpen={isOpen}
          onClose={onClose}
          state={location.state}
        />
      )}
      <VotingWrapper>
        <div className="max-w-7xl">
          <div className="flex flex-col">
            <div className="flex items-center justify-between">
              <Title>Voting Best staff</Title>
              <Image src={HeaderPict} />
            </div>
            <RContainer>
              <Title2>Ketentuan Best Staff</Title2>
              <OrderedList>
                <NumberedList>
                  Best Staff adalah staff terpilih dengan perolehan nilai voting
                  tertinggi di setiap biro/departemen
                </NumberedList>
                <NumberedList>
                  Setiap staff hanya memiliki kesempatan sebanyak satu kali
                  untuk menjadi best staff dalam satu periode
                </NumberedList>
                <NumberedList>
                  Jika staff mendapatkan perolehan nilai yang sama, maka akan
                  didiskusikan dengan BPH biro/departemen terkait dan diurutkan
                  berdasarkan kriteria:
                  <OrderedList2>
                    <LetterList>Kinerja</LetterList>
                    <LetterList>Komunikatif dan responsif</LetterList>
                    <LetterList>Tanggung jawab</LetterList>
                    <LetterList>Time management</LetterList>
                    <LetterList>Kehadiran rapat</LetterList>
                  </OrderedList2>
                </NumberedList>
                <NumberedList>
                  Setiap staff hanya memiliki satu kesempatan untuk melakukan
                  voting setiap term-nya
                </NumberedList>
              </OrderedList>
            </RContainer>
          </div>

          <Image2 src={Picture} />
          <BContainer>
            {votingOpen === true ? (
              data.vote.map((x, key) => (
                <>
                  <Button
                    className="font-bold text-center"
                    onClick={() => handleCheckURL(x.vote_url)}
                  >
                    {" "}
                    Vote {x.id}{" "}
                  </Button>
                </>
              ))
            ) : (
              <>
                {" "}
                <Button
                  className="font-bold"
                  onClick={() => {
                    setShowModal(true);
                    onOpen();
                  }}
                >
                  Vote Now!{" "}
                </Button>{" "}
              </>
            )}
          </BContainer>
        </div>
      </VotingWrapper>
    </>
  );
};

export default Voting;
