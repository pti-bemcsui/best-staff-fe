import React from 'react'
import { Modal, ModalOverlay, ModalContent, ModalBody, Button } from '@chakra-ui/react'
import { CloseIcon, CheckIcon } from '@chakra-ui/icons'
import { useHistory } from "react-router-dom";

const VotingModal = (props) => {
    const history = useHistory()

    return (
        <>
            <Modal isOpen={props.isOpen} onClose={props.onClose}>
                <ModalOverlay />
                <ModalContent bg="#F4F1E9" className="absolute top-1/4 flex flex-col items-center bg-[#F4F1E9] mx-4">
                    <ModalBody className="text-center">
                        {props.errorStatus === true ? 
                            <CloseIcon color="#D30026" className="font-bold text-7xl mt-12 mb-6" />
                            :
                            <CheckIcon color="#25946C" className="font-bold text-7xl mt-12" />
                        }
                        <p className="font-bold text-2xl my-6"> {props.text} </p>
                        <Button width="100%" bg='#121263' className="mb-6"
                            onClick={() => {
                                props.toggleShowModal();
                                if (props.state === "ada") {
                                    props.onClose()
                                    history.push({ pathname: "/best-staff/voting" })
                                } else {
                                    props.onClose()
                                }
                            }
                            }>
                            Tutup
                        </Button>
                    </ModalBody>
                </ModalContent>
            </Modal>
        </>
    )
}

export default VotingModal