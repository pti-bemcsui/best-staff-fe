import React from 'react'
import styled from 'styled-components'
import {
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    TableContainer,
} from "@chakra-ui/react"
import { getCookie } from '../../helpers/cookies';

const Result = styled.div`
    padding-bottom: 200px;
`;

const WrapForm = styled.div`
    display: flex;
    flex-direction: column;
    margin: 2rem;
    justify-content: space-between;
    align-items: center;

`;

const StyledTitle = styled.h1`
    font-family: 'Commissioner';
    font-style: normal;
    font-weight: 700;
    font-size: 48px;
    line-height: 120%;
    color: #121263;
    margin-right: 1rem;
    @media screen and (max-width: 640px) {
        font-size: 22px;
  }
`;

const BoxVoting = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    height: 138.92px;
    background: #121263;
    border-radius: 10px;
    color: white;
    padding: 16px 57px;
    font-family: 'Commissioner';
    font-style: normal;
    font-size: 24px;
    h1 {
        font-weight: 700;
    }
    span {
        font-weight: 400;
    }
    margin-top: 54px;
    margin-bottom: 54px; //Waiting for changes
`;

function VoteResult() {

    const [voteResult, setVoteResult] = React.useState([]);
    const [birdept, setBirdept] = React.useState("");
    const [name, setName] = React.useState("");
    const [totalVote, setTotalVote] = React.useState("0/0");

    React.useEffect(() => {
        const token = localStorage.getItem("token") || getCookie("bs-token");
        const request = {
            method: "GET",
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };
        const handleURL = () => {
            if (birdept === "" & name === "") {
                return `https://ptibem.cs.ui.ac.id/beststaff/api/v1/vote/paruh/result`
            } else if (birdept === "" & name !== "") {
                return `https://ptibem.cs.ui.ac.id/beststaff/api/v1/vote/paruh/result?name=${name}`
            } else if (birdept !== "" & name === "") {
                return `https://ptibem.cs.ui.ac.id/beststaff/api/v1/vote/paruh/result?birdept=${birdept}`
            } else {
                return `https://ptibem.cs.ui.ac.id/beststaff/api/v1/vote/paruh/result?birdept=${birdept}&?name=${name}`
            }
        };
        fetch(handleURL(), request)
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if (data.status === 403) {
                    alert("Anda tidak memiliki akses");
                    window.location.href = "/beststaff";
                }
                else {
                    setVoteResult(data.data.user_data);
                    setTotalVote(data.data.total_done_vote);
                }
            }).catch(err => {
                console.log(err);
            }
            );
    }, [birdept, name]);

    return (
        <Result>
            <WrapForm>
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                    <StyledTitle>{birdept !== "" ? `Hasil Voting Birdept ${birdept}` : "Hasil Voting Keseluruhan"}</StyledTitle>
                    <img
                        src={process.env.PUBLIC_URL + "/voteresult/voteresult_title.svg"}
                        alt="dekorasi title"
                    />
                </div>
                <BoxVoting>
                    <img
                        src={process.env.PUBLIC_URL + "/voteresult/voteresult_box.svg"}
                        alt="dekorasi title"
                    />
                    <h1>Jumlah Voting</h1>
                    <span>{totalVote}</span>
                </BoxVoting>
                <div style={{ marginBottom: '1rem' }}>
                    <span>Birdept: &nbsp;</span>
                    <select
                        value={birdept}
                        onChange={e => setBirdept(e.target.value)}
                    >
                        <option value={""}>---</option>
                        <option value={"PTI"}>PTI</option>
                        <option value={"PSDM"}>PSDM</option>
                        <option value={"Senbud"}>Senbud</option>
                        <option value={"SKI"}>SKI</option>
                        <option value={"Pengmas"}>Pengmas</option>
                        <option value={"PI"}>PI</option>
                        <option value={"Media"}>Media</option>
                        <option value={"Keilmuan"}>Keilmuan</option>
                        <option value={"Kastrat"}>Kastrat</option>
                        <option value={"KB"}>Koordinator Bidang</option>
                        <option value={"Humas"}>Humas</option>
                        <option value={"Depor"}>Depor</option>
                        <option value={"Bismit"}>Bismit</option>
                        <option value={"Akpem"}>Akpem</option>
                        <option value={"Adkesma"}>Adkesma</option>
                    </select>
                </div>
                <div>
                    <span>Nama: &nbsp;</span>
                    <input
                        type="text"
                        name="name"
                        onChange={e => setName(e.target.value)}
                        value={name}
                    />
                </div>
            </WrapForm>
            <TableContainer margin={"3%"} >
                <Table >
                    <Thead background={"#121263"} color={"white"}>
                        <Tr >
                            <Th color={"white"} textAlign="center">Rank</Th>
                            <Th color={"white"} textAlign="center">Nama Lengkap</Th>
                            <Th isNumeric color={"white"} textAlign="center">Q1</Th>
                            <Th isNumeric color={"white"} textAlign="center">Q2</Th>
                            <Th isNumeric color={"white"} textAlign="center">Q3</Th>
                            <Th isNumeric color={"white"} textAlign="center">Q4</Th>
                            <Th isNumeric color={"white"} textAlign="center">Q5</Th>
                            <Th isNumeric color={"white"} textAlign="center">Rata-Rata</Th>
                            <Th color={"white"} textAlign="center" >Status Vote</Th>
                        </Tr>
                    </Thead>
                    <Tbody background={"#D6D6D6"}>
                        {voteResult !== [] && voteResult.map((item) => (
                            <Tr key={item.rank}>
                                <Td textAlign="center">{item.rank}</Td>
                                <Td textAlign="center">{item.vote_data.nama}</Td>
                                <Td textAlign="center" isNumeric>{item.vote_data.nilai_per_soal.q1}</Td>
                                <Td textAlign="center" isNumeric>{item.vote_data.nilai_per_soal.q2}</Td>
                                <Td textAlign="center" isNumeric>{item.vote_data.nilai_per_soal.q3}</Td>
                                <Td textAlign="center" isNumeric>{item.vote_data.nilai_per_soal.q4}</Td>
                                <Td textAlign="center" isNumeric>{item.vote_data.nilai_per_soal.q5}</Td>
                                <Td textAlign="center" isNumeric>{item.vote_data['nilai_rata-rata']}</Td>
                                <Td textAlign="center">{item.vote_data.vote_status ? "Selesai" : "Belum Vote"}</Td>
                            </Tr>
                        ))}
                    </Tbody>
                </Table>
            </TableContainer>

        </Result>
    )
}

export default VoteResult