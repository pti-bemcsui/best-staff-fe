import { useState, useEffect } from "react";
import styled, { css } from "styled-components";
import { getCookie } from "../../helpers/cookies";
import {
  useDisclosure,
  AlertDialog,
  AlertDialogOverlay,
  AlertDialogCloseButton,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogFooter,
  Button,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
  Tbody,
} from "@chakra-ui/react";
import axios from "axios";
import React from "react";
import { ExternalLinkIcon } from '@chakra-ui/icons'

const Dafnil = styled.div`
  min-height: 600px;
`;

const PatternTitleContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 58%;
  margin: auto;

  @media screen and (max-width: 568px) {
    flex-direction: column;
    width: 90%;
    margin: 20px, auto;
  }

  @media screen and (min-width: 569px) and (max-width: 752px) {
    width: 80%;
  }
`;

const PatternContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-left: 8px;

  @media screen and (max-width: 568px) {
    display: none;
  }
`;

const TContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10vh;
  margin-bottom: 10vh;

  @media screen and (max-width: 568px) {
    margin-top: 6vh;
    margin-bottom: 7vh;
  }
`;

const DashboardTitle = styled.h1`
  font-size: 44px;
  color: #121263;
  font-weight: 700;

  @media screen and (max-width: 568px) {
    font-size: 36px;
  }
`;

const SubTitle = styled.h1`
  font-size: 22px;
  font-weight: 400;
  padding-top: 8px;

  @media screen and (max-width: 568px) {
    font-size: 20px;
  }
`;

const RaporContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 36px;
  margin: auto;
  gap: 12px;

  width: 58%;
  height: auto;

  background: #e5e5e5;
  border-radius: 12px;

  @media screen and (max-width: 568px) {
    width: 90%;
  }

  @media screen and (min-width: 569px) and (max-width: 752px) {
    width: 80%;
  }
`;

const DirectButtonContainer = styled.div`
  justify-content: center;
  flex-direction: row;
  gap: 40px;
  width: 100%;
  display: flex;
  margin-top: 24px;

  @media screen and (max-width: 568px) {
    flex-direction: column;
    align-items: center;
    gap: 16px;

    margin-top: 18px;
  }
`;

const FeatureHeader = styled.h3`
  color: #121263;
  font-family: Commissioner;
  font-size: 32px;
  font-weight: 700;
  line-height: 38px;
  letter-spacing: 0em;
`;

const FeatureDesc = styled.p`
  text-align: center;
  font-family: "Commissioner";

  @media screen and (max-width: 568px) {
    font-style: normal;
    font-weight: 400;
    font-size: 14px;

    color: #181823;
  }
`;

const arrowStyle = {
  width: "13%",
  paddingLeft: "4px",
};

const PenilaianContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 36px;
  margin: 80px auto;
  gap: 12px;

  width: 58%;
  height: auto;

  background: #e5e5e5;
  border-radius: 12px;

  @media screen and (max-width: 568px) {
    width: 90%;
  }

  @media screen and (min-width: 569px) and (max-width: 752px) {
    width: 80%;
  }
`;

const TableContainer = styled.div`
  width: 100%;
  align-items: left;
  margin-top: 20px;

  @media screen and (max-width: 568px) {
    margin-top: 28px;
  }
`;

const ContaienrShadow = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding: 8px;
  margin: 8px;
  gap: 10px;

  background: #e5e5e5;
  box-shadow: inset 0px 2px 6px rgba(0, 0, 0, 0.25);
  border-radius: 8px;

  flex: none;
  order: 1;
  flex-grow: 0;

  @media screen and (max-width: 568px) {
    padding: 0px;
  }
`;

const Scroll = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;

  &::-webkit-scrollbar {
    height: 8px;
  }

  &::-webkit-scrollbar-track {
    background: #e6e6e6;
    width: 4px;
  }

  &::-webkit-scrollbar-thumb {
    background: #bbb;
    border-radius: 8px;
    width: 4px;
  }

  &::-webkit-scrollbar-thumb:hover {
    background: #6d7589;
    width: 4px;
  }

  @media screen and (max-width: 886px) {
    display: block;
    overflow-x: auto;
    white-space: nowrap;
  }

  ${(finish) =>
    !finish &&
    css`
      padding-bottom: 500px;
    `}
`;

const Table = styled.table`
  width: 100%;
  background: #121263;
  margin-bottom: 4px;
  border-collapse: collapse;

  @media screen and (max-width: 720px) {
    overflow-x: auto;
    white-space: nowrap;
  }
`;

const TRow = styled.tr`
  background: #f4f1e9;
  padding: 8px;
`;

const THeader = styled.th`
  border-right: 4px solid #e5e5e5;
  padding: 8px;
  color: white;
  font-weight: 600;
  background: #121263;
`;

const TData = styled.td`
  border-right: 4px solid #e5e5e5;
  padding: 16px 8px 12px 16px;
  text-align: left;
  font-weight: 400;

  @media screen and (max-width: 450px) {
    font-size: 14px;
  }
`;

const TNilai = styled.td`
  border-right: 4px solid #e5e5e5;
  padding: 16px 8px 12px 0px;
  text-align: center;
  font-weight: 400;

  @media screen and (max-width: 450px) {
    font-size: 14px;
  }
`;

const TNomor = styled.td`
  border-right: 4px solid #e5e5e5;
  padding: 16px 8px 12px 0px;
  text-align: center;
  font-weight: 400;

  @media screen and (max-width: 450px) {
    font-size: 14px;
  }
`;

const RerataData = styled.td`
  border-right: 4px solid #e5e5e5;
  padding: 16px 8px 12px 16px;
  text-align: justify;
  font-weight: 700;

  @media screen and (max-width: 450px) {
    font-size: 14px;
  }
`;

const RerataNilai = styled.td`
  border-right: 4px solid #e5e5e5;
  padding: 16px 8px 12px 0px;
  text-align: center;
  font-weight: 700;

  @media screen and (max-width: 450px) {
    font-size: 14px;
  }
`;

var finish = false;

const Penilaian = (props) => {
  const [userBiro, setBiro] = useState("Biro/Departemen");
  const [disableRapor1, setDisableRapor1] = useState(false)
  const [disableRapor2, setDisableRapor2] = useState(false) 

  const [penilaianParuh1, setPenilaianParuh1] = useState({
    data_pertanyaan: [
      {
        no: 0,
        pertanyaan: "string",
        skor: 0,
      },
    ],
    "rata-rata": 0,
  });
  const [penilaianParuh2, setPenilaianParuh2] = useState({
    data_pertanyaan: [
      {
        no: 0,
        pertanyaan: "string",
        skor: 0,
      },
    ],
    "rata-rata": 0,
  });
  const [rerataUser, setRerataUser] = useState("0");
  const [raporParuh1, setRaporParuh1] = useState("null");
  const [raporParuh2, setRaporParuh2] = useState("null");

  const handleError = (response) => {
    if (response.status !== 200) throw Error(response.status);
    return response;
  };
  const token = localStorage.getItem("token") || getCookie("bs-token");

  const FetchUserData = () => {
    axios
      .get("https://ptibem.cs.ui.ac.id/beststaff/api/v1/dashboard/", {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(handleError)
      .then((data) => {
        setBiro(data.data.data.user_details.birdept);
        setPenilaianParuh1(data.data.data.penilaian.paruh_1);
        setPenilaianParuh2(data.data.data.penilaian.paruh_2);
        setRerataUser(data.data.data.penilaian["rata-rata"]);
        setRaporParuh1(data.data.data.rapor.raporParuh1);
        if (data.data.data.rapor.raporParuh1) {
          setDisableRapor1(false)
        } else {
          setDisableRapor1(true)
        }
        setRaporParuh2(data.data.data.rapor.raporParuh2);
        if (data.data.data.rapor.raporParuh2) {
          setDisableRapor2(false)
        } else {
          setDisableRapor2(true)
        }
      })
      .catch((error) => console.log(error));
  };
  useEffect(() => {
    FetchUserData();
  }, []);

  const namaPanggilan = getCookie("user")?.split(" ")[0];

  const showDataParuh1 = penilaianParuh1.data_pertanyaan.map(
    (penilaianUser, index) => (
      <TRow key={index}>
        <TNomor>{penilaianUser.no}</TNomor>
        <TData>{penilaianUser.pertanyaan}</TData>
        <TNilai>{penilaianUser.skor}</TNilai>
      </TRow>
    )
  );

  const showDataParuh2 = penilaianParuh2.data_pertanyaan.map(
    (penilaianUser, index) => (
      <TRow key={index}>
        <TNomor>{penilaianUser.no}</TNomor>
        <TData>{penilaianUser.pertanyaan}</TData>
        <TNilai>{penilaianUser.skor}</TNilai>
      </TRow>
    )
  );

  const showRerata = (paruh) => {
    const rerata =
      paruh === "1"
        ? penilaianParuh1["rata-rata"]
        : penilaianParuh2["rata-rata"];
    return (
      <TRow>
        <TData>{}</TData>
        <RerataData style={{ fontWeigt: "700" }}>Rata-rata</RerataData>
        <RerataNilai>{rerata}</RerataNilai>
      </TRow>
    );
  };

  const { isOpen, onOpen, onClose } = useDisclosure();
  const cancelRef = React.useRef();

  console.log(disableRapor1)
  const showButton1 = (raporLink) => {
    if (raporLink === null || raporLink === "null") {
      return (
        <Button
          className="flex items-center"
          onClick={onOpen}
          style={{ fontSize: "16px", padding: "16px" }}
          bg="#121263"
          rightIcon={<ExternalLinkIcon />}
          variant='solid'
          disabled={disableRapor1}
        >
          Lihat Rapor Paruh 1{" "}
        </Button>
      );
    } else {
      return (
        <Button
          onClick={() => {
            window.location.href = window.location.href = `${raporParuh1}`;
          }}
          style={{ fontSize: "16px", padding: "16px" }}
          bg="#121263"
          rightIcon={<ExternalLinkIcon />}
          variant='solid'
          disabled={disableRapor1}
        >
          Lihat Rapor Paruh 1{" "}
        </Button>
      );
    }
  };

  const showButton2 = (raporLink) => {
    if (raporLink === null || raporLink === "null") {
      return (
        <Button
          onClick={onOpen}
          style={{ fontSize: "16px", padding: "16px" }}
          bg="#121263"
          rightIcon={<ExternalLinkIcon />}
          variant='solid'
          disabled={disableRapor2}
        >
          Lihat Rapor Paruh 2{" "}
        </Button>
      );
    } else {
      return (
        <Button
          onClick={() => {
            window.location.href = `${raporParuh2}`;
          }}
          style={{ fontSize: "16px", padding: "16px" }}
          bg="#121263"
          rightIcon={<ExternalLinkIcon />}
          variant='solid'
          disabled={disableRapor2}
        >
          Lihat Rapor Paruh 2{" "}
        </Button>
      );
    }
  };

  return (
    <Dafnil>
      <PatternTitleContainer>
        <TContainer>
          <DashboardTitle>
            Halo,{" "}
            {namaPanggilan?.length > 15
              ? namaPanggilan?.slice(0, 15)
              : namaPanggilan}
          </DashboardTitle>
          <SubTitle>Staff Biro {userBiro}</SubTitle>
        </TContainer>
        <PatternContainer>
          <img
            src={process.env.PUBLIC_URL + "/dashboard/DashboardPattern.svg"}
            alt="dekorasi"
          />
        </PatternContainer>
      </PatternTitleContainer>

      <RaporContainer>
        <img
          src={process.env.PUBLIC_URL + "/dashboard/RaporIcon.svg"}
          alt="rapor icon"
        />
        <FeatureHeader>Rapor Saya</FeatureHeader>
        <FeatureDesc>
          Rapor BEM adalah salah satu proker dari PSDM sebagai bentuk penilaian
          dan evaluasi anggota BEM yang dilakukan pada dua term. Penilaian Rapor
          BEM berasal dari setiap anggota Biro/Departemen guna meningkatkan
          performa individu. Rapor BEM mencakup evaluasi dan apresiasi yang
          diberikan setelah melewati paruh satu dan paruh dua masa kepengurusan
          dalam bentuk kualitatif dan kuantitatif.
        </FeatureDesc>
        <DirectButtonContainer>
          {showButton1(raporParuh1)}
          {showButton2(raporParuh2)}
        </DirectButtonContainer>
      </RaporContainer>

      <PenilaianContainer>
        <img
          src={process.env.PUBLIC_URL + "/dashboard/BookIcon.svg"}
          alt="rapor icon"
        />
        <FeatureHeader>Penilaian Saya</FeatureHeader>
        <FeatureDesc>
          Tabel di bawah ini merupakan hasil penilaian yang didapatkan dari
          setiap anggota Biro/Departemen kamu. Penilaian tersebut diharapkan
          dapat membantu untuk merefleksikan diri dan meningkatkan kinerja kamu
          selama kepengurusan BEM kedepannya. Semangat! Belum terlambat untuk
          berproses dan berkembang
        </FeatureDesc>
        <TableContainer>
          <Tabs variant="bem-bar" useBorder={true}>
            <TabList>
              <Tab
                style={{
                  fontSize: "12px",
                  fontWeight: "700",
                  padding: "8px 12px",
                  marginRight: "4px",
                }}
              >
                Paruh 1
              </Tab>
              <Tab
                style={{
                  fontSize: "12px",
                  fontWeight: "700",
                  padding: "8px 12px",
                }}
              >
                Paruh 2
              </Tab>
            </TabList>

            <ContaienrShadow>
              <TabPanels>
                <TabPanel>
                  <Scroll>
                    <Table finish={finish}>
                      <Tbody>
                        <TRow>
                          <THeader
                            style={{ width: "10%", textAlign: "center" }}
                          >
                            No
                          </THeader>
                          <THeader
                            style={{ width: "80%", textAlign: "center" }}
                          >
                            Pertanyaan
                          </THeader>
                          <THeader
                            style={{ width: "16%", textAlign: "center" }}
                          >
                            Skor
                          </THeader>
                        </TRow>
                        {showDataParuh1}
                        {showRerata("1")}
                      </Tbody>
                    </Table>
                  </Scroll>
                </TabPanel>
                <TabPanel>
                  <Scroll>
                    <Table finish={finish}>
                      <Tbody>
                        <TRow>
                          <THeader
                            style={{ width: "10%", textAlign: "center" }}
                          >
                            No
                          </THeader>
                          <THeader
                            style={{ width: "80%", textAlign: "center" }}
                          >
                            Pertanyaan
                          </THeader>
                          <THeader
                            style={{ width: "16%", textAlign: "center" }}
                          >
                            Skor
                          </THeader>
                        </TRow>
                        {showDataParuh2}
                        {showRerata("2")}
                      </Tbody>
                    </Table>
                  </Scroll>
                </TabPanel>
              </TabPanels>
            </ContaienrShadow>
          </Tabs>
        </TableContainer>
      </PenilaianContainer>

      <AlertDialog
        motionPreset="slideInBottom"
        leastDestructiveRef={cancelRef}
        onClose={onClose}
        isOpen={isOpen}
        isCentered
      >
        <AlertDialogOverlay />

        <AlertDialogContent>
          <AlertDialogHeader>Oops!</AlertDialogHeader>
          <AlertDialogCloseButton />
          <AlertDialogBody>Rapor kamu belum ada.</AlertDialogBody>
          <AlertDialogFooter>
            <Button ref={cancelRef} onClick={onClose}>
              Close
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog>
    </Dafnil>
  );
};

export default Penilaian;
