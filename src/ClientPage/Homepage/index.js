import React, { useState, useEffect, useRef } from "react";
import "./style.css";
import styled from "styled-components";
import "react-slideshow-image/dist/styles.css";
import { motion } from "framer-motion";
import axios from "axios";
import { Link, Route, useHistory } from "react-router-dom";
import { Button, useDisclosure } from "@chakra-ui/react";
import ReactCardCarousel from "react-card-carousel";
import left from "./images/homeLeft.svg";
import right from "./images/homeRight.svg";
import leaderboard from "./images/leaderboard.svg";
import leaderboard2 from "./images/leaderboard2.svg";
import leftArrow from "./images/leftArrow.svg";
import rightArrow from "./images/rightArrow.svg";
import arrow from "./images/arrow.svg";
import people from "./images/people.svg";
import { Container, Button as MyButton } from "react-floating-action-button";
import Carousel, { consts } from "react-elastic-carousel";
import Modal from "./modal";
import Slide from "./Slide";
import NewModal from "./NewModal";

export const Wrapper = styled.div`
  position: relative;
  padding-bottom: 48px;
  display: flex;
  min-height: 450px;
`;

export const VotingWrapper = styled.div`
    display:flex;
    flex-direction: column;
    align-items:center;
    justify-content:center;
    width:100%;
    padding: 0 2em;
`;

const Description = styled.section`
  display: flex;
  column-gap: 30px;
  margin: 0px 240px;

  font-family: "Commissioner";
  color: #0f3b5e;

  @media screen and (min-width: 1500px) {
    justify-content: center;
    align-items: center;
  }

  @media screen and (max-width: 900px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin: 0px 160px;
  }
  @media screen and (max-width: 768px) {
    margin: 0px 25px;
  }
`;
export const Line = styled.hr`
  background-color: orange;
  border-color: orange;
  height: 5px;
  max-width: 180px;
  width: 10vw;
  display: inline-block;
  margin-top: 0;

  @media screen and (max-width: 1280px) {
    height: 5px;
  }

  @media screen and (max-width: 1000px) {
    width: 100px;
  }

  @media screen and (max-width: 600px) {
    width: 85px;
  }

  @media screen and (max-width: 370px) {
    width: 75px;
  }
`;

const Word = styled.p`
  font-size: 18px;
  font-style: normal;
  color: #092338;
  margin-top: 0;
  margin-bottom: 32px;
  // font-size: 24px;
  font-weight: 400;
  line-height: 29px;

  @media screen and (max-width: 750px) {
    font-size: 16px;
    margin-bottom: 20px;
    margin-left: 0;
    margin-right: 0;
  }
`;

export const ImageContainer = styled.div`
  height: 385px;
  width: 245px;
  border-radius: 8px;
  justify: space-between;
  background: #f4f1e9;
  position: relative;
  display: flex;
  top: 0%;
  box-sizing: border-box;
  transition: all 0.5s ease-in-out;

  @media screen and (max-width: 550px) {
    width: 200px;
    height: 400px;
  }
`;

export const TextName = styled.p`
  margin: 0;
  color: black;
  font-size: 20px;
  font-weight: 700;
  font-family: "Commissioner";

  @media screen and (max-width: 550px) {
    font-size: 16px;
  }
  @media screen and (max-width: 390px) {
    font-size: 12px;
  }
`;
export const PersonImage = styled.div`
  height: 240px;
  background-size: 100% 100%;
  background-image: url(${(props) => props.src});
  background-position: center;
  background-repeat: no-repeat;
  border-radius: 8px 8px 0 0;

  @media screen and (max-width: 550px) {
    height: 200px;
  }
`;
export const TextContainer = styled.div`
  height: auto;
  text-align: left;
  padding: 16px;
`;
export const BirdepTitle = styled.p`
  color: #444499;
  font-family: Commissioner;
  font-size: 22px;
  font-weight: 700;
  word-wrap: break-word;

  @media screen and (max-width: 550px) {
    font-size: 16px;
  }
  @media screen and (max-width: 390px) {
    font-size: 13px;
    padding-top: 10px;
  }
`;

export const AngkatanTitle = styled(BirdepTitle)`
  // padding-bottom: 20px;
  color: black;
  font-family: Commissioner;
  font-size: 16px;
  font-weight: 400;
  word-wrap: break-word;

  @media screen and(max-width: 390px) {
    padding-bottom: 10px;
  }
`;

export const ButtonKunjungi = styled.div`
  display:flex;  
  button{
    display: flex;
    box-sizing: border-box;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    padding: 12px 20px;
    gap: 8px;
    width: 200px;
    height: 27px;
    border: 2px solid #444499;
    border-radius: 8px;
    font-family: "Commissioner";
    font-style: normal;
    font-weight: 700;
    font-size: 9.81818px;
    line-height: 120%;
    align-items: center;
    text-align: center;
  }
`;

export const SubTitle = styled.h2`
  font-family: "Commissioner";
  font-weight: 700;
  color: #fcf8ee;
  text-align: center;
  font-size: calc(24px + (44 - 24) * ((100vw - 360px) / (1366 - 360)));
  margin-bottom: 60px;

  @media screen and (max-width: 750px) {
    margin-bottom: 20px;
  }
`;

const ImageSection = styled.div`
  position: relative;
  bottom: 0px;

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

const HomeSection = styled.div`
  align-self: flex-end;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-bottom: 64px;

  @media screen and (max-width: 920px) {
    justify-content: flex-start;
    margin-bottom: 50px;
  }
`;

const Title = styled.h1`
  font-size: calc(36px + (62 - 36) * ((100vw - 360px) / (1366 - 360)));
  font-weight: 700;
  font-family: "Fraunces";
  text-align: center;
  color: #000000;
  margin-top: 80px;
  margin-bottom: 4px;

  @media screen and (max-width: 900px) {
    text-align: left;
  }
`;

const ButtonLeaderboard = styled.div`
  display: none;
  @media screen and (max-width: 900px) {
    cursor: pointer;
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 8px;

    width: 210px;
    height: 44px;

    font-family: "Commissioner";
    font-style: normal;
    font-weight: 700;
    font-size: 16px;
    line-height: 120%;

    display: flex;
    align-items: center;
    text-align: center;

    color: #121263;
  }
`;

const ButtonLihat = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 12px 20px;
  gap: 8px;
  margin-right: auto;
  margin-left: auto;
  margin-top: 30px;

  width: 231px;
  height: 44px;

  background: #ffffff;
  border-radius: 8px;
  font-family: "Commissioner";
  font-style: normal;
  font-weight: 700;
  font-size: 16px;
  line-height: 120%;

  display: flex;
  align-items: center;
  text-align: center;

  color: #121263;

  @media screen and (max-width: 1000px) {
    margin-top: 100px;
  }
`;

const MonthlyArea = styled.section`
  background: #a32833;
  width: 100%;
  margin: 0px 0 0 0 !important;
  // background-repeat: no-repeat;
  background-size: cover;
  padding-top: 48px;
  min-height: 800px;

  @media screen and (max-width: 1500px) {
    background-position: center;
  }

  @media screen and (min-width: 1555px) {
    padding-bottom: 15%;
    padding-top: 15%;
  }

  @media screen and (max-width: 900px) {
    min-height: 700px;
    padding-top: 40px;
    padding-bottom: 50px;
  }

`;

const CarouselBest = styled.section`
  padding: 0px 0px 0 0px;
  position: relative;
  z-index: 1;

  @media screen and (max-width: 1000px) {
    display: none;
    padding: 0px 0px;
  }

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

const imageValidate = (link, linkAlternate = null) => {
  var res =
    "https://cdn.discordapp.com/attachments/755605623214964900/858178467349790731/Rectangle_59.png";
  var linkImage = link;
  //console.log(link)
  if (linkImage){ 
    if (linkImage.includes("http")) {
      res = linkImage;
    }
  } else if (linkAlternate && linkAlternate.includes("http")) {
    res = linkAlternate;
  }
  return res;
};

export const NoProperty = styled.p`
  text-align: center;
  margin-top: 150px;
  margin-bottom: 100px;
  font-weight: 500;
  font-size: calc(20px + (30 - 20) * ((100vw - 360px) / (1366 - 360)));
  color: #0b2942;

  @media screen and (max-width: 768px) {
    margin-top: 100px;
  }

  @media screen and (max-width: 1000px) {
    margin-left: auto;
    margin-right: auto;
  }
`;

export const BodyContainer = styled.div`
  box-shadow: 10px 0px 20px rgba(0, 0, 0, 0.25);
  width: 245px;

  @media screen and (max-width: 550px) {
    width: 200px;
    box-shadow: 3px 0px 10px rgba(0, 0, 0, 0.25);
  }

  @media screen and (max-width: 390px) {
    width: 200px;
  }
`;

const TitleFrom = {
  x: -100,
  opacity: 0,
};
const TitleTo = {
  x: 0,
  opacity: 1,
};

const ContainerStyle = styled.div`
  display: none;

  @media screen and (max-width: 1000px) {
    display: flex;
    position: relative;
    height: 50vh;
    width: 100%;
    display: flex;
    margin-top: 90px;
  }
`;

const Homepage = (props) => {
  localStorage.setItem("client", true);
  const [bestStaff, setBestStaff] = useState();
  const [kunjungiOpen, setKunjungiOpen] = useState(false)
  const [items, setItems] = useState();
  const [modalOpen, setModalOpen] = useState(false);
  const [style, setStyle] = useState({ display: 'none' });
  const [leaderboardState, setLeaderboardState] = useState();
  const Caro = useRef(null);

  function fetchData() {
    axios
      .get("https://ptibem.cs.ui.ac.id/beststaff/api/v1/homepage/paruh/1/2022")
      .then((res) => {
        //console.log(res.data.data);
        //console.log(res.data.data.best_staff_paruh);
        setBestStaff(res.data.data.best_staff_paruh);
        setItems(res.data.data.best_staff_paruh);
        setLeaderboardState(res.data.data.leaderboard);
        // const result = res.data.data;
        // setBest_Staff(result);
        // const arr = Array.from(Array(result.length).keys());
        // setItems(arr);
        // console.log(items);
        // console.log(arr);
        // console.log("masuk sini");
        // console.log(result);
      })
      .catch((err) => console.log(err));
  }

  function myArrow({ type, onClick }) {
    const pointer =
      type === consts.PREV ? (
        <img
          src={leftArrow}
          style={{ position: "relative", zIndex: "2" }}
        ></img>
      ) : (
        <img
          src={rightArrow}
          style={{ position: "relative", zIndex: "2" }}
        ></img>
      );
    return (
      <Button
        onClick={onClick}
        background="none"
        _hover={"none"}
        marginBottom={"auto"}
        marginTop={"auto"}
        padding={"40px"}
        style={{ position: "relative", zIndex: "2" }}
      >
        {pointer}
      </Button>
    );
  }

  function webUtama(Birdep) {
    // 👇️ redirect to external URL
    if (Birdep === "PTI") {
      window.open("https://bem.cs.ui.ac.id/pti-2021/");
    } else if (Birdep === "PSDM") {
      window.open("https://bem.cs.ui.ac.id/psdm-2021/");
    } else if (Birdep === "Bismit") {
      window.open("https://bem.cs.ui.ac.id/bismit-2021/");
    } else if (Birdep === "Humas") {
      window.open("https://bem.cs.ui.ac.id/humas-2021/");
    } else if (Birdep === "Media") {
      window.open("https://bem.cs.ui.ac.id/media-2021/");
    } else if (Birdep === "Kastrat") {
      window.open("https://bem.cs.ui.ac.id/kastrat-2021/");
    } else if (Birdep === "PENGMAS") {
      window.open("https://bem.cs.ui.ac.id/pengmas-2021/");
    } else if (Birdep === "Adkesma") {
      window.open("https://bem.cs.ui.ac.id/adkesma-2021/");
    } else if (Birdep === "Akpem") {
      window.open("https://bem.cs.ui.ac.id/akpem-2021/");
    } else if (Birdep === "Keilmuan") {
      window.open("https://bem.cs.ui.ac.id/keilmuan-2021/");
    } else if (Birdep === "Depor") {
      window.open("https://bem.cs.ui.ac.id/depor-2021/");
    } else if (Birdep === "Senbud") {
      window.open("https://bem.cs.ui.ac.id/senbud-2021/");
    } else {
      return null;
    }
    return null;
  }

  useEffect(() => {
    fetchData();
  }, []);

  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <div>
      {modalOpen && <Modal setOpenModal={setModalOpen} leaderboardData={leaderboardState} />}
      <NewModal isOpen={isOpen} onClose={onClose} leaderboardData={leaderboardState} />
      <div style={{ position: "relative", zIndex: "99" }}>
        <Container>
          <MyButton
            tooltip="Leaderboard!"
            rotate={true}
            onClick={() => {
              // setModalOpen(true);
              onOpen();
            }}
            className="myButton"
          >
            <img src={leaderboard}></img>
          </MyButton>
        </Container>
      </div>
      <Wrapper>
        <img className="bgImage" src={right} align="left" style={{ position: "absolute", bottom: "0" }}></img>
        <Description>
          <HomeSection>
            <motion.div
              initial={TitleFrom}
              animate={TitleTo}
              transition={{ duration: 1 }}
            >
              <Title>Best Staff</Title>
              <Word>
                Best Staff diberikan kepada anggota BEM Fasilkom UI 2022 sebagai
                simbol penghargaan terhadap kontribusi yang telah diberikan dan
                sebagai evaluasi untuk memotivasi serta menjadi parameter
                perbaikan kinerja dari setiap anggota untuk kedepannya.
              </Word>
            </motion.div>
            <ButtonLeaderboard
              onClick={() => {
                // setModalOpen(true);
                onOpen();
              }}
            >
              <Button leftIcon={<img src={leaderboard2}></img>} fontWeight="700" variant="outline" border="2px" borderColor="rgba(68, 68, 153, 1)">
                Lihat Leaderboard
              </Button>
            </ButtonLeaderboard>

          </HomeSection>
        </Description>
        <img className="bgImage" src={left} align="right" style={{ position: "absolute", bottom: "0", right: "0" }}></img>
      </Wrapper>

      <MonthlyArea>
        <SubTitle>Best Staff of the Term</SubTitle>
        <CarouselBest>
          {bestStaff === undefined || items === undefined || bestStaff.length === 0 ? (
            <NoProperty>Best staff belum dipilih</NoProperty>
          ) : (
            <>
              <div className="carousel-wrapper">
                <Carousel
                  itemsToShow={3}
                  itemPadding={[0, 10]}
                  renderArrow={myArrow}
                  style={{
                    position: "relative",
                    zIndex: "0",
                  }}
                >
                  {bestStaff.map((item) => (
                    <>
                      <Slide imageURL={() => imageValidate(item.best_staff.foto, item.best_staff.linkAlternate)} jurusan={item.best_staff.jurusan} angkatan={item.best_staff.angkatan} birdept={item.best_staff.kode_birdept} nama={item.best_staff.nama} webUtama={(birdept) => webUtama(birdept)} />
                    </>
                  ))}
                </Carousel>
              </div>
            </>
          )}
        </CarouselBest>

        <ContainerStyle>
          {bestStaff === undefined || items === undefined || bestStaff.length === 0 ? (
            <NoProperty>Best staff belum dipilih :( </NoProperty>
          ) : (
            <ReactCardCarousel
              spread={"wide"}
              autoplay={true}
              autoplay_speed={5000}
              className="card-carousel-mobile"
              ref={Caro}
            >
              {bestStaff.map((item) => (
                <>
                  <div className="each-slide">
                    <ImageContainer>
                      <BodyContainer>
                        <PersonImage
                          src={imageValidate(item.best_staff.foto, item.best_staff.linkAlternate)}
                        ></PersonImage>
                        <TextContainer>
                          <BirdepTitle>
                            {item.best_staff.kode_birdept === "PTI" ||
                              "PSDM" ||
                              "Bismit" ||
                              "Humas" ||
                              "Media"
                              ? "Biro " + item.best_staff.kode_birdept
                              : "Departemen " + item.best_staff.kode_birdept}
                          </BirdepTitle>
                          <TextName>
                            {item.best_staff.nama
                              .split(" ")
                              .slice(0, 1)
                              .join(" ")}
                            &nbsp;
                            {item.best_staff.nama
                              .split(" ")
                              .slice(-2, -1)
                              .join(" ")}
                          </TextName>
                          <AngkatanTitle>
                            {item.best_staff.jurusan} {item.best_staff.angkatan}
                          </AngkatanTitle>

                          {Caro.current ? (
                            <ButtonKunjungi
                              className="cursor-pointer mt duration-1000 mt-1"
                            >
                              <button onClick={() => webUtama(item.best_staff.kode_birdept)}>
                                <img src={people}></img> Kunjungi Halaman Birdep
                                <img src={arrow}></img>
                              </button>
                            </ButtonKunjungi>
                          ) : null}

                        </TextContainer>
                      </BodyContainer>
                    </ImageContainer>
                  </div>
                </>
              ))}
            </ReactCardCarousel>
          )}
        </ContainerStyle>
        <Link to="/best-staff/allbeststaff" style={{ textDecoration: "none" }}>
          <ButtonLihat>
            Lihat semua best staff <img src={arrow}></img>
          </ButtonLihat>
        </Link>
      </MonthlyArea>
    </div>
  );
};

export default Homepage;
