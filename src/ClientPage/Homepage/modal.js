import React, { useEffect } from "react";
import "./modal.css";
import styled from 'styled-components'

const StyledSelect = styled.select`
  background-color: #F4F1E9;
  border: 1px solid black;
  border-radius: 8px;
  &:hover {
    cursor: pointer;
  }
  padding: 6px;
  font-family: 'Commissioner';
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  margin-top: 0.5rem;
`;

function Modal({ setOpenModal, leaderboardData }) {
  const [selectedLeaderboad, setSelectedLeaderboard] = React.useState();
  console.log(leaderboardData.length);

  useEffect(() => {
    if (leaderboardData.length > 0) {
      setSelectedLeaderboard(leaderboardData[0]);
    }
  }, []);

  return (
    <div className="modalBackground">
      <div className="modalContainer">
        <div className="titleCloseBtn">
          <button
            onClick={() => {
              setOpenModal(false);
            }}
          >
            X
          </button>
        </div>
        <div className="title">
          <h1>Leaderboard</h1>
        </div>
        {leaderboardData.length > 0 ?
          <>
            <StyledSelect
              value={selectedLeaderboad}
              onChange={e => setSelectedLeaderboard(e.target.value)}
            >
              {leaderboardData.map(leaderboard => <option value={leaderboard.linkAlternate} key={leaderboard.linkAlternate}>{leaderboard.bulan}, {leaderboard.tahun}</option>)}
            </StyledSelect>
            <br />
            <div className="body">
              <img src={selectedLeaderboad} />
            </div>
          </> : <span>Belum ada Leaderboard</span>
        }
      </div>
    </div>
  );
}

export default Modal;
