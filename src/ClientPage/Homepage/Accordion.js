import { useState, useEffect } from "react";
import Text from "./text";
import Heading from "./heading";
import getAccordion from "./GetAccordion";
import { Link } from "react-router-dom";

const Accordion = getAccordion(1);

const NormalAccordion = (props) => {
  const [bulanan, setBulanan] = useState(undefined);
  const [tahunan, setTahunan] = useState(undefined);
  useEffect(() => {
    setBulanan(props.bulanan);
    setTahunan(props.tahunan);
  });
  return (
    <div className="app">
      <Accordion>
        <Heading
          render={(icon) => <div className={`icon ${icon && "open"}`}></div>}
        >
          <div className="heading-box">
            <h1 className="heading">2021</h1>
          </div>
        </Heading>
        <Text>
          <Accordion>
            <Heading>
              <div className="heading-box2">
                <Link
                  style={{ textDecoration: "none", color: "black" }}
                  to="/best-staff/tahunan"
                >
                  <h1 className="heading2">Annual List</h1>
                </Link>
              </div>
            </Heading>
            <Text onClick={() => {}}>
              {/* {tahunan ? tahunan.map(data=>(<p className="text">{data.nama}</p>)) : <p className="text"> Belum Ada :(</p>} */}
            </Text>
          </Accordion>
          <Accordion>
            <Heading>
              <div className="heading-box2">
                <Link
                  style={{ textDecoration: "none", color: "black" }}
                  to="/best-staff/bulanan"
                >
                  <h1 className="heading2">Monthly List</h1>
                </Link>
              </div>
            </Heading>
            <Text>
              {/* {bulanan ? bulanan.map(data=>(<p className="text">{data.best_staff.nama}</p>)): <p className="text">Belum Ada Data :(</p>} */}
            </Text>
          </Accordion>
        </Text>
        <br />
        <Heading
          render={(icon) => <div className={`icon ${icon && "open"}`}></div>}
        >
          <div className="heading-box">
            <h1 className="heading">2020</h1>
          </div>
        </Heading>
        <Text>
          <Accordion>
            <Heading>
              <div
                className="heading-box2"
                onClick={() => {
                  var child = window.open(
                    "https://bem.cs.ui.ac.id/best-staff/",
                    "_blank",
                    "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes"
                  );
                }}
              >
                <h1 className="heading2">Annual List</h1>
              </div>
            </Heading>
            {/* <Text>
            <p className="text">Belum tersedia :(</p>
        </Text> */}
          </Accordion>
          <Accordion>
            <Heading>
              <div className="heading-box2">
                <h1 className="heading2">Monthly List</h1>
              </div>
            </Heading>
            <Text>
              <p className="text">Belum tersedia :(</p>
            </Text>
          </Accordion>
        </Text>
      </Accordion>
    </div>
  );
};

export default NormalAccordion;
