import React from "react";
import "./modal.css";
import styled from "styled-components";

const Button = styled.div`
  // :hover {
  display: flex;
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 12px 20px;
  gap: 8px;

  // width: 170.91px;
  width: 200px;
  height: 27px;
  border: 2px solid #444499;
  border-radius: 8px;
  font-family: "Commissioner";
  font-style: normal;
  font-weight: 700;
  font-size: 9.81818px;
  line-height: 120%;

  display: flex;
  align-items: center;
  text-align: center;
  // }
`;

function ButtonKunjungi({ setOpenKunjungo }) {
  return (
    <a href="https://google.com" target="_blank" rel="noreferrer">
      <Button>
        {/* <img src={people}></img>  */}
        Kunjungi Halaman Birdep
        {/* <img src={arrow}></img> */}
      </Button>
    </a>
  );
}

export default ButtonKunjungi;
