import React, { useState, useEffect, Component } from "react";
import "./style.css";
import "react-slideshow-image/dist/styles.css";
import arrow from "./images/arrow.svg";
import people from "./images/people.svg";
import { ImageContainer, BodyContainer, PersonImage, TextContainer, BirdepTitle, TextName, AngkatanTitle, ButtonKunjungi } from "./index"

const Slide = (props) => {
    const [style, setStyle] = useState({ display: 'none' });
    return (
        <div>
            <div className="each-slide"
                onMouseEnter={e => {
                    setStyle({ display: 'flex' });
                }}
                onMouseLeave={e => {
                    setStyle({ display: 'none' })
                }}
            >
                <ImageContainer>
                    <BodyContainer>
                        <PersonImage
                            src={props.imageURL
                            }
                        ></PersonImage>
                        <TextContainer>
                            <BirdepTitle>
                                {props.birdept ===
                                    "PTI" ||
                                    "PSDM" ||
                                    "Bismit" ||
                                    "Humas" ||
                                    "Media"
                                    ? "Biro " +
                                    props.birdept
                                    : "Departemen " +
                                    props.birdept}
                            </BirdepTitle>
                            <TextName>
                                {props.nama.split(" ")
                                    .slice(0, 1)
                                    .join(" ")}
                                &nbsp;
                                {props.nama.split(" ")
                                    .slice(-2, -1)
                                    .join(" ")}
                            </TextName>
                            <AngkatanTitle>
                                {props.jurusan}{" "}
                                {props.angkatan}
                            </AngkatanTitle>
                            <ButtonKunjungi
                                className="cursor-pointer mt duration-1000 mt-1"
                            >
                                <button style={style} onClick={() => props.webUtama(props.birdept)}>
                                    <img src={people}></img> Kunjungi Halaman Birdep
                                    <img src={arrow}></img>
                                </button>
                            </ButtonKunjungi>
                        </TextContainer>
                    </BodyContainer>
                </ImageContainer>
            </div>

        </div >
    )
}

export default Slide