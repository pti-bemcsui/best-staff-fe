import styled from "styled-components";

const Description = styled.div`
  margin: 178px 150px 80px 150px;
  height: 400px;
`;
const Line = styled.div`
  width: 146px;
  margin: 10px 0;
  border: 8px solid #f99622;
`;
const Title = styled.p`
  height: 30px;
  width: 765px;
  border-radius: nullpx;
  font-family: Poppins;
  font-size: 60px;
  font-style: normal;
  font-weight: 800;
  line-height: 90px;
  letter-spacing: 0em;
  text-align: left;
  color: #0f3b5e;
`;
const Descword = styled.div`
  float: left;
  width: 50%;
  margin-bottom: 50px;
`;
const Word = styled.p`
  font-size: 18px;
  font-style: normal;
`;
const Descimage = styled.div`
  float: left;
  width: 50%;
  margin-top: -150px;
`;
const Image = styled.img``;
const MonthButton = styled.a`
  background-color: #f99622;
  color: #ffffff;
  padding: 5px 30px;
  font-weight: 600;
  margin-right: 30px;
  border-radius: 20px;
  box-shadow: 3px 5px 5px #a1a1a1;
`;
const YearButton = styled.a`
  background-color: #f99622;
  color: #ffffff;
  padding: 5px 30px;
  font-weight: 600;
  border-radius: 20px;
  box-shadow: 3px 5px 5px #a1a1a1;
`;
const BestStaffBackground = styled.div`
  height: 800px;
  margin-top: 0px;
  padding: 10px 250px;
  z-index: -1;
  border-bottom-right-radius: 35%;
  border-bottom-left-radius: 35%;
  background-color: #f99622;
`;
const Text = styled.p`
  position: fix;
  top: 120px;
  color: #0f3b5e;
  font-size: 50px;
  font-weight: 1000;
  text-align: center;
  z-index: 1;
`;
const ImageContainer = styled.div`
  width: 300px;
  height: 600px;
  z-index: 3;
  position: relative;
  left: 30%;
  top: 0%;
`;
const MonthTitle = styled.p`
  background-color: #f99622;
  color: #0f3b5e;
  width: 300px;
  font-size: 35px;
  font-weight: 700;
  text-align: center;
  border-bottom: 7px solid #ffffff;
`;
const PersonImage = styled.div`
  height: 280px;
  width: 300px;
  left: 0%;
  top: 0%;
  background-size: 100% 100%;
  margin-top: -7%;
  position: relative;
  background-image: url(${(props) => props.src});
  border: 1px solid black;
  box-shadow: 10px 0px 20px rgba(0, 0, 0, 0.25);
`;
const TextContainer = styled.div`
  width: 300px;
  height: 80px;
  background: #fafafa;
  top: -60px;
  position: relative;
  text-align: center;
  box-shadow: 10px 0px 20px rgba(0, 0, 0, 0.25);
`;
const BirdepTitle = styled.p`
  margin: 0;
  color: black;
  font-size: 22px;
  font-weight: 500;
  z-index: 5;
  position: relative;
`;
const OlderList = styled.div`
  margin: 100px;
`;
