import React, { useEffect, useState } from 'react'
import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Button,
    Image
} from '@chakra-ui/react'
import styled from 'styled-components'

const StyledSelect = styled.select`
  background-color: #F4F1E9;
  border: 1px solid black;
  border-radius: 8px;
  &:hover {
    cursor: pointer;
  }
  padding: 6px;
  font-family: 'Commissioner';
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
`;

function NewModal(props) {
    const [selectedLeaderboad, setSelectedLeaderboard] = React.useState();

    useEffect(() => {
        if (props.leaderboardData) {
            setSelectedLeaderboard(props.leaderboardData[0].linkAlternate);
        }
    }, [props.leaderboardData]);

    return (
        <Modal isOpen={props.isOpen} onClose={props.onClose} isCentered>
            <ModalOverlay />
            <ModalContent background={'#f4f1e9'}>
                <ModalHeader textAlign={'center'} fontWeight='700' fontSize={{ base: '36px', lg: '64px' }} fontFamily={'Fraunces'}>Leaderboard</ModalHeader>
                <ModalCloseButton />
                <ModalBody display={'flex'} justifyContent='center' alignItems={'center'} flexDirection='column'>
                    {props.leaderboardData ?
                        <>
                            <StyledSelect
                                value={selectedLeaderboad}
                                onChange={e => setSelectedLeaderboard(e.target.value)}
                            >
                                {props.leaderboardData.map(leaderboard => <option value={leaderboard.linkAlternate} key={leaderboard.linkAlternate}>{leaderboard.bulan}, {leaderboard.tahun}</option>)}
                            </StyledSelect>
                            <br />
                            <Image src={selectedLeaderboad} />
                        </> : <span>Belum ada Leaderboard</span>
                    }
                </ModalBody>
                <ModalFooter display={'flex'} justifyContent='center' alignItems={'center'}>
                    <Button background={'#121263'} mr={3} onClick={props.onClose} >
                        &larr; Kembali
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default NewModal