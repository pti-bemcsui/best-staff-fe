import styled from "styled-components";
import axios from "axios";
import { useEffect, useState } from "react";
import WavyBalls from "../../utils/waveballs";
import { TitleFrom, TitleTo } from "../../utils/transition";
import { motion } from "framer-motion";
import { NoProperty, Line } from "../Homepage";
import { Title } from "../Voting";


const Description = styled.div`
  margin: 120px 235px 30px 235px;
  height: 160px;

  @media screen and (max-width: 1400px) {
    margin: 120px 175px 30px 175px;
  }
  @media screen and (max-width: 1300px) {
    margin: 120px 125px 30px 125px;
  }
  @media screen and (max-width: 992px) {
    margin: 120px 175px 30px 175px;
  }
  @media screen and (max-width: 900px) {
    margin: 120px 125px 30px 125px;
  }
  @media screen and (max-width: 800px) {
    margin: 120px 75px 30px 75px;
  }
  @media screen and (max-width: 768px) {
    margin: 120px 150px 30px 150px;
  }
  @media screen and (max-width: 700px) {
    margin: 120px 100px 30px 100px;
    height: 180px;
  }
  @media screen and (max-width: 550px) {
    margin: 80px 50px 30px 50px;
    height: 150px;
  }
  @media screen and (max-width: 362px) {
    height: 180px;
  }
  @media screen and (max-width: 307px) {
    height: 210px;
  }
`;
export const Line2 = styled(Line)`
  width: 100px;
`;

// const Title = styled.p`
//   height: 30px;
//   border-radius: 0px;
//   font-family: Poppins;
//   font-size: 48px;
//   font-style: normal;
//   font-weight: 800;
//   letter-spacing: 0em;
//   text-align: left;
//   color: #0f3b5e;
//   position: relative;
//   top: -30px;
//   text-decoration: none;

//   @media screen and (max-width: 550px) {
//     font-size: 36px;
//     top: 0px;
//   }
//   @media screen and (max-width: 310px) {
//     margin-bottom: 40px;
//   }
// `;

const StaffList = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 0vh 12vw;
  margin: 5rem 5rem 10rem 5rem;
  gap: 8rem 5rem;
  justify-content: center;
  align-items: flex-start;

  @media screen and (max-width: 1300px) {
    padding: 0vh 6vw;
  }
  @media screen and (max-width: 500px) {
    gap: 5rem;
  }
`;

const ImageContainer = styled.div`
  display: flex;
  max-width: 275px;
  flex-direction: column;
  height: 350px;
  z-index: 3;
  position: relative;
  top: 0%;
  flex: 2;

  @media screen and (max-width: 384px) {
    height: 300px;
    left: 5%;
  }
`;

const HeadContainer = styled.div`
  background: #f99622;
  color: #ffffff;
  position: relative;
  text-align: center;
  text-transform: uppercase;
  font-size: 22px;
  font-weight: 500;
  flex: 0;
  left: -30px;
  top: -20px;
  width: 70%;
  height: 15%;
  padding: 3px 0;

  @media screen and (max-width: 500px) {
    width: 120px;
    height: 33px;
    font-size: 18px;
    margin-left: -30px;
    top: -12px;
    left: -10px;
  }
`;

const HeadContainer2 = styled.div`
  margin-left: -50px;
  top: -19px;
  width: 200px;
  height: 50px;
  background: #0f3b5e;
  color: #ffffff;
  padding: 3px 0;
  position: relative;
  text-align: center;
  text-transform: uppercase;
  font-size: 25px;
  font-weight: 500;
  box-shadow: 10px 0px 20px rgba(0, 0, 0, 0.25);

  @media screen and (max-width: 500px) {
    width: 120px;
    height: 33px;
    font-size: 18px;
    margin-left: -30px;
    top: -12px;
  }
`;

const PersonImage = styled.div`
  height: 330px;
  width: 260px;
  left: 0%;
  top: 0%;
  background-size: 100% 100%;
  margin-top: -7%;
  position: relative;
  flex: 0px;
  background-image: url(${(props) => props.src});
  box-shadow: 10px 0px 20px rgba(0, 0, 0, 0.25);

  @media screen and (max-width: 500px) {
    width: 240px;
    height: 230px;
  }
  @media screen and (max-width: 384px) {
    width: 200px;
  }
`;

const TextContainer = styled.div`
  width: 260px;
  height: auto;
  background: #fafafa;
  padding: 5px 10px;
  margin-top: 0px;
  position: relative;
  text-align: left;
  box-shadow: 10px 0px 20px rgba(0, 0, 0, 0.25);

  @media screen and (max-width: 500px) {
    width: 240px;
  }
  @media screen and (max-width: 384px) {
    width: 200px;
  }
`;

const BirdepTitle = styled.p`
  margin: 0;
  color: #0f3b5e;
  font-size: 20px;
  line-height: 40px;
  text-transform: uppercase;
  z-index: 5;
  position: relative;

  @media screen and (max-width: 500px) {
    font-size: 16px;
    line-height: 35px;
  }
  @media screen and (max-width: 384px) {
    font-size: 13px;
    line-height: 30px;
  }
`;

const BirdepTitle2 = styled.p`
  margin: 0;
  color: #0f3b5e;
  font-size: 20px;
  font-weight: 500;
  z-index: 5;
  position: relative;

  @media screen and (max-width: 500px) {
    font-size: 18px;
  }
  @media screen and (max-width: 384px) {
    font-size: 14px;
  }
`;

const imageValidate = (link, linkAlternate = null) => {
  var res =
    "https://cdn.discordapp.com/attachments/755605623214964900/858178467349790731/Rectangle_59.png";
  var linkImage = link;
  //console.log(link)
  if (linkImage){ 
    if (linkImage.includes("http")) {
      res = linkImage;
    }
  } else if (linkAlternate && linkAlternate.includes("http")) {
    res = linkAlternate;
  }
  return res;
};

const BestStaff = (props) => {
  const [bestStaff, setBest_Staff] = useState(undefined);
  const [show, setShow] = useState(false);
  function fetchData() {
    axios
      .get("https://ptibem.cs.ui.ac.id/beststaff/api/v1/homepage")
      .then((res) => {
        const result = res.data.data.best_staff_bulanan;
        setBest_Staff(result);
        setShow(true);
      });
      // .catch((err) => console.log(err));
  }

  useEffect(() => {
    fetchData();
  }, []);

  if (!show) {
    return (
      <div
        style={{ minHeight: "100vh", display: "flex", flexDirection: "column" }}
      >
        <WavyBalls></WavyBalls>
        <h1 style={{ margin: "30px auto" }}>Loading ... </h1>
      </div>
    );
  }

  return (
    <div>
      <Description>
        <Title>BEST STAFF OF THE MONTH</Title>
        <Line2/>
      </Description>
      <StaffList>
        {bestStaff === undefined || bestStaff.length === 0 ? (
          <NoProperty style={{ marginTop: "0" }}>
          Best staff belum dipilih :(
        </NoProperty>
        ) : (
          bestStaff
            .sort((a, b) => (a > b ? 1 : -1))
            .map((data) =>
              data.id % 2 == 0 ? (
                <motion.div
                  initial={TitleFrom}
                  animate={TitleTo}
                  transition={{ duration: 2 }}
                >
                  <ImageContainer>
                    <PersonImage src={imageValidate(data.best_staff.foto, data.best_staff.linkAlternate)}>
                      <HeadContainer>{data.nama_bulan}</HeadContainer>
                    </PersonImage>
                    <TextContainer>
                      <BirdepTitle style={{ fontWeight: "700" }}>
                        {data.best_staff.nama}
                      </BirdepTitle>
                      {/* <hr/> */}
                      <BirdepTitle style={{ fontWeight: "500" }}>
                        {data.best_staff.kode_birdept}
                      </BirdepTitle>
                    </TextContainer>
                  </ImageContainer>
                </motion.div>
              ) : (
                <ImageContainer>
                  <PersonImage src={imageValidate(data.best_staff.foto, data.best_staff.linkAlternate)}>
                    <HeadContainer2>{data.nama_bulan}</HeadContainer2>
                  </PersonImage>
                  <TextContainer>
                    <BirdepTitle style={{ fontWeight: "700" }}>
                      {data.best_staff.nama}
                    </BirdepTitle>
                    {/* <hr/> */}
                    <BirdepTitle style={{ fontWeight: "500" }}>
                      {data.best_staff.kode_birdept}
                    </BirdepTitle>
                  </TextContainer>
                </ImageContainer>
              )
            )
        )}
      </StaffList>
    </div>
  );
};
export default BestStaff;
