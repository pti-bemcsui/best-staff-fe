import styled, { css } from "styled-components";
import { Link, useLocation, useHistory } from "react-router-dom";
import Success from "./Success.png";
import Failed from "./Failed.png";
import { useState, useEffect } from "react";
import { useMediaPredicate } from "react-media-hook";
import axios from "axios";
import roundToNearestMinutes from "date-fns/esm/fp/roundToNearestMinutes/index.js";
import { getCookie } from "../../helpers/cookies";
import Swal from 'sweetalert2'
import { Title, Square } from "../Voting";
import Decoration from "./Assets/Decoration.png";
import detect from "detect.js";

const Main = styled.div`
  margin: 0px 160px;

  @media screen and (max-width: 960px) {
    margin: 0px 80px;
  }

  @media screen and (max-width: 768px) {
    margin: 0px 20px;
  }
`;

const TContainer = styled.section`
  display: flex;
  flex-direction: row;
  margin-top: 10vh;
  margin-bottom: 0px;
  justify-content: space-between;
`;
const QContainer = styled.section`
  display: ${props => props.condition ? "flex" : "none"};
  text-align: center;
  flex-direction: column;
  width: 100%;
  height: auto;
  margin-bottom: 10vh;
`;

const Title2 = styled(Title)`
  font-family: "Commissioner";
  font-style: normal;
  font-weight: 700;
  font-size: 56px;
  line-height: 120%;
  /* identical to box height, or 77px */

  color: #0f3b5e;
  @media screen and (max-width: 960px) {
    font-size: 32px;
  }
`;

const Square2 = styled(Square)`
  @media screen and (max-width: 768px){
    position: relative;
    right: 15px;
  }
`

// const Title = styled.p`
//   height: 30px;
//   border-radius: 0px;
//   font-family: Poppins;
//   font-size: 60px;
//   font-style: normal;
//   font-weight: 800;
//   letter-spacing: 0em;
//   text-align: left;
//   color: #0f3b5e;
//   position: relative;
//   top: -30px;
//   text-decoration: none;

//   @media screen and (max-width: 670px) {
//     right: 40px;
//   }

//   @media screen and (max-width: 604px) {
//     font-size: 44px;
//     top: -10px;
//     right: 20px;
//   }
// `;
const Question = styled.section`
  width: 100%;
  height: auto;
  padding-bottom: 8px;
  background: rgba(18, 18, 99, 1);
  border-radius: 12px 12px 0px 0px;
`;
const Poll = styled.section`
  width: 100%;
  height: 600px;
  overflow-y: scroll;
  color: black;
  background: #f8f8f8;
  padding-top: 32px;
  border-radius: 0px 0px 12px 12px;

  @media screen and (max-width: 960px) {
    overflow-y: hidden;
    height: auto;
  }
`;
const Table = styled.table`
  margin: auto;
  width: 95%;
  border-collapse: collapse;
  border: 0;
  border-color: black;
  margin: 16px;

  @media screen and (max-width: 960px) {
    margin: auto;
    margin-bottom: 60px;
    width: 80%;
  }

  @media screen and (max-width: 465px) {
    margin-left: 9.5%;
    margin-right: 9.5%;
  }

  @media screen and (max-width: 376px) {
    margin-left: 8.7%;
    margin-right: 8.7%;
  }

  @media screen and (max-width: 321px) {
    margin-left: 8%;
    margin-right: 8%;
  }
`;
const Popup = styled.section`
  display: flex;
  flex-direction: column;
  width: 500px;
  height: 300px;
  z-index: 1;

  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  margin: 0;
  background: white;
  border-radius: 24px;
  filter: drop-shadow(0px 2px 2px rgba(0, 0, 0, 0.25));
`;

// const Square = styled.section`
//   padding: 3px;
//   background: #f99622;
//   margin-top: 4px;
//   width: 144px;

//   @media screen and (max-width: 670px) {
//     position: relative;
//     // width: 120px;
//     right: 30px;
//     top: 20px;
//   }
//   @media screen and (max-width: 604px) {
//     position: relative;
//     width: 120px;
//     right: 18px;
//     top: 0;
//   }
//   @media screen and (max-width: 362px) {
//     top: 40px;
//     width: 90px;
//   }

// `;

const TData = styled.th`
  padding-bottom: 0px;
  width: 7.5%;
  margin-left: 8px;
  position: relative;
`;
const RadioButton = styled.th`
  padding-bottom: 20px;
  position: relative;
  @media screen and (max-width: 960px) {
    padding-top: 4px;
    padding-right: 2px;
  }

  @media screen and (max-width: 1144px) {
    left: 0%;
  }
`;
const Button = styled.button`
  display: flex;
  z-index: 1;

  margin: auto;
  padding: 12px 48px;
  background: #f99622;
  border: 0;
  border-radius: 12px;
  outline: none;

  font-size: 20px;
  color: white;
  cursor: pointer;

  @media screen and (max-width: 960px) {
    padding: 8px 24px;
  }
`;

const RadioButtonLabel = styled.label`
  position: relative;
  bottom: 3px;
  left: 35%;
  display: block;
  width: 25px;
  height: 25px;
  border-radius: 50%;
  background: rgba(253, 224, 203, 0.3);
  border: 1px solid #bebebe;
  // border: 1px solid red;

  @media screen and (max-width: 960px) {
    bottom: 11px;
    left: 40%;
    width: 15px;
    height: 15px;
  }

  @media screen and (max-width: 426px) {
    left: 25%;
  }

  @media screen and (max-width: 376px) {
    left: 20%;
  }

  @media screen and (max-width: 321px) {
    left: 15%;
  }
`;
const Circle = styled.input`
  position: relative;
  top: 22px;
  left: 35%;
  display: block;
  width: 25px;
  height: 25px;
  border-radius: 50%;
  cursor: pointer;
  opacity: 1;
  z-index: 1;
  &:hover ~ ${RadioButtonLabel} {
    background: #ffcba5;
  }
  &:checked ~ ${RadioButtonLabel} {
    background: #ff7c1f;
  }

  @media screen and (max-width: 960px) {
    top: 4px;
    left: 40%;
    width: 15px;
    height: 15px;
  }

  @media screen and (max-width: 426px) {
    left: 25%;
  }

  @media screen and (max-width: 376px) {
    left: 20%;
  }

  @media screen and (max-width: 321px) {
    left: 15%;
  }
`;
const Scroll = styled.div`
  display: block;
  overflow-x: hidden;
  white-space: nowrap;

  ${(finish) =>
    !finish &&
    css`
      padding-bottom: 500px;
    `}
`;
const OnMobile = styled.div``;
const PersonName = styled.h4`
  font-family: "Commissioner";
  font-weight: 700;
  font-size: 16px;

  @media screen and (max-width: 960px) {
    font-size: 12px;
    margin-top: 24px;
    position: relative;
    word-wrap: break-word;
  }

  @media screen and (max-width: 376px) {
    left: -1.5%;
  }
`;

const Pertanyaan = styled.p`
  width: max-content;
  margin: auto;
  font-family: 'Commissioner';
  font-style: normal;
  font-weight: 700;
  font-size: 30px;
  line-height: 120%;
  color: #FFFFFF;
  margin-bottom: 12px;

  @media screen and (max-width: 768px) {
    font-size: 20px;
  }
`;
const IsiPertanyaan = styled.p`
  width: max-content;
  margin: auto;
  font-family: "Commissioner";
  font-style: normal;
  font-weight: 400;
  font-size: 20px;
  line-height: 120%;
  margin-bottom: 20px;

  @media screen and (max-width: 768px) {
    font-size: 15px;
  }
`;

const Ket = styled.h5`
  margin-top: 20px;
  font-family: "Commissioner";
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  line-height: 120%;
  margin-bottom: 13px;

  @media screen and (max-width: 768px) {
    font-size: 15px;
  }
`;
const Desc = styled.p`
  
  font-family: 'Commissioner';
  font-style: normal;
  font-weight: 400;
  font-size: 16px;

  color: #FFFFFF;
  width: 100%;
  margin: -4px auto;
  word-wrap: break-word;
  white-space: normal;

  @media screen and (max-width: 960px) {
    width: 100%;
    margin-top: 8px;
    font-size: 14px;
  }

  @media screen and (max-width: 768px) {
    width: 100%;
    margin-top: 8px;
    font-size: 12px;
  }
`;

const MainDesktop = styled.div`
  display: block;
  overflow-x: hidden;
  @media screen and (max-width: 960px) {
    display: none;
  }

`;
const Mobile = styled.div`
  display: none;
  @media screen and (max-width: 960px) {
    display: block;
    overflow-x: hidden;
  }
`;

const Penjelasan = styled.p`
  margin-top: 4px;
  font-size: 15px;
  font-family: "Commissioner";

  @media screen and (max-width: 960px) {
    margin-top: 4px;
    font-size: 14px;
    font-family: "Commissioner";
  }

  @media screen and (max-width: 768px) {
    margin-top: 4px;
    font-size: 12px;
    font-family: "Commissioner";
  }
`;

const Content = styled.div`
  width: 100%;
`

const children = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const desc =
  [
    'Personality : terbuka akan hal baru, mudah beradaptasi, ramah, percaya diri',
    'Kinerja : kehadiran di kegiatan BEM, tepat waktu&maksimal dalam mengerjakan tugas',
    'Kontribusi : aktif, inisiatif, menghargai pendapat, memberi kritik dan saran',
    'Komunikasi : fastrespon, dapat menyampaikan pesan dan memahami instruksi dengan baik',
    'Kredibel : konsisten, dapat diandalkan'
  ]
const rate = children.map((number) => (
  <TData style={{ fontSize: "15px" }}>{number}</TData>
));
const rate1 = children
  .slice(0, 5)
  .map((number) => <TData style={{ textIndent: "0" }}>{number}</TData>);
const rate2 = children
  .slice(5)
  .map((number) => <TData style={{ textIndent: "0" }}>{number}</TData>);
let errorMessage = "Ada error terjadi";

const Vote = () => {
  const location = useLocation().state;
  const history = useHistory()
  // console.log(location);
  const [defaultData, setDefaultData] = useState({
    questions: [
      {
        id: "string",
        question: "string",
        staff: [{ nama: "", id: "" }],
      },
    ],
  });
  const [loading, setLoading] = useState(false);
  const handleError = (response) => {
    if (response.status == 400) {
      errorMessage = "Bulan atau tahun atau birdept tidak ditemukan";
      throw Error(response.status);
    } else if (response.status == 403) {
      errorMessage = "Kamu sudah melakukan vote";
      throw Error(response.status);
    } else if (response.status == 406) {
      errorMessage = "Kamu bukan anggota dari biro/departemen ini";
      throw Error(response.status);
    } else if (response.status != 200) throw Error(response.status);
    return response;
  };

  const token = localStorage.getItem("token") || getCookie("bs-token");
  const request1 = {
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  const request2 = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  const detectMob = () => {
    const toMatch = [
      /Android/i,
      /webOS/i,
      /iPhone/i,
      /iPad/i,
      /iPod/i,
      /BlackBerry/i,
      /Windows Phone/i,
    ];

    return toMatch.some((toMatchItem) => {
      return navigator.userAgent.match(toMatchItem);
    });
  };

  function fnBrowserDetect() {

    let browserName;
    let browserShift;

    var user = detect.parse(navigator.userAgent);

    // Display some property values in my browser's dev tools console
    browserName = user.browser.family.toLowerCase();
    let width = window.innerWidth - 160;

    if (browserName === "firefox") {
      browserShift = width / 500;
    } else if (browserName === "safari") {
      browserShift = 2;
    } else if (browserName === "opera") {
      browserShift = width / 1600;
    } else if (browserName === "edge") {
      browserShift = width / 400;
    } else if (browserName === "chrome") {
      browserShift = width / 400;
    } else {
      browserName = "No browser detection";
      browserShift = 0;
    }
    //console.log(browserName);
    //console.log(browserShift);
    return browserShift;
  }

  useEffect(() => {
    const URL = `https://ptibem.cs.ui.ac.id/beststaff${location}/`;
    fetch(URL, request1)
      .then(handleError)
      .then((response) => response.json())
      .then((data) => {
        //console.log(data);
        setDefaultData(data.data);
        setDefaultDataLength(data.data.questions.length);
      })
      .catch((error) => {
        Swal.fire({
          position: "center",
          icon: "error",
          title: "Akses ditolak",
          text: errorMessage,
          showConfirmButton: "tutup",
        }).then((result) => {
          window.location = "/best-staff/voting";
        });
        // console.log(error);
      });
  }, []);

  const [browserShiftState, setBrowserShiftState] = useState(0);
  const [cutWords, setCutWords] = useState(5);
  const [cutShift, setCutShift] = useState(window.innerWidth <= 768 ? -120 + browserShiftState : window.innerWidth <= 960 ? 0 + browserShiftState : detectMob() ? 160 : 174 + browserShiftState);

  useEffect(() => {
    const browserShift = fnBrowserDetect();
    setBrowserShiftState(browserShift);

    function handleResize() {
      setWidth(window.innerWidth - 160);
      setCutWords(
        window.innerWidth < 480 ? 3 : window.innerWidth < 640 ? 4 : 5
      );
      setCutShift(window.innerWidth <= 768 ? -120 + browserShift : window.innerWidth <= 960 ? 0 + browserShift : detectMob() ? 160 : 174 + browserShift);
    }

    handleResize();
    window.addEventListener("resize", handleResize);
  });

  let data = defaultData;
  const [currentPage, setCurrentPage] = useState(1);
  const [defaultDataLength, setDefaultDataLength] = useState(5);
  const [showPrevious, setShowPrevious] = useState(false);
  const [showNext, setShowNext] = useState(true);
  const [showSubmit, setShowSubmit] = useState(false);
  const [PostError, setPostError] = useState(false);
  const nextPage = () => {
    setCurrentPage(currentPage + 1);
    if (currentPage == defaultDataLength - 1) {
      setShowNext(false);
      setShowSubmit(true);
      setShowPrevious(true);
    } else setShowPrevious(true);
  };
  const previousPage = () => {
    setCurrentPage(currentPage - 1);
    if (currentPage == 2) {
      setShowPrevious(false);
      setShowSubmit(false);
      setShowNext(true);
    } else {
      setShowSubmit(false);
      setShowNext(true);
    }
  };

  const DisplayQuestion = (word) => {
    var arr = word.split(" ");
    var display = [];
    var wording = "";
    var cnt = 1;

    console.log("tes");
    for (let i = 0; i < arr.length; i++) {
      if (arr.length === i + 1) {
        wording += arr[i];
        display.push(<IsiPertanyaan>{wording}</IsiPertanyaan>);
        // display.push(<br />);
      } else if (cnt % cutWords === 0) {
        wording += arr[i];
        display.push(<IsiPertanyaan>{wording}</IsiPertanyaan>);
        // display.push(<br />);
        cnt = 1;
        wording = "";
      } else {
        wording += arr[i] + " ";
        cnt += 1;
      }
    }

    return display;
  };

  const MobileDisplayName = (nama = "Nama Staff") => {
    var wording = "";
    var cnt = 1;
    var Display = [];
    var spt = nama.split(" ");
    for (let i = 0; i < spt.length; i++) {
      if (spt.length === i + 1) {
        wording += spt[i];
        Display.push(<PersonName>{wording}</PersonName>);
        // display.push(<br />);
      } else if (cnt % (cutWords - 1) === 0) {
        wording += spt[i];
        Display.push(<PersonName>{wording}</PersonName>);
        // display.push(<br />);
        cnt = 1;
        wording = "";
      } else {
        wording += spt[i] + " ";
        cnt += 1;
      }
    }

    return Display;
  };

  const MobileDisplayDesc = (nama = "Nama Staff") => {
    var wording = "";
    var cnt = 1;
    var Display = [];
    var spt = nama.split(" ");
    for (let i = 0; i < spt.length; i++) {
      if (spt.length === i + 1) {
        wording += spt[i];
        Display.push(<Desc>{wording}</Desc>);
        // display.push(<br />);
      } else if (cnt % (cutWords + 1) === 0) {
        wording += spt[i];
        Display.push(<Desc>{wording}</Desc>);
        // display.push(<br />);
        cnt = 1;
        wording = "";
      } else {
        wording += spt[i] + " ";
        cnt += 1;
      }
    }

    return Display;
  };

  const indexOfLastData = currentPage;
  const indexOfFirstData = indexOfLastData - 1;
  data = data.questions.slice(indexOfFirstData, indexOfLastData);

  const [validVote, setValidVote] = useState(true);
  const [popup, setPopup] = useState(false);
  const [popupError, setPopupError] = useState(false);
  const closePopup = () => setPopup(false);
  const [width, setWidth] = useState(window.innerWidth - 160);

  const saveValue = (number, index, staffIndex, type) => {
    const radioButtonsDesktop = document.getElementsByName(
      `${defaultData.questions[0].staff[staffIndex].nama} ${index} Dekstop`
    );
    const radioButtonsMobile = document.getElementsByName(
      `${defaultData.questions[0].staff[staffIndex].nama} ${index} Mobile`
    );
    if (type == 0) {
      radioButtonsMobile[number - 1].checked = true;
    } else if (type == 1) {
      radioButtonsDesktop[number - 1].checked = true;
    }
  };

  const submitVote = () => {
    setLoading(true);
    let checkValue = 0;
    let lockValue = 0;
    let payload = {
      bulan: "string",
      tahun: "string",
      vote_result: [
        {
          question: {
            id: "string",
            question: "string",
          },
          questionscore: [
            {
              id: "string", // npm yang divote
              nama: "string", // nama yang divote
              score: 0,
              birdep_choosen: "string",
            },
          ],
        },
      ],
    };
    payload.bulan = defaultData.bulan;
    payload.tahun = defaultData.tahun;
    for (let i = 0; i < defaultData.questions.length; i++) {
      payload.vote_result[i] = { question: {}, questionscore: [] };
      payload.vote_result[i].question.id = defaultData.questions[i].id;
      payload.vote_result[i].question.question =
        defaultData.questions[i].question;
      for (let q = 0; q < defaultData.questions[0].staff.length; q++) {
        const radioButtons = document.getElementsByName(
          `${defaultData.questions[0].staff[q].nama} ${i} Mobile`
        );
        // console.log(i);
        let value = 0;
        radioButtons.forEach((e) => {
          if (e.checked) {
            value = e.value;
            if (i == 0 && q == 0) {
              lockValue = value;
              checkValue++;
            } else if (lockValue == value) checkValue++;
          }
        });
        if (value == 0) {
          Swal.fire({
            position: "center",
            icon: "warning",
            title: "Respon gagal tersimpan!",
            text: "Mohon lengkapi semua form penilaian",
            showConfirmButton: false,
            timer: 1000,
          });
          setLoading(false);
          return;
        }
        payload.vote_result[i].questionscore[q] = {
          id: "",
          nama: "",
          score: 0,
        };
        payload.vote_result[i].questionscore[q].score = parseInt(value);
        payload.vote_result[i].questionscore[q].id =
          defaultData.questions[0].staff[q].id;
        payload.vote_result[i].questionscore[q].nama =
          defaultData.questions[0].staff[q].nama;
        payload.vote_result[i].questionscore[q].birdep_choosen =
          defaultData.kode_birdept;
      }
    }

    if (checkValue == defaultData.questions.length * payload.vote_result[0].questionscore.length) {
      setLoading(false);
      Swal.fire({
        title: "Error!",
        text: "Tidak dapat vote dengan nilai yang sama secara menyeluruh",
        icon: "error",
        confirmButtonText: "Tutup",
      });
    } else {
      const URL = `https://ptibem.cs.ui.ac.id/beststaff${location}/`;
      // console.log("URL : ", URL, " PAYLOAD : ", payload);
      axios
        .post(URL, payload, request2)
        .then((response) => {
          // console.log(response);
          // console.log(payload);
          setValidVote(true);
          setPopup(true);
          setLoading(false);
          /*
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Berhasil',
            text: 'Terima kasih! Votemu telah disimpan',
            confirmButtonText: 'Tutup'
            })*/
          history.push({ pathname: "/best-staff/voting", state: "ada" })
          // window.location= "/best-staff/voting";

        })
        .catch((error) => {
          // console.log(error);
          // setPostError(true);
          setLoading(false);
          Swal.fire({
            title: "Gagal Mengirim Data",
            text: "Mohon coba lagi nanti",
            icon: "error",
            // confirmButtonText: 'Tutup'
          });
        });
    }
  };

  // const showValid = validVote ? (
  //   <>
  //     <img
  //       src={Success}
  //       style={{ width: "100px", height: "100px", margin: "auto" }}
  //     />
  //     <h3 style={{ textAlign: "center" }}>
  //       Terima kasih! Votemu telah disimpan
  //     </h3>
  //     <Link
  //       to="/best-staff/voting"
  //       style={{ margin: "auto", textDecoration: "none" }}
  //     >
  //       <Button>Close</Button>
  //     </Link>
  //   </>
  // ) : (
  //   <>
  //     <img
  //       src={Failed}
  //       style={{ width: "100px", height: "100px", margin: "auto" }}
  //     />
  //     <h3 style={{ textAlign: "center" }}>Ada bagian yang belum diisi</h3>
  //     <Button onClick={closePopup}>Close</Button>
  //   </>
  // );

  const checkIsDesktop = useMediaPredicate("(min-width: 961px)");

  const checkIsMobile = useMediaPredicate("(max-width: 960px)");


  let rows = [];

  return (
    <Main>
      <TContainer>
        <Title2>Voting Best staff</Title2>
        {checkIsDesktop ? (
          <img
            src={Decoration}
            alt="Logo"
            style={{
              width: "332px",
              height: "80px",
              marginTop: "72px"
            }}
          />
        ) : null}
      </TContainer>
      {defaultData.questions.forEach((page, index) => {
        rows.push(
          <td width={`${100 / (defaultData.questions.length + 1)}%`}>
            <QContainer condition={currentPage - 1 == index}>
              <Question>
                <div style={{ color: "white" }}>
                  <Pertanyaan style={{ marginTop: "32px" }}>
                    PERTANYAAN {index + 1}
                  </Pertanyaan>
                  <IsiPertanyaan>{page.question}</IsiPertanyaan>
                  {/*DisplayQuestion(page.question)*/}
                  <Desc>{page.description}</Desc>
                  {/*MobileDisplayDesc(desc[index])*/}
                </div>
                <div
                  style={{
                    textAlign: "left",
                    marginLeft: "16px",
                    padding: "16px",
                    color: "white",
                  }}
                >
                  <Ket>Keterangan</Ket>
                  <Penjelasan>
                    1-2 : tidak memenuhi kriteria apapun
                    <br />
                    3-4 : memenuhi 1 kriteria
                    <br />
                    5-6 : memenuhi 2 kriteria
                    <br />
                    7-8 : memenuhi 3 kriteria
                    <br />
                    9-10 : memenuhi semua kriteria
                  </Penjelasan>
                </div>
              </Question>
              <Poll>
                {page.staff.map((staff, staffIndex) => (
                  <div style={{ marginBottom: "60px" }}>
                    <PersonName>{staff.nama}</PersonName>
                    <br />
                    <Content>
                      <Table
                        style={{
                          gap: "32px",
                          margin: "auto",
                        }}
                      >
                        <tr style={{ background: "#f8f8f8", color: "black" }}>
                          {rate}
                        </tr>
                        <tr>
                          {children.map((number) => (
                            <RadioButton>
                              <Circle
                                value={number}
                                type="radio"
                                name={`${staff.nama} ${index} Mobile`}
                                onClick={() =>
                                  saveValue(number, index, staffIndex, 1)
                                }
                              />
                              <RadioButtonLabel />
                            </RadioButton>
                          ))}
                        </tr>
                      </Table>
                    </Content>
                  </div>
                ))}
              </Poll>
            </QContainer>
          </td>
        );
      })}
      {checkIsDesktop &&
        <MainDesktop>
          <Scroll>
            <table
              width={width}
              test={currentPage}
              style={{
                transform: `translateX(${-1 * (currentPage - 1) * (width - cutShift)
                  }px)`,
                overflowX: "auto",
                width: `${defaultData.questions.length * 100}%`,
              }}
            >
              <tr style={{ width: "20%" }}>{rows}</tr>
            </table>
          </Scroll>
        </MainDesktop>
      }
      {checkIsMobile &&
        <Mobile>
          <Scroll>
            <table
              width={width}
              test={currentPage}
              style={{
                transform: `translateX(${-1 * (currentPage - 1) * (width - cutShift)
                  }px)`,
                overflowX: "auto",
                width: `${defaultData.questions.length * 100}%`,
              }}
            >
              <tr style={{ width: "20%" }}>{rows}</tr>
            </table>
          </Scroll>
        </Mobile>
      }

      {checkIsDesktop && (
        <div style={{ display: "flex", flexDirection: "row" }}>
          {showPrevious && (
            <Button
              style={{ marginBottom: "10vh", background: "#121263" }}
              onClick={previousPage}
            >
              Previous
            </Button>
          )}
          {showNext && (
            <Button
              style={{ marginBottom: "10vh", background: "#121263" }}
              onClick={nextPage}
            >
              Next
            </Button>
          )}
          {showSubmit && loading === false && (
            <Button
              style={{ marginBottom: "10vh", background: "#121263" }}
              onClick={submitVote}
            >
              Submit
            </Button>
          )}
        </div>
      )}
      {checkIsMobile && (
        <div style={{ display: "flex", flexDirection: "row", marginBottom: "32px" }}>
          {showPrevious && (
            <Button
              style={{ marginBottom: "10vh", background: "#121263" }}
              onClick={previousPage}
            >
              Previous
            </Button>
          )}
          {showNext && (
            <Button
              style={{ marginBottom: "10vh", background: "#121263" }}
              onClick={nextPage}
            >
              Next
            </Button>
          )}
          {showSubmit && loading === false && (
            <Button
              style={{ marginBottom: "10vh", background: "#121263" }}
              onClick={submitVote}
            >
              Submit
            </Button>
          )}
        </div>
      )}
      {popupError && (
        <div
          style={{
            width: "100%",
            height: "100%",
            position: "fixed",
            background: "rgba(0,0,0,0.25)",
            top: "0",
            left: "0",
            zIndex: "1",
          }}
        >
          <Popup>
            <img
              src={Failed}
              style={{
                width: "100px",
                height: "100px",
                margin: "auto",
                marginTop: "36px",
              }}
            />
            <h3 style={{ textAlign: "center" }}>{errorMessage}</h3>
            <Link
              to="/best-staff/voting"
              style={{ margin: "auto", textDecoration: "none" }}
            >
              <Button>Close</Button>
            </Link>
          </Popup>
        </div>
      )}
      {PostError ? (
        <div
          style={{
            width: "100%",
            height: "100%",
            position: "fixed",
            background: "rgba(0,0,0,0.25)",
            top: "0",
            left: "0",
            zIndex: "1",
          }}
        >
          <Popup>
            <img
              src={Failed}
              style={{
                width: "100px",
                height: "100px",
                margin: "auto",
                marginTop: "36px",
              }}
            />
            <h3 style={{ textAlign: "center" }}>Gagal mengirim data</h3>
            <p style={{ textAlign: "center" }}>Mohon coba lagi nanti</p>
            <Link
              to="/best-staff/voting"
              style={{ margin: "auto", textDecoration: "none" }}
            >
              <Button>Close</Button>
            </Link>
          </Popup>
        </div>
      ) : null}
    </Main>
  );
};;;;;;;;;;;;;
export default Vote;
