import React, { useState } from "react";
import { ChevronDownIcon } from "@chakra-ui/icons";

export const FilterDropDown = (props) => {
  const [isDropDownOpen, setIsDropDownOpen] = useState(false);
  const [yearNow, setYearNow] = useState(2022);

  const dropDownHandler = () => {
    if (isDropDownOpen) {
      setIsDropDownOpen(false);
    } else {
      setIsDropDownOpen(true);
    }
  };

  const duaDuaHandler = () => {
    if (isDropDownOpen) {
      setYearNow(2022);
      setIsDropDownOpen(false);
      props.changeTahun(2022);
    } else {
      setYearNow(2022);
      setIsDropDownOpen(true);
    }
  };

  const duaSatuHandler = () => {
    if (isDropDownOpen) {
      setYearNow(2021);
      setIsDropDownOpen(false);
      props.changeTahun(2021);
    } else {
      setYearNow(2021);
      setIsDropDownOpen(true);
      props.changeTahun(2021);
    }
  };

  const duaPuluhHandler = () => {
    if (isDropDownOpen) {
      // setYearNow(2020);
      setIsDropDownOpen(false);
      window.open('https://bem.cs.ui.ac.id/best-staff/', '_blank');
    } else {
      // setYearNow(2020);
      setIsDropDownOpen(true);
      window.open('https://bem.cs.ui.ac.id/best-staff/', '_blank');
    }
  };

  // Fungsi info filter tahun melalui console
  const sendYearToConsole = () => {
    // console.log("Filter { Tahun : " + yearNow + " }");
  };

  // Berjalan setiap rendering
  sendYearToConsole();

  // Fungsi untuk GET tahun
  const sendTahun = () => {
    const tahunData = {
      tahun: yearNow,
    };
    props.onSendTahun(tahunData);
  };

  // Auto send tahunData ketika ada perubahan
  sendTahun();

  if (props.preset === "mobile") {
    return isDropDownOpen ? (
      <div className="flex flex-col w-3/4">
        <button
          className="bg-isabelline rounded-t-xl px-3 py-2"
          onClick={dropDownHandler}
        >
          <div className="flex justify-between items-center">
            <p className="text-spaceblue">{yearNow}</p>
            <ChevronDownIcon w={8} h={8} color="#121263" />
          </div>
        </button>
        <button
          className="bg-strawhat px-3 py-2"
          onClick={yearNow === 2022 ? duaSatuHandler : duaDuaHandler}
        >
          <div className="flex justify-between items-center">
            <p className="text-spaceblue">{yearNow === 2022 ? 2021 : 2022}</p>
          </div>
        </button>
        <button
          className="bg-strawhat rounded-b-xl px-3 py-2"
          onClick={yearNow === 2020 ? duaSatuHandler : duaPuluhHandler}
        >
          <div className="flex justify-between items-center">
            <p className="text-spaceblue">{yearNow === 2020 ? 2021 : 2020}</p>
          </div>
        </button>
      </div>
    ) : (
      <button
        className="bg-isabelline rounded-xl w-3/4 px-3 py-2"
        onClick={dropDownHandler}
      >
        <div className="flex justify-between items-center">
          <p className="text-spaceblue">{yearNow}</p>
          <ChevronDownIcon w={8} h={8} color="#121263" />
        </div>
      </button>
    );
  } else {
    return isDropDownOpen ? (
      <div className="flex flex-col">
        <button
          className="bg-isabelline rounded-t-lg px-3 py-2"
          onClick={dropDownHandler}
        >
          <div className="flex justify-between items-center">
            <p className="text-spaceblue pr-24 desktop:pr-48 text-[12px] desktop:text-[15px]">
              {yearNow}
            </p>
            <ChevronDownIcon w={4} h={4} color="#121263" />
          </div>
        </button>
        <button
          className="bg-strawhat px-3 py-2"
          onClick={yearNow === 2022 ? duaSatuHandler : duaDuaHandler}
        >
          <div className="flex justify-between items-center">
            <p className="text-spaceblue pr-24 desktop:pr-48 text-[12px] desktop:text-[15px]">
              {yearNow === 2022 ? 2021 : 2022}
            </p>
          </div>
        </button>
        <button
          className="bg-strawhat rounded-b-xl px-3 py-2"
          onClick={yearNow === 2020 ? duaSatuHandler : duaPuluhHandler}
        >
          <div className="flex justify-between items-center">
            <p className="text-spaceblue pr-24 desktop:pr-48 text-[12px] desktop:text-[15px]">
              {yearNow === 2020 ? 2021 : 2020}
            </p>
          </div>
        </button>
      </div>
    ) : (
      <button
        className="bg-isabelline rounded-lg px-3 py-2"
        onClick={dropDownHandler}
      >
        <div className="flex justify-between items-center">
          <p className="text-spaceblue pr-24 desktop:pr-48 text-[12px] desktop:text-[15px]">
            {yearNow}
          </p>
          <ChevronDownIcon w={4} h={4} color="#121263" />
        </div>
      </button>
    );
  }
};
