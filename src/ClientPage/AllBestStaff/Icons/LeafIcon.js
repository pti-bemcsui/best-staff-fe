export const LeafIcon = () => {
  return (
    <svg
      width="185"
      height="38"
      viewBox="0 0 185 38"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M147.353 37.0059C167.677 37.0059 184.153 20.5299 184.153 0.20586H147.353V37.0059Z"
        fill="#121263"
      />
      <path
        d="M147.353 0.205872C127.029 0.205872 110.553 16.6818 110.553 37.0059H147.353V0.205872Z"
        fill="#A32833"
      />
      <path
        d="M147.353 37.0059V0.205872L110.553 37.0059H147.353Z"
        fill="#121263"
      />
      <path
        d="M73.7532 37.0059V0.205872L110.553 37.0059H73.7532Z"
        fill="#121263"
      />
      <ellipse
        rx="8.832"
        ry="8.832"
        transform="matrix(4.37114e-08 -1 -1 -4.37114e-08 92.0061 18.7531)"
        fill="#F5D39B"
      />
      <path
        d="M36.9534 37.0059C57.2775 37.0059 73.7534 20.5299 73.7534 0.205864H36.9534V37.0059Z"
        fill="#A32833"
      />
      <path
        d="M73.7534 0.205862L36.9534 0.20586L73.7534 37.0059L73.7534 0.205862Z"
        fill="#121263"
      />
      <path
        d="M36.9533 0.205872C16.6292 0.205872 0.15332 16.6818 0.15332 37.0059H36.9533V0.205872Z"
        fill="#A32833"
      />
    </svg>
  );
};
