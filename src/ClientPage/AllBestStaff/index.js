import React, { useState } from "react";
import { AllBestStaffUpper } from "./AllBestStaffUpper";
import { AllBestStaffFilter } from "./AllBestStaffFilter";
import { AllBestStaffCompilation } from "./AllBestStaffCompilation";

const AllBestStaf = () => {
  const [tahun, setTahun] = useState(2022);
  const [paruh, setParuh] = useState(1);

  // Get Filter dari AllBestStaffFilter
  const getFilter = (enteredFilterData) => {
    const filterData = {
      ...enteredFilterData,
      id: Math.random().toString(),
    };
    // console.log(filterData);
    // Send tahun dan paruh ke props AllBestStaffCompilation
    setTahun(filterData.tahun);
    setParuh(filterData.paruh);
    // console.log(filterData);
  };

  const changeTahun = (tahun) => setTahun(tahun);
  const changeParuh = (paruh) => setParuh(paruh);

  return (
    <div>
      <AllBestStaffUpper />
      <AllBestStaffFilter onSendFilter={getFilter} changeTahun={changeTahun} changeParuh={changeParuh} tahun={tahun}/>
      <AllBestStaffCompilation tahun={tahun} paruh={paruh} />
    </div>
  );
};

export default AllBestStaf;
