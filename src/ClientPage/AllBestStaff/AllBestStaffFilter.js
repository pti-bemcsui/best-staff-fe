import React, { useState, useEffect } from "react";
import { FilterDropDown } from "./FilterDropdown";
import { ParuhButton } from "./ParuhButton";

export const AllBestStaffFilter = (props) => {
  // SOLUTION AUTO RESPONSIVE WITH GADGET
  // Using getter size of window width to first state
  const getWindowSize = () => {
    if (window.innerWidth < 768) {
      return true;
    } else {
      return false;
    }
  };

  const [isMobile, setIsMobile] = useState(getWindowSize);

  //choose the screen size
  const handleResize = () => {
    if (window.innerWidth < 768) {
      setIsMobile(true);
    } else {
      setIsMobile(false);
    }
  };

  // create an event listener
  useEffect(() => {
    window.addEventListener("resize", handleResize);
  });

  // Send props child => parent
  const filterData = {
    paruh: "",
    tahun: "",
  };

  // Get Paruh dari ParuhButton
  const getParuh = (enteredParuhData) => {
    const paruhData = {
      ...enteredParuhData,
      id: Math.random().toString(),
    };
    // console.log(paruhData);
    filterData["paruh"] = paruhData.paruh;
  };

  // Get Tahun dari FilterDropdown
  const getTahun = (enteredTahunData) => {
    const tahunData = {
      ...enteredTahunData,
      id: Math.random().toString(),
    };
    // console.log(tahunData);
    filterData["tahun"] = tahunData.tahun;
  };

  // Send filter ke index.js
  // Send props child => parent
  const sendFilter = () => {
    props.onSendFilter(filterData);
  };

  // Auto send filterData ketika ada perubahan
  // setInterval(sendFilter, 1000);

  return (
    <div className="bg-spaceblue py-6 desktop:pt-14">
      {!isMobile && (
        <div className="flex justify-between desktop:w-11/12 desktop:pl-16">
          <div className="pl-12 desktop:pl-16">
            <ParuhButton preset="nonmobile" onSendParuh={getParuh} changeParuh={props.changeParuh} tahun={props.tahun} />
          </div>
          <div className="flex tablet:justify-end pt-6">
            <div className="tablet:flex tablet:justify-end tablet:pr-16 desktop:pr-8">
              <FilterDropDown preset="nonmobile" onSendTahun={getTahun} changeTahun={props.changeTahun} />
            </div>
          </div>
        </div>
      )}
      {isMobile && (
        <div>
          <div className="flex mobile:justify-center tablet:justify-end pt-6">
            <FilterDropDown preset="mobile" onSendTahun={getTahun} />
          </div>
          <div className="flex justify-center">
            <ParuhButton preset="mobile" onSendParuh={getParuh} tahun={props.tahun} />
          </div>
        </div>
      )}
    </div>
  );
};
