import React, { useState, useEffect } from "react";
import { LeafIcon } from "./Icons/LeafIcon";
import { AllBestStaffIcon } from "./Icons/AllBestStaffIcon";
import { AllBestStaffIconDesktop } from "./Icons/AllBestStaffIconDesktop";
import { LeafIconDesktop } from "./Icons/LeafIconDesktop";

export const AllBestStaffUpper = () => {
  // SOLUTION AUTO RESPONSIVE WITH DESKTOP
  // Using getter size of window width to first state
  const getWindowSize = () => {
    if (window.innerWidth >= 1280) {
      return true;
    } else {
      return false;
    }
  };

  const [isDesktop, setIsDesktop] = useState(getWindowSize);

  //choose the screen size
  const handleResize = () => {
    if (window.innerWidth >= 1280) {
      setIsDesktop(true);
    } else {
      setIsDesktop(false);
    }
  };

  // create an event listener
  useEffect(() => {
    window.addEventListener("resize", handleResize);
  });
  return (
    <div className="flex mobile:flex-col tablet:flex-row tablet:justify-center bg-isabelline tablet:py-8 desktop:py-16">
      <div className="flex mobile:justify-center py-2 desktop:pr-8">
        {isDesktop && <AllBestStaffIconDesktop />}
        {!isDesktop && <AllBestStaffIcon />}
      </div>
      <div className="tablet:flex tablet:flex-col tablet:pt-4">
        <div className="flex mobile:justify-center tablet:justify-start">
          <div className="font-fraunces font-bold text-4xl desktop:text-7xl">
            Semua Best Staff
          </div>
        </div>
        <div className="flex mobile:justify-center tablet:justify-start py-2">
          <div className="font-commissioner mobile:max-w-[280px] tablet:max-w-[430px] desktop:max-w-[720px] desktop:text-2xl tablet:tracking-wider desktop:pb-2 desktop:leading-none">
            Semua Best Staff merupakan semua staf yang diberikan simbol
            penghargaan oleh BEM Fasilkom UI terhadap kontribusi yang telah
            diberikan dan sebagai evaluasi untuk memotivasi serta menjadi
            parameter perbaikan kinerja dari setiap anggota untuk kedepannya.
          </div>
        </div>
        <div className="mobile:invisible tablet:visible tablet:py-4">
          {isDesktop && <LeafIconDesktop />}
          {!isDesktop && <LeafIcon />}
        </div>
      </div>
    </div>
  );
};
