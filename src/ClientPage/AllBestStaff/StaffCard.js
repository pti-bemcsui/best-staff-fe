import React from "react";

export const StaffCard = (props) => {
  // Example : https://static.vecteezy.com/system/resources/previews/005/264/067/non_2x/man-walk-on-white-background-gesture-of-greeting-modern-trendy-geometric-fat-character-outline-cartoon-illustration-on-white-background-vector.jpg
  // Example 2 : https://cdn.dribbble.com/users/846207/screenshots/7044864/media/fbf2a224b8ad1ef8a0ef9c195b4c1fe2.gif;
  // Example 3 : https://static.vecteezy.com/system/resources/previews/005/264/067/non_2x/man-walk-on-white-background-gesture-of-greeting-modern-trendy-geometric-fat-character-outline-cartoon-illustration-on-white-background-vector.jpg
  // const imageUrl =
  //   "https://img.freepik.com/free-vector/man-shows-gesture-great-idea_10045-637.jpg?w=2000";
  console.log(props);

  return (
    <div className="overflow-hidden mobile:py-3 tablet:py-2 desktop:py-5 mobile:px-1 my-4 rounded-xl desktop:rounded-3xl">
      <div className="relative bg-darkgrey w-[143px] desktop:w-[235px] h-full rounded-xl desktop:rounded-3xl">
        {/* CARD IMAGE */}
        <div className="justify-center rounded-3xl flex flex-wrap">
          <div className="bg-isabelline rounded-t-lg desktop:rounded-t-2xl w-full">
            <img
              src={props.imageUrl}
              alt="staff"
              className="block w-full h-36 desktop:h-56 m-auto rounded-t-lg desktop:rounded-t-2xl object-cover"
            />
          </div>
        </div>
        {/* CARD INFO */}
        <div className="pt-2 relative bg-isabelline px-2 desktop:px-4 rounded-b-lg desktop:rounded-b-2xl max-h-full h-full //bg-red-500">
          {props.tahun === 2021 && (
            <div className="text-japanese font-bold font-commissioner text-[11px] desktop:text-[20px] //absolute //bottom-[60px] //desktop:bottom-[98px] //right-[8px] //desktop:right-[16px] text-right px-2">
              {props.bulan}
            </div>
          )}
          <div className="text-oceanblue font-bold font-commissioner text-[13px] desktop:text-[34px] mobile:pb-1 tablet:pb-1 //absolute //bottom-[45px]">
            {props.birdept}
          </div>
          <div className="font-bold font-commisioner text-[12px] desktop:text-[17px] desktop:py-1 //absolute //bottom-7 //desktop:bottom-[38px]">
            {props.nama}
          </div>
          <div className="font-normal font-commisioner text-[12px] desktop:text-[17px] //absolute //bottom-3 //desktop:bottom-4">
            {props.jurusan + " " + props.angkatan}
          </div>
        </div>
      </div>
    </div>
  );
};
