import React, { useState } from "react";

export const ParuhButton = (props) => {
  const [paruh, setParuh] = useState(1);

  const paruhSatuHandler = () => {
    // setParuh(1);
    props.changeParuh(1);
  };

  const paruhDuaHandler = () => {
    // setParuh(2);
    props.changeParuh(2);
  };

  // Fungsi untuk GET paruh
  // Send props child => parent
  const sendParuh = () => {
    const paruhData = {
      paruh: paruh,
    };
    props.onSendParuh(paruhData);
  };

  // Auto send paruhData ketika ada perubahan
  // sendParuh();

  if (props.tahun === 2022) {
    if (props.preset === "mobile") {
      return (
        <div className="flex text-isabelline text-[16px] font-commissioner font-bold gap-8 px-4 pt-6">
          <button onClick={paruhSatuHandler}>
            {paruh === 1 ? <u>Paruh 1</u> : "Paruh 1"}
          </button>
          {/* <button onClick={paruhDuaHandler}>
          {paruh === 2 ? <u>Paruh 2</u> : "Paruh 2"}
        </button> */}
        </div>
      );
    } else {
      return (
        <div className="flex text-isabelline text-[13px] desktop:text-[18px] font-commissioner font-semibold gap-3 desktop:gap-6 px-4 pt-9">
          <button onClick={paruhSatuHandler}>
            {paruh === 1 ? <u>Paruh 1</u> : "Paruh 1"}
          </button>
          {/* <button onClick={paruhDuaHandler}>
          {paruh === 2 ? <u>Paruh 2</u> : "Paruh 2"}
        </button> */}
        </div>
      );
    }
  } else return <></>;
};;
