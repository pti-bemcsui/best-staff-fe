import React, { useState, useEffect } from "react";
import { StaffCard } from "./StaffCard";

export const AllBestStaffCompilation = (props) => {
  const tahun = props.tahun;
  const paruh = props.paruh;

  // MASUKKAN API ADDRESS DI SINI :
  const API_URL = "https://ptibem.cs.ui.ac.id/beststaff/api/v1/homepage/";

  const [data, setData] = useState([]);
  const [fetchError, setFetchError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (tahun === 2022) {
          const response = await fetch(`${API_URL}/paruh/${paruh}/${tahun}`);
          if (!response.ok) throw Error("Did not receive expected data");
          const listItems = await response.json();
          console.log(listItems);
          console.log(tahun + "/" + paruh);
          setData(listItems.data.best_staff_paruh);
          setFetchError(null);
        } else if (tahun === 2021) {
          const response = await fetch(`${API_URL}`);
          if (!response.ok) throw Error("Did not receive expected data");
          const listItems = await response.json();
          console.log(listItems);
          console.log(tahun + "/" + paruh);
          setData(listItems.data.best_staff_bulanan);
          setFetchError(null);
        }

      } catch (err) {
        setFetchError(err.message);
      }
    };
    setTimeout(() => {
      (async () => await fetchData())();
    }, 2000);
  }, [props.tahun, props.paruh]);

  console.log(tahun + "/" + paruh);

  return (
    <div className="bg-spaceblue">
      <div className="grid mobile:grid-cols-2 tablet:grid-cols-4 mobile:px-3 tablet:px-16 desktop:px-36 pb-8 desktop:pb-16">
        {data && data.map((item) => (
          <div className="flex justify-center">
            <StaffCard
              imageUrl={tahun === 2022 ? item.best_staff.linkAlternate : item.best_staff.foto}
              nama={item.best_staff.nama}
              tahun={tahun}
              bulan={item.nama_bulan}
              birdept={item.best_staff.kode_birdept}
              jurusan={item.best_staff.jurusan}
              angkatan={item.best_staff.angkatan}
            />
          </div>
        ))}
      </div>
    </div>
  );
};
