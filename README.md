# PSDM Best Staff

FE Repository khusus frontend untuk salah satu program kerja **PSDM** yakni _Best Staff_

<br>
<hr>

# Starting Project

```
1. git clone https://gitlab.com/pti-bemcsui/best-staff-fe.git
atau menggunakan ssh masing - masing
2. npm install
3. npm start atau yarn start
```

<br>
<br>
<hr>

# Stack Use

1. React Bootstrap
2. styled-component
3. React Hooks Contex

<br>
<br>
<hr>

# Untuk dapatkan update dari main ke branch masing - masing

```
1. git remote -v (to see is linked or not)
2. git pull origin master
```

**Note : dilakukan di posisi git branch masing - masing**

<br>
<br>
<hr>

# Untuk push ke branch masing - masing

```
1.git add .
2.git commit -m'Fitur teraru'
2.git push (langsung push ke branch masing - masing)

                    Atau
2. git push --set-upstream origin <nama  branch>

```

_Note : Bisa dilakuakan apabila berada pada branch masing-masing_
<br>
<br>

<hr>

# Untuk push ke master dari branch masing - masing

```
1. git checkout master
2. git pull               # to update the latest  master state
3. git merge [nama branch]      # to merge branch to master
3. git push origin master # push current HEAD to master
```

<br>
<br>
<hr>

# To use self branch

```
format : git branch [nama branch]

contoh : git branch budi

         git branch

         git checkout budi
```

Note :
<br>
1.git checkout [nama brach] -> digunakan untuk berpindah branch
<br>
2.recomended use git branch

<br>
<br>
<hr>

# `yarn start` OR `npm start` to start

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
